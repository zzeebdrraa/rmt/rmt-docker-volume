# rmt-docker-volumes

`rmt-docker-volumes` is a command line tool that copies content of docker volumes from a remote host to a local machine.

## Note

*this project is currently work in progress. once end-2-end tests have been implemented and ran successfully, it is safe to use.*

## Features

- copy data from one or more docker volumes attached to docker containers to a local machine
- copy data from one or more standalone docker volumes to a local machine
- list docker volumes attached to particular docker containers
- list standalone docker volumes
- list files and directories in docker volumes attached to particular docker containers
- list files and directories in standalone docker volumes

## Dependencies

- `sshpass`
- `ssh`
- `rsync`
- `jq`
- [bats (for unit testing)](https://github.com/bats-core/bats-core)

## Submodules

submodules for acessing library code are not in place yet. currently, one should manually checkout the following repos alongside this repo:

- common-test
- common

## Installation

TO BE DONE

## Testing

```bash
cd tests/unittests
# run all bats tests in this directory
bats .
```

## Prerequisits

remote hosts are reached via ssh. it is currently assumed that authentication on remote host is done via ssh-keys.

it is currently not possible to explicitly specify a particular ssh user or key to be used by `rmt-docker-volumes`.

therefore one shoud have added all hosts with their ssh-keys and username to the local ssh config-file that is usually located in `~/.ssh/config`.

## Help

For detailed help run

```bash
rmt-docker-volumes.sh help
```

## Usage

```bash
rmt-docker-volumes.sh -h|--help
rmt-docker-volumes.sh help
rmt-docker-volumes.sh help s|short
rmt-docker-volumes.sh help d|details
rmt-docker-volumes.sh help e|examples

rmt-docker-volumes.sh --version

rmt-docker-volumes.sh s|status l|local
rmt-docker-volumes.sh s|status r|remote <host>

rmt-docker-volumes.sh [sudo] cp|copy
                      [-d|--dry-run] [-v|--verbose] [-p|--progress]
                      [--fp|--fullpath] [--ts|--timestamp]
                        <docker-volume-uri> ... <docker-volume-uri>
                        <local-directory>

rmt-docker-volumes.sh [sudo] lsv|list-volumes
                        <docker-volume-uri> ... <docker-volume-uri>

rmt-docker-volumes.sh [sudo] lsd|list-data
                      [--fp|--fullpath]
                        <docker-volume-uri> ... <docker-volume-uri>

rmt-docker-volumes.sh [sudo] rm|remove
                      [-d|--dry-run] [-v|--verbose] [-p|--progress]
                        <docker-volume-uri> ... <docker-volume-uri>
```

## Exit codes

- **0** - copying data from docker volume(s) was successful
- **1** - copying data from docker volume(s) failed completely
- **2** - copying data from docker volume(s) failed party

## Examples

```bash
# copy standalone volumes

# use a name defined in ssh config, and pass ssh-key password stored
# in a local variable, via stdin
read -r -s mypwd
rmt-docker-volumes.sh copy vol://my-host/my-volume ./dump <<<"${mypwd}"

# copy all volumes containing 'my' at any position in their names
rmt-docker-volumes.sh copy vol://my-host/my ./dump <<<"${mypwd}"

# copy all volumes starting with 'my' in their names
rmt-docker-volumes.sh copy vol://my-host/^my ./dump <<<"${mypwd}"

# copy all volumes ending with 'volume' in their names
rmt-docker-volumes.sh copy vol://my-host/volume$ ./dump <<<"${mypwd}"

# copy all volumes, some variations for the same goal
rmt-docker-volumes.sh copy vol://my-host/ ./dump <<<"${mypwd}"
rmt-docker-volumes.sh copy vol://my-host/* ./dump <<<"${mypwd}"
rmt-docker-volumes.sh copy vol://my-host/.* ./dump <<<"${mypwd}"

# copy volumes from several hosts to the same local folder
rmt-docker-volumes.sh copy vol://my-host-a/this-volume vol://my-host-b/that-volume ./dump <<<"${mypwd}"
  
# the same but use --fullpath option in order to create a subfolder
# tree on local host to distinguish data from both hosts
rmt-docker-volumes.sh copy --fullpath vol://my-host-a/this-volume vol://my-host-b/that-volume ./dump <<<"${mypwd}"
```

```bash
# copy volumes attached to cotainers/running services

# use a name defined in ssh config, and pass ssh-key password stored
# in a local variable, via stdin
read -r -s mypwd
rmt-docker-volumes.sh copy servol://my-host/my-service/my-volume ./dump <<<"${mypwd}"

# copy all volumes containing 'my' at any position in their names
rmt-docker-volumes.sh copy servol://my-host/my-service/my ./dump <<<"${mypwd}"

# copy all volumes starting with 'my' in their names
rmt-docker-volumes.sh copy servol://my-host/my-service/^my ./dump <<<"${mypwd}"

# copy all volumes ending with 'volume' in their names
rmt-docker-volumes.sh copy servol://my-host/my-service/volume$ ./dump <<<"${mypwd}"

# copy all volumes, some variations for the same goal
rmt-docker-volumes.sh copy servol://my-host/my-service ./dump <<<"${mypwd}"
rmt-docker-volumes.sh copy servol://my-host/my-service/* ./dump <<<"${mypwd}"
rmt-docker-volumes.sh copy servol://my-host/my-service/.* ./dump <<<"${mypwd}"

# copy volumes from several hosts to the same local folder
rmt-docker-volumes.sh copy vol://my-host-a/my-service/this-volume vol://my-host-b/my-service/that-volume ./dump <<<"${mypwd}"
```

```bash
# list data in a standalone volume
rmt-docker-volumes.sh lsd vol://my-host-a/my-volume

# list data in a volume attached to a container/running-service
rmt-docker-volumes.sh lsd vol://my-host-a/my-service/this-volume
```

```bash
# list standalone volumes on a particular host
rmt-docker-volumes.sh lsv vol://my-host-a

# list volumes attached to a container/running-service on a particular host
rmt-docker-volumes.sh lsv vol://my-host-a/my-service
```

```bash
# print status of mandatory dependencies available on local machine
rmt-docker-volumes.sh status local
> sshpass is /usr/bin/sshpass
> ssh is /usr/bin/ssh
> rsync is /usr/bin/rsync
> jq is /usr/bin/jq

# print status of mandatory dependencies available on existing remote machine (from where to copy docker volume data)
rmt-docker-volumes.sh status remote host-dev
> rsync is /usr/bin/rsync
> docker is /usr/bin/docker
```

## About volume uris

### Scheme

```bash
# select a docker volume of a docker container that is running on a host
servol://<host>/<docker-service>/<volume-name>

# select a particular named docker volume that exists on a host
vol://<host>/<volume-name>

# select particular files from a docker volume of a docker container that is running on a host
servol://<host>/<docker-service>/<volume-name>/<file>
servol://<host>/<docker-service>/<volume-name>/<file-pattern>

servol://<host>/<docker-service>/<volume-name>/<dir-path>/<file>
servol://<host>/<docker-service>/<volume-name>/<dir-path>/<file-pattern>

# select particular files from a particular named docker volume that exists on a host
vol://<host>/<volume-name>/<file>
vol://<host>/<volume-name>/<file-pattern>

vol://<host>/<volume-name>/<dir-path>/<file>
vol://<host>/<volume-name>/<dir-path>/<file-pattern>
```

### Details

- `<host>` is the uri or name of a remote host from where to copy volume data from to the local machine.

- `<host>` can be provided as a `<ssh-hostname>` referenced in `~/.ssh/config`

- `<docker-service>` might be the name of a running docker container/service on *<host>* to copy volume data from.

- `<docker-service>` might be the name that is set when starting a docker container via `docker run --name container-name`, or a generated name if `--name` option has not been provided explicitly.

- `<docker-volume>` might be a named or annonymous volume attached to the given and running `<docker-service>`.

- `<docker-volume>` might be a named standalone volume on the host, most likely not attached to any running `<docker-service>`.

- `<docker-volume>` might be a bind-mount volume path on the host, mounted into a running `<docker-service>`.

### Variations

```bash
# select all volumes of a docker container
servol://<host>/<docker-service>

# select particular volumes of a docker container
servol://<host>/<docker-service>/(<docker-volume>|<docker-volume>|<docker-volume>)
servol://<host>/<docker-service>/<pattern>

# select all named standalone volumes on a host
vol://<host>

# select particular named standalone volumes on a host
vol://<host>/(<docker-volume>|<docker-volume>|<docker-volume>)
vol://<host>/<pattern>
```

## ToDo

- create git submodules
- install via `make`
- run tests via `make`
- extract testing taxonomy in order to create a generic taxonomy for future testcases
- run code coverage of bats tests with [bashcov](https://github.com/infertux/bashcov)
- implement end-to-end tests with [robotframework](https://github.com/robotframework/robotframework)

## License

[GPL3](https://www.gnu.org/licenses/gpl-3.0.html#license-text)
