#!/usr/bin/env bash

# Copyright (C) 2023 zeebdrraa
#
# This file is part of rmt-docker-volumes.
#
# rmt-docker-volumes is free software: you can redistribute it and/or
# modify it under the terms of the GNU General Public License as published
# by the Free Software Foundation, either version 3 of the License, or 
# (at your option) any later version.
#
# rmt-docker-volumes is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
# Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with rmt-docker-volumes. If not, see <https://www.gnu.org/licenses/>. 

############################
# DEPENDENCIES
############################

# the following file must have been sourced before using functions of this file:
#
# - test-rmt-docker-volumes-mocks-common
#

############################
# MOCKS
############################

#***************************
# for cmd-list-data-servol
#***************************

mock-ssh-docker-inspect-service-multiple-mixed-json() {

  [[ "$*" =~ (.*docker inspect*) ]] && printf '%s' "${__docker_inspect_service_multi_mixed_json}" && return 0

  [[ "$*" =~ (sudo ls -la /mount/path/on/remote/host/abc) ]] && printf '%s\n' 'journal-abc.txt'
  [[ "$*" =~ (sudo ls -la /mount/path/on/remote/host/xyz) ]] && printf '%s\n' 'journal-xyz.txt'

  return 0
}

mock-ssh-docker-inspect-service-multiple-mixed-pattern-json() {

  [[ "$*" =~ (.*docker inspect*) ]] && printf '%s' "${__docker_inspect_service_multi_mixed_json}" && return 0

  [[ "$*" =~ (sudo ls -la /mount/path/on/remote/host/abc/\*\.txt) ]] && printf '%s\n' 'journal-abc.txt'
  [[ "$*" =~ (sudo ls -la /mount/path/on/remote/host/xyz/\*\.txt) ]] && printf '%s\n' 'journal-xyz.txt'
  [[ "$*" =~ (sudo ls -la /mount/path/on/remote/host/abc/journal-abc\*) ]] && printf '%s\n' 'journal-abc.txt'
  [[ "$*" =~ (sudo ls -la /mount/path/on/remote/host/xyz/journal-xyz\*) ]] && printf '%s\n' 'journal-xyz.txt'

  return 0
}

mock-ssh-docker-inspect-service-unknown-mixed-name-json() {

  [[ "$*" =~ (.*docker inspect*) ]] && printf '%s' "${__docker_inspect_service_single_mixed_unknown_name_json}" && return 0
  [[ "$*" =~ (sudo ls -la /mount/path/on/remote/host*) ]] && printf '%s\n' 'unknown.txt'

  return 0
}


mock-ssh-docker-inspect-service-single-bind-json() {

  [[ "$*" =~ (.*docker inspect*) ]] && printf '%s' "${__docker_inspect_service_single_bind_json}" && return 0
  [[ "$*" =~ (sudo ls -la /mount/path/on/remote/host*) ]] && printf '%s\n' 'journal.txt'

  return 0
}

mock-ssh-docker-inspect-service-multi-bind-json() {

  [[ "$*" =~ (.*docker inspect*) ]] && printf '%s' "${__docker_inspect_service_multi_bind_json}" && return 0

  [[ "$*" =~ (sudo ls -la /mount/path/on/remote/host/abc) ]] && printf '%s\n' 'journal-abc.txt'
  [[ "$*" =~ (sudo ls -la /mount/path/on/remote/host/xyz) ]] && printf '%s\n' 'journal-xyz.txt'

  return 0
}

mock-ssh-docker-inspect-service-single-bind-pattern-json() {

  [[ "$*" =~ (.*docker inspect*) ]] && printf '%s' "${__docker_inspect_service_single_bind_json}" && return 0

  [[ "$*" =~ (sudo ls -la /mount/path/on/remote/host/journal\*) ]] && printf '%s\n' 'journal.txt'
  [[ "$*" =~ (sudo ls -la /mount/path/on/remote/host/\*\.txt) ]] && printf '%s\n' 'journal.txt'

  return 0
}

mock-ssh-docker-inspect-service-multi-bind-pattern-json() {

  [[ "$*" =~ (.*docker inspect*) ]] && printf '%s' "${__docker_inspect_service_multi_bind_json}" && return 0

  [[ "$*" =~ (sudo ls -la /mount/path/on/remote/host/abc/\*\.txt) ]] && printf '%s\n' 'journal-abc.txt'
  [[ "$*" =~ (sudo ls -la /mount/path/on/remote/host/xyz/\*\.txt) ]] && printf '%s\n' 'journal-xyz.txt'
  [[ "$*" =~ (sudo ls -la /mount/path/on/remote/host/abc/journal-abc\*) ]] && printf '%s\n' 'journal-abc.txt'

  return 0
}

mock-sshp-docker-inspect-service-unknown-bind-name-json() {

  [[ "$*" =~ (.*docker inspect*) ]] && printf '%s' "${__docker_inspect_service_single_bind_unknown_name_json}" && return 0
  [[ "$*" =~ (sudo ls -la /mount/path/on/remote/host*) ]] && printf '%s\n' 'unknown.txt'
  return 0
}

mock-ssh-docker-inspect-service-single-volume-json() {

  [[ "$*" =~ (.*docker inspect*) ]] && printf '%s' "${__docker_inspect_service_single_volume_json}" && return 0
  [[ "$*" =~ (sudo ls -la /mount/path/on/remote/host*) ]] && printf '%s\n' 'journal.txt'
  return 0
}

mock-ssh-docker-inspect-service-multi-volume-json() {

  [[ "$*" =~ (.*docker inspect*) ]] && printf '%s' "${__docker_inspect_service_multi_volume_json}" && return 0

  [[ "$*" =~ (sudo ls -la /mount/path/on/remote/host/abc) ]] && printf '%s\n' 'journal-abc.txt'
  [[ "$*" =~ (sudo ls -la /mount/path/on/remote/host/xyz) ]] && printf '%s\n' 'journal-xyz.txt'

  return 0
}

mock-ssh-docker-inspect-service-single-volume-pattern-json() {

  [[ "$*" =~ (.*docker inspect*) ]] && printf '%s' "${__docker_inspect_service_single_volume_json}" && return 0

  [[ "$*" =~ (sudo ls -la /mount/path/on/remote/host/journal\*) ]] && printf '%s\n' 'journal.txt'
  [[ "$*" =~ (sudo ls -la /mount/path/on/remote/host/\*\.txt) ]] && printf '%s\n' 'journal.txt'

  return 0
}

mock-ssh-docker-inspect-service-multi-volume-pattern-json() {

  [[ "$*" =~ (.*docker inspect*) ]] && printf '%s' "${__docker_inspect_service_multi_volume_json}" && return 0

  [[ "$*" =~ (sudo ls -la /mount/path/on/remote/host/abc/\*\.txt) ]] && printf '%s\n' 'journal-abc.txt'
  [[ "$*" =~ (sudo ls -la /mount/path/on/remote/host/xyz/\*\.txt) ]] && printf '%s\n' 'journal-xyz.txt'
  [[ "$*" =~ (sudo ls -la /mount/path/on/remote/host/abc/journal-abc\*) ]] && printf '%s\n' 'journal-abc.txt'

  return 0
}

mock-ssh-docker-inspect-service-multi-volume-pattern-with-path-json() {
  [[ "$*" =~ (.*docker inspect*) ]] && printf '%s' "${__docker_inspect_service_multi_volume_json}" && return 0

  [[ "$*" =~ (sudo ls -la /mount/path/on/remote/host/abc/a/path/to/files/\*\.txt) ]] && printf '%s\n' 'journal-abc.txt'
  [[ "$*" =~ (sudo ls -la /mount/path/on/remote/host/xyz/a/path/to/files/\*\.txt) ]] && printf '%s\n' 'journal-xyz.txt'
  [[ "$*" =~ (sudo ls -la /mount/path/on/remote/host/abc/a/path/to/files/journal-abc\*) ]] && printf '%s\n' 'journal-abc.txt'

  return 0
}

mock-ssh-docker-inspect-service-single-volume-invalid-type-json() {

  [[ "$*" =~ (.*docker inspect*) ]] && printf '%s' "${__docker_inspect_service_single_volume_unknown_type_json}" && return 0
  [[ "$*" =~ (sudo ls -la /mount/path/on/remote/host*) ]] && printf '%s\n' 'journal.txt'
  return 0
}

mock-sshp-docker-inspect-service-unknown-volume-name-json() {

  [[ "$*" =~ (.*docker inspect*) ]] && printf '%s' "${__docker_inspect_service_single_volume_unknown_name_json}" && return 0
  [[ "$*" =~ (sudo ls -la /mount/path/on/remote/host*) ]] && printf '%s\n' 'unknown.txt'
  return 0
}

mock-ssh-docker-inspect-service-single-volume-ls-error() {

  [[ "$*" =~ (.*docker inspect*) ]] && printf '%s' "${__docker_inspect_service_single_volume_json}" && return 0
  [[ "$*" =~ (sudo ls -la /mount/path/on/remote/host*) ]] && return 1
  return 0
}