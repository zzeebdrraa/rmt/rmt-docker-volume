#!/usr/bin/env bash

# Copyright (C) 2023 zeebdrraa
#
# This file is part of rmt-docker-volumes.
#
# rmt-docker-volumes is free software: you can redistribute it and/or
# modify it under the terms of the GNU General Public License as published
# by the Free Software Foundation, either version 3 of the License, or 
# (at your option) any later version.
#
# rmt-docker-volumes is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
# Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with rmt-docker-volumes. If not, see <https://www.gnu.org/licenses/>. 

############################
# DEPENDENCIES
############################

# the following file must have been sourced before using functions of this file:
#
# - test-rmt-docker-volumes-mocks-common
#

#############################
# MOCKS 
#############################

#****************************
# for cmd-list-data-vol
#****************************

mock-ssh-docker-inspect-single-volume-json() {

  [[ "$*" =~ (sudo ls -la /mount/path/on/remote/host*) ]] && printf '%s\n' 'book.pdf' && return 0
  mock-ssh-docker-volume-inspect-single-volume-json "$@"
  return 0
}

mock-ssh-docker-inspect-multi-volume-json() {

  [[ "$*" =~ (sudo ls -la /mount/path/on/remote/host/abc) ]] && printf '%s\n' 'book-abc.pdf' && return 0
  [[ "$*" =~ (sudo ls -la /mount/path/on/remote/host/xyz) ]] && printf '%s\n' 'book-xyz.pdf' && return 0

  mock-ssh-docker-volume-inspect-single-volume-abc-json "$@"
  mock-ssh-docker-volume-inspect-single-volume-xyz-json "$@"
  mock-ssh-docker-volume-inspect-multi-volume-json "$@"

  return 0
}

mock-ssh-docker-inspect-unknown-volume-json() {
  [[ "$*" =~ (sudo ls -la /mount/path/on/remote/host*) ]] && printf '%s\n' 'unknown.txt' && return 0
  mock-ssh-docker-volume-inspect-unknown-name "$@"
  return 0
}

mock-ssh-docker-inspect-single-volume-list-json() {

  [[ "$*" =~ (sudo ls -la /mount/path/on/remote/host/book\*) ]] && printf '%s\n' 'book.pdf' && return 0
  [[ "$*" =~ (sudo ls -la /mount/path/on/remote/host/\*\.pdf) ]] && printf '%s\n' 'book.pdf' && return 0

  mock-ssh-docker-volume-inspect-single-volume-json "$@"
  return 0
}

mock-ssh-docker-inspect-multi-volume-list-json() {

  [[ "$*" =~ (sudo ls -la /mount/path/on/remote/host/abc/\*\.pdf) ]] && printf '%s\n' 'book-abc.pdf' && return 0
  [[ "$*" =~ (sudo ls -la /mount/path/on/remote/host/xyz/\*\.pdf) ]] && printf '%s\n' 'book-xyz.pdf' && return 0
  [[ "$*" =~ (sudo ls -la /mount/path/on/remote/host/abc/book-abc\*) ]] && printf '%s\n' 'book-abc.pdf' && return 0

  mock-ssh-docker-volume-inspect-multi-volume-json "$@"
  # always 0
  return 0
}

mock-ssh-docker-inspect-multi-volume-pattern-with-path-json() {

  [[ "$*" =~ (sudo ls -la /mount/path/on/remote/host/abc/a/path/to/files/\*\.pdf) ]] && printf '%s\n' 'book-abc.pdf' && return 0
  [[ "$*" =~ (sudo ls -la /mount/path/on/remote/host/xyz/a/path/to/files/\*\.pdf) ]] && printf '%s\n' 'book-xyz.pdf' && return 0
  [[ "$*" =~ (sudo ls -la /mount/path/on/remote/host/abc/a/path/to/files/book-abc\*) ]] && printf '%s\n' 'book-abc.pdf' && return 0

  mock-ssh-docker-volume-inspect-multi-volume-json "$@"
  # always 0
  return 0
}

mock-ssh-docker-inspect-empty-list-json() {
  [[ "$*" =~ (sudo ls -la /mount/path/on/remote/host*) ]] && printf '%s\n' 'journal.txt' && return 0

  [[ "$*" =~ (.*docker volume inspect*) ]] && mock-ssh-empty-list-json
  return 0
}

mock-ssh-docker-inspect-single-volume-ls-error() {
  [[ "$*" =~ (sudo ls -la /mount/path/on/remote/host*) ]] && return 1

  mock-ssh-docker-volume-inspect-single-volume-json "$@"
  return 0
}
