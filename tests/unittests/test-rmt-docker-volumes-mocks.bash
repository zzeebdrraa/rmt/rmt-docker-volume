#!/usr/bin/env bash

# Copyright (C) 2023 zeebdrraa
#
# This file is part of rmt-docker-volumes.
#
# rmt-common is free software: you can redistribute it and/or
# modify it under the terms of the GNU General Public License as published
# by the Free Software Foundation, either version 3 of the License, or 
# (at your option) any later version.
#
# rmt-common is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
# Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with rmt-docker-volumes. If not, see <https://www.gnu.org/licenses/>.

################################
# GLOBAL DATA
################################

#*******************************
# json for cmd-list-data-servol
#*******************************

# Type can only be 'bind' or "volume"
declare -r -g __docker_inspect_service_single_volume_unknown_type_json='[
  {
    "Mounts":[{
      "Type":"unknownType",
      "Name":"test-volume",
      "Source":"/mount/path/on/remote/host",
      "Destination":"some-path"
    }],
    "Name":"test-service",
    "Id":"1234"
  }
]'

declare -r -g __docker_inspect_service_single_volume_unknown_name_json='[
  {
    "Mounts":[{
      "Type":"volume",
      "Name":"unknown",
      "Source":"/mount/path/on/remote/host",
      "Destination":"some-path"
    }],
    "Name":"test-service",
    "Id":"1234"
  }
]'

declare -r -g __docker_inspect_service_single_bind_unknown_name_json='[
  {
    "Mounts":[{
      "Type":"bind",
      "Source":"/mount/path/on/remote/host/unknown",
      "Destination":"/some/path/unknown",
      "Mode": "",
      "RW": true,
      "Propagation": "rprivate"
    }],
    "Name":"test-service",
    "Id":"1234"
  }
]'

declare -r -g __docker_inspect_service_single_mixed_unknown_name_json='[
  {
    "Mounts":[{
      "Type":"volume",
      "Name":"unknown",
      "Source":"/mount/path/on/remote/host",
      "Destination":"some-path"
    },{
      "Type":"bind",
      "Source":"/mount/path/on/remote/host/unknown",
      "Destination":"/some/path/unknown",
      "Mode": "",
      "RW": true,
      "Propagation": "rprivate"
    }],
    "Name":"test-service",
    "Id":"1234"
  }
]'

declare -r -g __docker_inspect_service_single_volume_json='[
  {
    "Mounts":[{
      "Type":"volume",
      "Name":"test-volume",
      "Source":"/mount/path/on/remote/host",
      "Destination":"some-path"
    }],
    "Name":"test-service",
    "Id":"1234"
  }
]'

declare -r -g __docker_inspect_service_multi_volume_json='[
  {
    "Mounts":[{
      "Type":"volume",
      "Name":"test-volume-abc",
      "Destination":"/mount/path/on/remote/host/abc",
      "Source":"/mount/path/on/remote/host/abc"
    },{
      "Type":"volume",
      "Name":"test-volume-xyz",
      "Destination":"/mount/path/on/remote/host/xyz",
      "Source":"/mount/path/on/remote/host/xyz"
    }],
    "Name":"test-service",
    "Id":"1234"
  }
]'

declare -r -g __docker_inspect_service_single_bind_json='[
  {
    "Mounts":[{
      "Type":"bind",
      "Source":"/mount/path/on/remote/host",
      "Destination":"/some/path/in/volume",
      "Mode": "",
      "RW": true,
      "Propagation": "rprivate"
    }],
    "Name":"test-service",
    "Id":"1234"
  }
]'

declare -r -g __docker_inspect_service_multi_bind_json='[
  {
    "Mounts":[{
      "Type":"bind",
      "Source":"/mount/path/on/remote/host/abc",
      "Destination":"/some/path/in/volume/abc",
      "Mode": "",
      "RW": true,
      "Propagation": "rprivate"
    },{
      "Type":"bind",
      "Source":"/mount/path/on/remote/host/xyz",
      "Destination":"/some/path/in/volume/xyz",
      "Mode": "",
      "RW": true,
      "Propagation": "rprivate"
    }],
    "Name":"test-service",
    "Id":"1234"
  }
]'

declare -r -g  __docker_inspect_service_multi_mixed_json='[
  {
    "Mounts":[{
      "Type":"bind",
      "Source":"/mount/path/on/remote/host/abc",
      "Destination":"/some/path/in/volume/abc",
      "Mode": "",
      "RW": true,
      "Propagation": "rprivate"
    },{
      "Type":"volume",
      "Name":"test-volume-xyz",
      "Destination":"/mount/path/on/remote/host/xyz",
      "Source":"/mount/path/on/remote/host/xyz"
    }],
    "Name":"test-service",
    "Id":"1234"
  }
]'

#*******************************
# json for cmd-list-data-vol
#*******************************

declare -r -g __docker_inspect_volume_single_unknown_name_json='[
  {
    "Driver": "local",
    "Labels": {},
    "Name":"test-unknown",
    "Mountpoint":"/mount/path/on/remote/host/abc"
  }
]'

declare -r -g __docker_inspect_volume_single_json='[
  {
    "Driver": "local",
    "Labels": {},
    "Name":"test-volume",
    "Mountpoint":"/mount/path/on/remote/host"
  }
]'

declare -r -g __docker_inspect_volume_single_abc_json='[
  {
    "Driver": "local",
    "Labels": {},
    "Name":"test-volume-abc",
    "Mountpoint":"/mount/path/on/remote/host/abc"
  }
]'

declare -r -g __docker_inspect_volume_single_xyz_json='[
  {
    "Driver": "local",
    "Labels": {},
    "Name":"test-volume-xyz",
    "Mountpoint":"/mount/path/on/remote/host/xyz"
  }
]'

declare -r -g __docker_inspect_volume_multi_json='[
  {
    "Driver": "local",
    "Labels": {},
    "Name":"test-volume-abc",
    "Mountpoint":"/mount/path/on/remote/host/abc"
  },{
    "Driver": "local",
    "Labels": {},
    "Name":"test-volume-xyz",
    "Mountpoint":"/mount/path/on/remote/host/xyz"
  }
]'

############################
# MOCKS
############################

#*******************************
# json mocks
#*******************************

mock-ssh-empty-object-json() {
  printf '{}'
  return 0
}

mock-ssh-empty-objects-in-list-json() {
  printf '[{},{},{}]'
  return 0
}

mock-ssh-empty-list-json() {
  printf '[]'
  return 0
}

#*******************************
# ls mocks
#*******************************

mock-ssh-ls() {

  [[ "$*" =~ (sudo ls -la /mount/path/on/remote/host/abc) ]] && printf '%s\n' 'book-abc.pdf'
  [[ "$*" =~ (sudo ls -la /mount/path/on/remote/host/xyz) ]] && printf '%s\n' 'book-xyz.pdf'
  
  return 0
}

#*******************************
# mocks docker volume inspect
#*******************************

mock-ssh-docker-volume-inspect-error() {
  [[ "$*" =~ (.*docker volume inspect*) ]] && return 1
  return 0
}

mock-ssh-docker-service-inspect-error() {
  [[ "$*" =~ (.*docker inspect*) ]] && return 1
  return 0
}

mock-ssh-docker-volume-inspect-single-volume-json() {
  [[ "$*" =~ (.*docker volume inspect*) ]] && printf '%s' "${__docker_inspect_volume_single_json}"

  return 0
}

mock-ssh-docker-service-inspect-single-volume-json() {
  [[ "$*" =~ (.*docker inspect*) ]] && printf '%s' "${__docker_inspect_service_single_volume_json}"

  return 0
}

mock-ssh-docker-service-inspect-multi-volume-json() {
  [[ "$*" =~ (.*docker inspect*) ]] && printf '%s' "${__docker_inspect_service_multi_volume_json}"

  return 0
}