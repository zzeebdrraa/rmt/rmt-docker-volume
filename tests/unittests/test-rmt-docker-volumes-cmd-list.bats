#!/usr/bin/env bats

# Copyright (C) 2023 zeebdrraa
#
# This file is part of rmt-docker-volumes.
#
# rmt-docker-volumes is free software: you can redistribute it and/or
# modify it under the terms of the GNU General Public License as published
# by the Free Software Foundation, either version 3 of the License, or 
# (at your option) any later version.
#
# rmt-docker-volumes is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
# Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with rmt-docker-volumes. If not, see <https://www.gnu.org/licenses/>. 

############################
# TODO
############################

# - currently, tests that contain file patterns do not really reflect real
#     listing behaviour. this is due to the fact that mocks do not invoke
#     'ls'. 'ls' itself usually does the filtering with patterns.
#
#   it might be possible to test file pattern filtering behaviour by invoking
#     'ls' by a mock, with a particular temporary dirctory that is created
#     for testing only and that has been populated with some files and folders.
#

############################
# INCLUDES
############################

declare -r -g __source_dir="${BATS_TEST_DIRNAME}"

load "${__source_dir}/test-rmt-docker-volumes-mocks-common.bash"
load "${__source_dir}/test-rmt-docker-volumes-mocks-vol.bash"
load "${__source_dir}/test-rmt-docker-volumes-mocks-servol.bash"
load "${__source_dir}/../../../common-test/test-rmt-mocks-common.bash"

load "${__source_dir}/../../rmt-docker-volumes-cmd-list.dotfile"
load "${__source_dir}/../../rmt-docker-volumes-cmd-get.dotfile"

load "${__source_dir}/../../rmt-docker-volumes-data-common.dotfile"
load "${__source_dir}/../../rmt-docker-volumes-ssh.dotfile"
load "${__source_dir}/../../rmt-docker-volumes-json.dotfile"

load "${__source_dir}/../../../common/json.dotfile"
load "${__source_dir}/../../../common/strings.dotfile"
load "${__source_dir}/../../../common/types.dotfile"


############################
# Tests
############################

#***************************
# make-list-cmd / make-list-cmd-sudo
#***************************

@test "invoke rmt-docker-volumes-cmd-list - make-list-cmd - no args" {

  run make-list-cmd
  # printf '# l1 [%s]\n' "${output}" >&3

  [ ${status} -eq 0 ]
  [ "${output}" == 'ls -la' ]
  
  run make-list-cmd-sudo

  [ ${status} -eq 0 ]
  [ "${output}" == 'sudo ls -la' ]
}

@test "invoke rmt-docker-volumes-cmd-list - make-list-cmd - generate command" {

  run make-list-cmd '/mount/path/on/remote/host/abc'

  [ ${status} -eq 0 ]
  [ "${output}" == 'ls -la /mount/path/on/remote/host/abc' ]

  run make-list-cmd '/mount/path/on/remote/host/abc' '/mount/path/on/remote/host/xyz'

  [ ${status} -eq 0 ]
  [ "${output}" == 'ls -la /mount/path/on/remote/host/abc /mount/path/on/remote/host/xyz' ]
}

@test "invoke rmt-docker-volumes-cmd-list - make-list-cmd-sudo - generate command" {

  run make-list-cmd-sudo '/mount/path/on/remote/host/abc'

  [ ${status} -eq 0 ]
  [ "${output}" == 'sudo ls -la /mount/path/on/remote/host/abc' ]

  run make-list-cmd-sudo '/mount/path/on/remote/host/abc' '/mount/path/on/remote/host/xyz'

  [ ${status} -eq 0 ]
  [ "${output}" == 'sudo ls -la /mount/path/on/remote/host/abc /mount/path/on/remote/host/xyz' ]
}

#***************************
# ssh-list-dir / ssh-list-dir-sudo
#***************************

@test "invoke rmt-docker-volumes-cmd-list - ssh-list-dir - no args" {

  run ssh-list-dir
  # printf '# l1 %s\n' "${lines[@]}" >&3

  [ ${status} -eq 1 ]
  [ -z "${output}" ]

  run ssh-list-dir-sudo host

  [ ${status} -eq 1 ]
  [ -z "${output}" ]
}

@test "invoke rmt-docker-volumes-cmd-list - ssh-list-dir - list data - no pwd" {

  __ssh_cmd=mock-ssh-ls

  run ssh-list-dir 'host' '/mount/path/on/remote/host/abc'
  # printf '# l1 %s\n' "${lines[@]}" >&3

  [ ${status} -eq 0 ]
  [ "${output}" == 'book-abc.pdf' ]
  
  run ssh-list-dir 'host' '/mount/path/on/remote/host/xyz'

  [ ${status} -eq 0 ]
  [ "${output}" == 'book-xyz.pdf' ]
  
  run ssh-list-dir 'host' '/mount/path/on/remote/host/xyz' '/mount/path/on/remote/host/abc'

  [ ${status} -eq 0 ]
  [ "${lines[0]}" == 'book-xyz.pdf' ]
  [ "${lines[1]}" == 'book-abc.pdf' ]
}

@test "invoke rmt-docker-volumes-cmd-list - ssh-list-dir-sudo - list data - no pwd" {

  __ssh_cmd=mock-ssh-ls-sudo

  run ssh-list-dir-sudo 'host' '/mount/path/on/remote/host/abc'
  # printf '# l1 %s\n' "${lines[@]}" >&3

  [ ${status} -eq 0 ]
  [ "${output}" == 'book-abc.pdf' ]
  
  run ssh-list-dir-sudo 'host' '/mount/path/on/remote/host/xyz'
  # printf '# l2 %s\n' "${lines[@]}" >&3

  [ ${status} -eq 0 ]
  [ "${output}" == 'book-xyz.pdf' ]
  
  run ssh-list-dir-sudo 'host' '/mount/path/on/remote/host/xyz' '/mount/path/on/remote/host/abc'
  # printf '# l3 %s\n' "${lines[@]}" >&3

  [ ${status} -eq 0 ]
  [ "${lines[0]}" == 'book-xyz.pdf' ]
  [ "${lines[1]}" == 'book-abc.pdf' ]
}

@test "invoke rmt-docker-volumes-cmd-list - ssh-list-dir - ssh args" {

  __ssh_cmd=mock-cmd-ok-print-args

  run ssh-list-dir 'host' '/mount/path/on/remote/host/abc'
  # printf '# l1 %s\n' "${lines[@]}" >&3

  [ ${status} -eq 0 ]
  [ "${output}" == 'host ls -la /mount/path/on/remote/host/abc' ]
  
  run ssh-list-dir 'host' '/mount/path/on/remote/host/xyz' '/mount/path/on/remote/host/abc'
  # printf '# l3 %s\n' "${lines[@]}" >&3

  [ ${status} -eq 0 ]
  [ "${lines[0]}" == 'host ls -la /mount/path/on/remote/host/xyz' ]
  [ "${lines[1]}" == 'host ls -la /mount/path/on/remote/host/abc' ]
}

@test "invoke rmt-docker-volumes-cmd-list - ssh-list-dir-sudo - ssh args" {

  __ssh_cmd=mock-cmd-ok-print-args

  run ssh-list-dir-sudo 'host' '/mount/path/on/remote/host/abc'
  # printf '# l1 %s\n' "${lines[@]}" >&3

  [ ${status} -eq 0 ]
  [ "${output}" == 'host sudo ls -la /mount/path/on/remote/host/abc' ]

  run ssh-list-dir-sudo 'host' '/mount/path/on/remote/host/xyz' '/mount/path/on/remote/host/abc'
  # printf '# l3 %s\n' "${lines[@]}" >&3

  [ ${status} -eq 0 ]
  [ "${lines[0]}" == 'host sudo ls -la /mount/path/on/remote/host/xyz' ]
  [ "${lines[1]}" == 'host sudo ls -la /mount/path/on/remote/host/abc' ]
}

#***************************
# cmd-list-data-servol - service volumes
#***************************

@test "invoke rmt-docker-volumes-cmd-list - cmd-list-data-servol - no args" {

  # prevent accidential network connection via ssh
  __ssh_cmd=mock-cmd-error

  run cmd-list-data-servol

  [ ${status} -eq 1 ]
  [ -z "${output}" ]

  # general flags but no host
  run cmd-list-data-servol 0 0 0

  [ ${status} -eq 1 ]
  [ -z "${output}" ]
  
  # general flags and host but no service
  run cmd-list-data-servol 0 0 0 host

  [ ${status} -eq 1 ]
  [ -z "${output}" ]
}

@test "invoke rmt-docker-volumes-cmd-list - cmd-list-data-servol - no pwd - single - volume - match" {

  __ssh_cmd=mock-ssh-docker-inspect-service-single-volume-json

  run cmd-list-data-servol 1 1 0 host test-service volume
  # printf '# l %s\n' "${lines[@]}" >&3

  [ ${status} -eq 0 ]
  [ "${lines[0]}" == 'list volume data for [test-service:test-volume]' ]
  [ "${lines[2]}" == 'journal.txt' ]
}

@test "invoke rmt-docker-volumes-cmd-list - cmd-list-data-servol - no pwd - single - volume - match - file" {

  __ssh_cmd=mock-ssh-docker-inspect-service-single-volume-pattern-json

  run cmd-list-data-servol 1 1 0 host test-service volume 'journal*'
  # printf '# l1 %s\n' "${lines[@]}" >&3

  [ ${status} -eq 0 ]
  [ "${lines[0]}" == 'list volume data for [test-service:test-volume]' ]
  [ "${lines[2]}" == 'journal.txt' ]
  
  run cmd-list-data-servol 1 1 0 host test-service volume '*.txt'
  # printf '# l2 %s\n' "${lines[@]}" >&3

  [ ${status} -eq 0 ]
  [ "${lines[0]}" == 'list volume data for [test-service:test-volume]' ]
  [ "${lines[2]}" == 'journal.txt' ]
}

@test "invoke rmt-docker-volumes-cmd-list - cmd-list-data-servol - no pwd - multiple - volume - match" {

  __ssh_cmd=mock-ssh-docker-inspect-service-multi-volume-json

  # match all volumes
  run cmd-list-data-servol 1 1 0 host test-service volume
  # printf '# l %s\n' "${lines[@]}" >&3

  [ ${status} -eq 0 ]
  [ "${lines[0]}" == 'list volume data for [test-service:test-volume-abc]' ]
  [ "${lines[2]}" == 'journal-abc.txt' ]
  [ "${lines[4]}" == 'list volume data for [test-service:test-volume-xyz]' ]
  [ "${lines[6]}" == 'journal-xyz.txt' ]
  
  # match only one volume
  run cmd-list-data-servol 1 1 0 host test-service volume-abc
  # printf '# l %s\n' "${lines[@]}" >&3

  [ ${status} -eq 0 ]
  [ "${lines[0]}" == 'list volume data for [test-service:test-volume-abc]' ]
  [ "${lines[2]}" == 'journal-abc.txt' ]
}

@test "invoke rmt-docker-volumes-cmd-list - cmd-list-data-servol - no pwd - multiple - volume - match - file" {

  __ssh_cmd=mock-ssh-docker-inspect-service-multi-volume-pattern-json

  run cmd-list-data-servol 1 1 0 host test-service volume '*.txt'
  # printf '# l1 %s\n' "${lines[@]}" >&3

  [ ${status} -eq 0 ]
  [ "${lines[0]}" == 'list volume data for [test-service:test-volume-abc]' ]
  [ "${lines[2]}" == 'journal-abc.txt' ]
  [ "${lines[4]}" == 'list volume data for [test-service:test-volume-xyz]' ]
  [ "${lines[6]}" == 'journal-xyz.txt' ]
  
  # match only one volume
  run cmd-list-data-servol 1 1 0 host test-service volume 'journal-abc*'
  # printf '# l2 %s\n' "${lines[@]}" >&3

  [ ${status} -eq 0 ]
  [ "${lines[0]}" == 'list volume data for [test-service:test-volume-abc]' ]
  [ "${lines[2]}" == 'journal-abc.txt' ]
  
  # nothing else is listed as file pattern does not match files in this volume
  [ "${lines[4]}" == 'list volume data for [test-service:test-volume-xyz]' ]
}

@test "invoke rmt-docker-volumes-cmd-list - cmd-list-data-servol - no pwd - multiple - volume - match - path" {

  __ssh_cmd=mock-ssh-docker-inspect-service-multi-volume-pattern-with-path-json

  run cmd-list-data-servol 1 1 0 host test-service volume 'a/path/to/files/*.txt'
  # printf '# l1 %s\n' "${lines[@]}" >&3

  [ ${status} -eq 0 ]
  [ "${lines[0]}" == 'list volume data for [test-service:test-volume-abc]' ]
  [ "${lines[2]}" == 'journal-abc.txt' ]
  [ "${lines[4]}" == 'list volume data for [test-service:test-volume-xyz]' ]
  [ "${lines[6]}" == 'journal-xyz.txt' ]
  
  # match only one volume
  run cmd-list-data-servol 1 1 0 host test-service volume 'a/path/to/files/journal-abc*'
  # printf '# l2 %s\n' "${lines[@]}" >&3

  [ ${status} -eq 0 ]
  [ "${lines[0]}" == 'list volume data for [test-service:test-volume-abc]' ]
  [ "${lines[2]}" == 'journal-abc.txt' ]

  # nothing else is listed as file pattern does not match files in this volume
  [ "${lines[4]}" == 'list volume data for [test-service:test-volume-xyz]' ]
}

@test "invoke rmt-docker-volumes-cmd-list - cmd-list-data-servol - no pwd - single - bind - match" {

  __ssh_cmd=mock-ssh-docker-inspect-service-single-bind-json

  run cmd-list-data-servol 1 1 0 host test-service volume
  # printf '# l %s\n' "${lines[@]}" >&3

  [ ${status} -eq 0 ]
  [ "${lines[0]}" == 'list volume data for [test-service:/some/path/in/volume]' ]
  [ "${lines[2]}" == 'journal.txt' ]
}

@test "invoke rmt-docker-volumes-cmd-list - cmd-list-data-servol - no pwd - single - bind - match - file" {

  __ssh_cmd=mock-ssh-docker-inspect-service-single-bind-pattern-json

  run cmd-list-data-servol 1 1 0 host test-service volume 'journal*'
  # printf '# l1 %s\n' "${lines[@]}" >&3

  [ ${status} -eq 0 ]
  [ "${lines[0]}" == 'list volume data for [test-service:/some/path/in/volume]' ]
  [ "${lines[2]}" == 'journal.txt' ]
  
  run cmd-list-data-servol 1 1 0 host test-service volume '*.txt'
  # printf '# l2 %s\n' "${lines[@]}" >&3

  [ ${status} -eq 0 ]
  [ "${lines[0]}" == 'list volume data for [test-service:/some/path/in/volume]' ]
  [ "${lines[2]}" == 'journal.txt' ]
}

@test "invoke rmt-docker-volumes-cmd-list - cmd-list-data-servol - no pwd - multiple - bind - match" {

  __ssh_cmd=mock-ssh-docker-inspect-service-multi-bind-json

  # match all volumes
  run cmd-list-data-servol 1 1 0 host test-service volume
  # printf '# l1 %s\n' "${lines[@]}" >&3

  [ ${status} -eq 0 ]
  [ "${lines[0]}" == 'list volume data for [test-service:/some/path/in/volume/abc]' ]
  [ "${lines[2]}" == 'journal-abc.txt' ]
  [ "${lines[4]}" == 'list volume data for [test-service:/some/path/in/volume/xyz]' ]
  [ "${lines[6]}" == 'journal-xyz.txt' ]
  
  # match only one volume
  run cmd-list-data-servol 1 1 0 host test-service 'in/volume/abc'
  # printf '# l2 %s\n' "${lines[@]}" >&3

  [ ${status} -eq 0 ]
  [ "${lines[0]}" == 'list volume data for [test-service:/some/path/in/volume/abc]' ]
  [ "${lines[2]}" == 'journal-abc.txt' ]
}

@test "invoke rmt-docker-volumes-cmd-list - cmd-list-data-servol - no pwd - multiple - bind - match - file" {

  __ssh_cmd=mock-ssh-docker-inspect-service-multi-bind-pattern-json

  run cmd-list-data-servol 1 1 0 host test-service volume '*.txt'
  # printf '# l1 %s\n' "${lines[@]}" >&3

  [ ${status} -eq 0 ]
  [ "${lines[0]}" == 'list volume data for [test-service:/some/path/in/volume/abc]' ]
  [ "${lines[2]}" == 'journal-abc.txt' ]
  [ "${lines[4]}" == 'list volume data for [test-service:/some/path/in/volume/xyz]' ]
  [ "${lines[6]}" == 'journal-xyz.txt' ]
  
  # match only one volume
  run cmd-list-data-servol 1 1 0 host test-service volume 'journal-abc*'
  # printf '# l2 %s\n' "${lines[@]}" >&3

  [ ${status} -eq 0 ]
  [ "${lines[0]}" == 'list volume data for [test-service:/some/path/in/volume/abc]' ]
  [ "${lines[2]}" == 'journal-abc.txt' ]
  
  # nothing else is listed as file pattern does not match files in this volume
  [ "${lines[4]}" == 'list volume data for [test-service:/some/path/in/volume/xyz]' ]
}

@test "invoke rmt-docker-volumes-cmd-list - cmd-list-data-servol - no pwd - multiple - mixed - match" {

  __ssh_cmd=mock-ssh-docker-inspect-service-multiple-mixed-json

  # match all volumes
  run cmd-list-data-servol 1 1 0 host test-service volume
  # printf '# l1 %s\n' "${lines[@]}" >&3

  [ ${status} -eq 0 ]
  [ "${lines[0]}" == 'list volume data for [test-service:test-volume-xyz]' ]
  [ "${lines[2]}" == 'journal-xyz.txt' ]
  [ "${lines[4]}" == 'list volume data for [test-service:/some/path/in/volume/abc]' ]
  [ "${lines[6]}" == 'journal-abc.txt' ]
  
  # match only one volume
  run cmd-list-data-servol 1 1 0 host test-service 'volume-xyz'
  # printf '# l2 %s\n' "${lines[@]}" >&3

  [ ${status} -eq 0 ]
  [ "${lines[0]}" == 'list volume data for [test-service:test-volume-xyz]' ]
  [ "${lines[2]}" == 'journal-xyz.txt' ]
  
  # match only one volume
  run cmd-list-data-servol 1 1 0 host test-service 'in/volume/abc'
  # printf '# l3 %s\n' "${lines[@]}" >&3

  [ ${status} -eq 0 ]
  [ "${lines[0]}" == 'list volume data for [test-service:/some/path/in/volume/abc]' ]
  [ "${lines[2]}" == 'journal-abc.txt' ]
}

@test "invoke rmt-docker-volumes-cmd-list - cmd-list-data-servol - no pwd - multiple - mixed - match - file" {

  __ssh_cmd=mock-ssh-docker-inspect-service-multiple-mixed-pattern-json

  # match all volumes
  run cmd-list-data-servol 1 1 0 host test-service volume '*.txt'
  # printf '# l1 %s\n' "${lines[@]}" >&3

  [ ${status} -eq 0 ]
  [ "${lines[0]}" == 'list volume data for [test-service:test-volume-xyz]' ]
  [ "${lines[2]}" == 'journal-xyz.txt' ]
  [ "${lines[4]}" == 'list volume data for [test-service:/some/path/in/volume/abc]' ]
  [ "${lines[6]}" == 'journal-abc.txt' ]
  
  # match only one volume
  run cmd-list-data-servol 1 1 0 host test-service 'volume' 'journal-xyz*'
  # printf '# l2 %s\n' "${lines[@]}" >&3

  [ ${status} -eq 0 ]
  [ "${lines[0]}" == 'list volume data for [test-service:test-volume-xyz]' ]
  [ "${lines[2]}" == 'journal-xyz.txt' ]
  [ "${lines[4]}" == 'list volume data for [test-service:/some/path/in/volume/abc]' ]
  [ "${lines[5]}" == '---' ]
  [ "${lines[6]}" == '---' ]
  [ -z "${lines[7]}" ]
  
  # match only one volume
  run cmd-list-data-servol 1 1 0 host test-service 'volume' 'journal-abc*'
  # printf '# l3 %s\n' "${lines[@]}" >&3

  [ ${status} -eq 0 ]
  [ "${lines[0]}" == 'list volume data for [test-service:test-volume-xyz]' ]
  [ "${lines[1]}" == '---' ]
  [ "${lines[2]}" == '---' ]
  [ "${lines[3]}" == 'list volume data for [test-service:/some/path/in/volume/abc]' ]
  [ "${lines[5]}" == 'journal-abc.txt' ]
}

@test "invoke rmt-docker-volumes-cmd-list - cmd-list-data-servol - no pwd - failure - mixed - no match" {
  __ssh_cmd=mock-ssh-docker-inspect-service-multiple-mixed-json

  run cmd-list-data-servol 1 1 0 host test-service volume-not-existing
  # printf '# l1 %s\n' "${lines[@]}" >&3

  [ ${status} -eq 0 ]
  [ -z "${output}" ]

  __ssh_cmd=mock-ssh-docker-inspect-service-unknown-mixed-name-json
  
  run cmd-list-data-servol 1 1 0 host test-service volume
  # printf '# l2 %s\n' "${lines[@]}" >&3

  [ ${status} -eq 0 ]
  [ -z "${output}" ]
}

@test "invoke rmt-docker-volumes-cmd-list - cmd-list-data-servol - no pwd - failure - bind - no match" {
  __ssh_cmd=mock-ssh-docker-inspect-service-multi-bind-json

  run cmd-list-data-servol 1 1 0 host test-service volume-not-existing
  # printf '# l %s\n' "${lines[@]}" >&3

  [ ${status} -eq 0 ]
  [ -z "${output}" ]

  __ssh_cmd=mock-sshp-docker-inspect-service-unknown-bind-name-json
  
  run cmd-list-data-servol 1 1 0 host test-service volume
  # printf '# l %s\n' "${lines[@]}" >&3

  [ ${status} -eq 0 ]
  [ -z "${output}" ]
}

@test "invoke rmt-docker-volumes-cmd-list - cmd-list-data-servol - no pwd - failure - volume - no match" {
  __ssh_cmd=mock-ssh-docker-inspect-service-multi-volume-json

  run cmd-list-data-servol 1 1 0 host test-service volume-not-existing
  # printf '# l %s\n' "${lines[@]}" >&3

  [ ${status} -eq 0 ]
  [ -z "${output}" ]

  __ssh_cmd=mock-sshp-docker-inspect-service-unknown-volume-name-json
  
  run cmd-list-data-servol 1 1 0 host test-service volume
  # printf '# l %s\n' "${lines[@]}" >&3

  [ ${status} -eq 0 ]
  [ -z "${output}" ]
}

@test "invoke rmt-docker-volumes-cmd-list - cmd-list-data-servol - no pwd - failure - misc - no match" {

  # no match because mock returns an empty list
  __ssh_cmd=mock-ssh-empty-list-json

  run cmd-list-data-servol 1 1 0 host test-service volume
  # printf '# l %s\n' "${lines[@]}" >&3

  [ ${status} -eq 0 ]
  [ -z "${output}" ]

  # the volume type of json object in Mounts list is neither bind nor volume
  # thus nothing is being selected
  __ssh_cmd=mock-ssh-docker-inspect-service-single-volume-invalid-type-json

  run cmd-list-data-servol 1 1 0 host test-service volume
  # printf '# l1 %s\n' "${lines[@]}" >&3

  [ ${status} -eq 0 ]
  [ -z "${output}" ]
}

@test "invoke rmt-docker-volumes-cmd-list - cmd-list-data-servol - no pwd - error - ssh" {

  __ssh_cmd=mock-cmd-error

  run cmd-list-data-servol 1 1 0 host test-service volume
  # printf '# l1 %s\n' "${lines[@]}" >&3

  [ ${status} -eq 1 ]
  [ "${output}" == 'error: retrieval of docker volumes failed' ]
  
  __ssh_cmd=mock-ssh-docker-inspect-service-single-volume-ls-error

  run cmd-list-data-servol 1 1 0 host test-service volume
  # printf '# l2 %s\n' "${lines[@]}" >&3

  [ ${status} -eq 1 ]
  [ "${lines[0]}" == 'list volume data for [test-service:test-volume]' ]
  [ "${lines[2]}" == 'error: cannot list content of directory [/mount/path/on/remote/host] on host [host]' ]
}

@test "invoke rmt-docker-volumes-cmd-list - cmd-list-data-servol - no pwd - error - json" {
  
  # mock provides no json at all
  __ssh_cmd=mock-cmd-ok-no-output

  run cmd-list-data-servol 1 1 0 host test-service volume
  # printf '# l1 %s\n' "${lines[@]}" >&3

  [ ${status} -eq 1 ]
  [ "${output}" == 'error: empty inspect json' ]

  # mock provides an empty list. this is valid
  __ssh_cmd=mock-ssh-empty-list-json

  run cmd-list-data-servol 1 1 0 host test-service volume
  # printf '# l2 %s\n' "${lines[@]}" >&3

  [ ${status} -eq 0 ]
  [ -z "${output}" ]

  # mock provides an object instead of an array as top level json data
  __ssh_cmd=mock-ssh-empty-object-json

  run cmd-list-data-servol 1 1 0 host test-service volume
  # printf '# l3 %s\n' "${lines[@]}" >&3

  [ ${status} -eq 1 ]
  [ "${output}" == 'error: json object is not of type [array] but [object]' ]

  # mock provides three empty objects in a list, therefore we expect three error messages
  __ssh_cmd=mock-ssh-empty-objects-in-list-json

  run cmd-list-data-servol 1 1 0 host test-service volume
  # printf '# l4 %s\n' "${lines[@]}" >&3

  [ ${status} -eq 1 ]
  [ "${lines[0]}" == 'error: json object has no keys ["Mounts" "Name" "Id"]' ]
  [ "${lines[1]}" == 'error: json object has no keys ["Mounts" "Name" "Id"]' ]
  [ "${lines[2]}" == 'error: json object has no keys ["Mounts" "Name" "Id"]' ]
}

#***************************
# cmd-list-data-vol - volumes
#***************************

@test "invoke rmt-docker-volumes-cmd-list - cmd-list-data-vol - no args" {

  # prevent accidential network connection via ssh
  __ssh_cmd=mock-cmd-error

  run cmd-list-data-vol

  [ ${status} -eq 1 ]
  [ -z "${output}" ]
}

@test "invoke rmt-docker-volumes-cmd-list - cmd-list-data-vol - no pwd - single - match" {

  __ssh_cmd=mock-ssh-docker-inspect-single-volume-json

  run cmd-list-data-vol 1 1 0 host volume
  # printf '# l %s\n' "${lines[@]}" >&3

  [ ${status} -eq 0 ]
  [ "${lines[0]}" == 'list volume data for [test-volume]' ]
  [ "${lines[2]}" == 'book.pdf' ]
}

@test "invoke rmt-docker-volumes-cmd-list - cmd-list-data-vol - no pwd - single - match - file" {

  __ssh_cmd=mock-ssh-docker-inspect-single-volume-list-json

  run cmd-list-data-vol 1 1 0 host volume 'book*'
  # printf '# l %s\n' "${lines[@]}" >&3

  [ ${status} -eq 0 ]
  [ "${lines[0]}" == 'list volume data for [test-volume]' ]
  [ "${lines[2]}" == 'book.pdf' ]
  
  run cmd-list-data-vol 1 1 0 host volume '*.pdf'
  # printf '# l %s\n' "${lines[@]}" >&3

  [ ${status} -eq 0 ]
  [ "${lines[0]}" == 'list volume data for [test-volume]' ]
  [ "${lines[2]}" == 'book.pdf' ]
}

@test "invoke rmt-docker-volumes-cmd-list - cmd-list-data-vol - no pwd - multiple - match" {

  __ssh_cmd=mock-ssh-docker-inspect-multi-volume-json

  # match all volumes when leaving volumes argument empty
  run cmd-list-data-vol 1 1 0 host
  # printf '# l %s\n' "${lines[@]}" >&3

  [ ${status} -eq 0 ]
  [ "${lines[0]}" == 'list volume data for [test-volume-abc]' ]
  [ "${lines[2]}" == 'book-abc.pdf' ]
  [ "${lines[4]}" == 'list volume data for [test-volume-xyz]' ]
  [ "${lines[6]}" == 'book-xyz.pdf' ]

  run cmd-list-data-vol 1 1 0 host volume
  # printf '# l %s\n' "${lines[@]}" >&3

  [ ${status} -eq 0 ]
  [ "${lines[0]}" == 'list volume data for [test-volume-abc]' ]
  [ "${lines[2]}" == 'book-abc.pdf' ]
  [ "${lines[4]}" == 'list volume data for [test-volume-xyz]' ]
  [ "${lines[6]}" == 'book-xyz.pdf' ]
  
  # match only one volume
  run cmd-list-data-vol 1 1 0 host volume-abc
  # printf '# l %s\n' "${lines[@]}" >&3

  [ ${status} -eq 0 ]
  [ "${lines[0]}" == 'list volume data for [test-volume-abc]' ]
  [ "${lines[2]}" == 'book-abc.pdf' ]
}

@test "invoke rmt-docker-volumes-cmd-list - cmd-list-data-vol - no pwd - multiple - match - file" {

  __ssh_cmd=mock-ssh-docker-inspect-multi-volume-list-json

  run cmd-list-data-vol 1 1 0 host volume '*.pdf'
  # printf '# l %s\n' "${lines[@]}" >&3

  [ ${status} -eq 0 ]
  [ "${lines[0]}" == 'list volume data for [test-volume-abc]' ]
  [ "${lines[2]}" == 'book-abc.pdf' ]
  [ "${lines[4]}" == 'list volume data for [test-volume-xyz]' ]
  [ "${lines[6]}" == 'book-xyz.pdf' ]
  
  # match only one volume
  run cmd-list-data-vol 1 1 0 host volume 'book-abc*'
  # printf '# l %s\n' "${lines[@]}" >&3

  [ ${status} -eq 0 ]
  [ "${lines[0]}" == 'list volume data for [test-volume-abc]' ]
  [ "${lines[2]}" == 'book-abc.pdf' ]
  
  # nothing else is listed as file pattern does not match files in this volume
  [ "${lines[4]}" == 'list volume data for [test-volume-xyz]' ]
}

@test "invoke rmt-docker-volumes-cmd-list - cmd-list-data-vol - no pwd - multiple - match - path" {

  __ssh_cmd=mock-ssh-docker-inspect-multi-volume-pattern-with-path-json

  run cmd-list-data-vol 1 1 0 host volume 'a/path/to/files/*.pdf'
  # printf '# l1 %s\n' "${lines[@]}" >&3

  [ ${status} -eq 0 ]
  [ "${lines[0]}" == 'list volume data for [test-volume-abc]' ]
  [ "${lines[2]}" == 'book-abc.pdf' ]
  [ "${lines[4]}" == 'list volume data for [test-volume-xyz]' ]
  [ "${lines[6]}" == 'book-xyz.pdf' ]
  
  # match only one volume
  run cmd-list-data-vol 1 1 0 host volume 'a/path/to/files/book-abc*'
  # printf '# l2 %s\n' "${lines[@]}" >&3

  [ ${status} -eq 0 ]
  [ "${lines[0]}" == 'list volume data for [test-volume-abc]' ]
  [ "${lines[2]}" == 'book-abc.pdf' ]
  
  # nothing else is listed as file pattern does not match files in this volume
  [ "${lines[4]}" == 'list volume data for [test-volume-xyz]' ]
}

@test "invoke rmt-docker-volumes-cmd-list - cmd-list-data-vol - no pwd - failure - no match" {

  __ssh_cmd=mock-ssh-docker-inspect-empty-list-json

  # no match because mock returns an empty list
  run cmd-list-data-vol 1 1 0 host service volume
  # printf '# l1 %s\n' "${lines[@]}" >&3

  [ ${status} -eq 0 ]
  [ -z "${output}" ]
  
   __ssh_cmd=mock-ssh-docker-inspect-multi-volume-json

  run cmd-list-data-vol 1 1 0 host service volume-not-existing
  # printf '# l2 %s\n' "${lines[@]}" >&3

  [ ${status} -eq 0 ]
  [ -z "${output}" ]
  
  __ssh_cmd=mock-ssh-docker-inspect-unknown-volume-json
  
  run cmd-list-data-vol 1 1 0 host service volume
  # printf '# l3 %s\n' "${lines[@]}" >&3

  [ ${status} -eq 0 ]
  [ -z "${output}" ]
}


@test "invoke rmt-docker-volumes-cmd-list - cmd-list-data-vol - no pwd - error - ssh" {

  __ssh_cmd=mock-cmd-error

  run cmd-list-data-vol 1 1 0 host volume
  # printf '# l1 %s\n' "${lines[@]}" >&3

  [ ${status} -eq 1 ]
  [ "${output}" == 'error: retrieval of docker volumes failed' ]
  
  __ssh_cmd=mock-ssh-docker-inspect-single-volume-ls-error

  run cmd-list-data-vol 1 1 0 host volume
  # printf '# l2 %s\n' "${lines[@]}" >&3

  [ ${status} -eq 1 ]
  [ "${lines[0]}" == 'list volume data for [test-volume]' ]
  [ "${lines[2]}" == 'error: cannot list content of directory [/mount/path/on/remote/host] on host [host]' ]
}

@test "invoke rmt-docker-volumes-cmd-list - cmd-list-data-vol - no pwd - error - json" {
  
  # mock provides no json at all
  __ssh_cmd=mock-cmd-ok-no-output

  run cmd-list-data-vol 1 1 0 host volume
  # printf '# l1 %s\n' "${lines[@]}" >&3

  [ ${status} -eq 1 ]
  [ "${output}" == 'error: empty inspect json' ]

  # mock provides an empty list. this is valid
  __ssh_cmd=mock-ssh-empty-list-json

  run cmd-list-data-vol 1 1 0 host volume
  # printf '# l2 %s\n' "${lines[@]}" >&3

  [ ${status} -eq 0 ]
  [ -z "${output}" ]

  # mock provides an object instead of an array as top level json data
  __ssh_cmd=mock-ssh-empty-object-json

  run cmd-list-data-vol 1 1 0 host volume
  # printf '# l3 %s\n' "${lines[@]}" >&3

  [ ${status} -eq 1 ]
  [ "${output}" == 'error: json object is not of type [array] but [object]' ]

  # mock provides three empty objects in a list, therefore we expect three error messages
  __ssh_cmd=mock-ssh-empty-objects-in-list-json

  run cmd-list-data-vol 1 1 0 host volume
  # printf '# l4 %s\n' "${lines[@]}" >&3

  [ ${status} -eq 1 ]
  [ "${lines[0]}" == 'error: json object has no keys ["Name" "Mountpoint"]' ]
  [ "${lines[1]}" == 'error: json object has no keys ["Name" "Mountpoint"]' ]
  [ "${lines[2]}" == 'error: json object has no keys ["Name" "Mountpoint"]' ]
}

@test "invoke rmt-docker-volumes-cmd-list - cmd-list-volumes-vol - no pwd - single - match" {

  __ssh_cmd=mock-ssh-docker-inspect-single-volume-json

  run cmd-list-volumes-vol 1 1 0 host
  # printf '# l %s\n' "${lines[@]}" >&3

  [ ${status} -eq 0 ]
  [ "${lines[0]}" == 'test-volume' ]
  
  run cmd-list-volumes-vol 1 1 0 host volume
  # printf '# l %s\n' "${lines[@]}" >&3

  [ ${status} -eq 0 ]
  [ "${lines[0]}" == 'test-volume' ]
}

@test "invoke rmt-docker-volumes-cmd-list - cmd-list-volumes-vol - no pwd - multiple - match" {

  __ssh_cmd=mock-ssh-docker-inspect-multi-volume-json

  run cmd-list-volumes-vol 1 1 0 host
  # printf '# l1 %s\n' "${lines[@]}" >&3

  [ ${status} -eq 0 ]
  [ "${lines[0]}" == 'test-volume-abc' ]
  [ "${lines[1]}" == 'test-volume-xyz' ]
  
  run cmd-list-volumes-vol 1 1 0 host volume
  # printf '# l2 %s\n' "${lines[@]}" >&3

  [ ${status} -eq 0 ]
  [ "${lines[0]}" == 'test-volume-abc' ]
  [ "${lines[1]}" == 'test-volume-xyz' ]
  
  run cmd-list-volumes-vol 1 1 0 host abc
  # printf '# l3 %s\n' "${lines[@]}" >&3

  [ ${status} -eq 0 ]
  [ "${lines[0]}" == 'test-volume-abc' ]
  
  run cmd-list-volumes-vol 1 1 0 host xyz
  # printf '# l4 %s\n' "${lines[@]}" >&3

  [ ${status} -eq 0 ]
  [ "${lines[0]}" == 'test-volume-xyz' ]
}

@test "invoke rmt-docker-volumes-cmd-list - cmd-list-volumes-servol - no pwd - single - match" {

  __ssh_cmd=mock-ssh-docker-inspect-service-single-volume-json

  run cmd-list-volumes-servol 1 1 0 host test-service 
  # printf '# l1 %s\n' "${lines[@]}" >&3

  [ ${status} -eq 0 ]
  [ "${lines[0]}" == 'test-service:test-volume' ]
  
  run cmd-list-volumes-servol 1 1 0 host test-service volume
  # printf '# l2 %s\n' "${lines[@]}" >&3

  [ ${status} -eq 0 ]
  [ "${lines[0]}" == 'test-service:test-volume' ]
}

@test "invoke rmt-docker-volumes-cmd-list - cmd-list-volumes-servol - no pwd - multiple - match" {

  __ssh_cmd=mock-ssh-docker-inspect-service-multi-volume-json

  run cmd-list-volumes-servol 1 1 0 host test-service 
  # printf '# l1 %s\n' "${lines[@]}" >&3

  [ ${status} -eq 0 ]
  [ "${lines[0]}" == 'test-service:test-volume-abc' ]
  [ "${lines[1]}" == 'test-service:test-volume-xyz' ]
  
  run cmd-list-volumes-servol 1 1 0 host test-service volume
  # printf '# l2 %s\n' "${lines[@]}" >&3

  [ ${status} -eq 0 ]
  [ "${lines[0]}" == 'test-service:test-volume-abc' ]
  [ "${lines[1]}" == 'test-service:test-volume-xyz' ]
  
  run cmd-list-volumes-servol 1 1 0 host test-service abc
  # printf '# l3 %s\n' "${lines[@]}" >&3

  [ ${status} -eq 0 ]
  [ "${lines[0]}" == 'test-service:test-volume-abc' ]
  
  run cmd-list-volumes-servol 1 1 0 host test-service xyz
  # printf '# l4 %s\n' "${lines[@]}" >&3

  [ ${status} -eq 0 ]
  [ "${lines[0]}" == 'test-service:test-volume-xyz' ]
}