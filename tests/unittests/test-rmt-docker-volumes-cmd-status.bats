#!/usr/bin/env bats

# Copyright (C) 2023 zeebdrraa
#
# This file is part of rmt-docker-volumes.
#
# rmt-docker-volumes is free software: you can redistribute it and/or
# modify it under the terms of the GNU General Public License as published
# by the Free Software Foundation, either version 3 of the License, or 
# (at your option) any later version.
#
# rmt-docker-volumes is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
# Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with rmt-docker-volumes. If not, see <https://www.gnu.org/licenses/>. 

############################
# INCLUDES
############################

declare -r -g __source_dir="${BATS_TEST_DIRNAME}"

load "${__source_dir}/../../rmt-docker-volumes-cmd-status.dotfile"
load "${__source_dir}/../../rmt-docker-volumes-data-common.dotfile"
load "${__source_dir}/../../rmt-docker-volumes-data-help.dotfile"

load "${__source_dir}/../../../common-test/test-rmt-mocks-common"

############################
# MOCKS
############################

mock-print-status-local-cmd-ok() {
  printf 'generate-local-status-cmd-string'
  return 0
}

############################
# SETUP
############################

Setup() {
  __rmt_docker_volumes_local_status_cmd_generator=generate-local-status-cmd-string
  __rmt_docker_volumes_remote_status_cmd_generator=generate-remote-status-cmd-string
}

############################
# TESTS
############################

@test "invoke rmt-docker-volumes-cmd-status - generate-local-status-cmd-string" {

  run generate-local-status-cmd-string

  [ ${status} -eq 0 ]
  [ "${output}" == 'type sshpass ssh rsync jq' ]
}

@test "invoke rmt-docker-volumes-cmd-status - generate-remote-status-cmd-string" {

  run generate-remote-status-cmd-string

  [ ${status} -eq 0 ]
  [ "${output}" == 'type rsync docker' ]
}

###########

@test "invoke rmt-docker-volumes-cmd-status - print-help-status" {

  run print-help-status
  # printf '# l %s\n' "${lines[@]}" >&3

  [ ${status} -eq 0 ]
  [ ! -z "${output}" ]
}

###########

@test "invoke rmt-docker-volumes-cmd-status - print-status-remote - no args" {

  run print-status-remote
  # printf '# l %s\n' "${lines[@]}" >&3

  [ ${status} -eq 1 ]
  [ -z "${output}" ]
}

@test "invoke rmt-docker-volumes-cmd-status - print-status-remote - host - no pwd" {

  __ssh_cmd=mock-cmd-ok-print-args

  run print-status-remote test-host
  # printf '# l %s\n' "${lines[@]}" >&3

  [ ${status} -eq 0 ]
  [ "${output}" == 'test-host type rsync docker' ]
}

@test "invoke rmt-docker-volumes-cmd-status - print-status-remote - host - with pwd" {

  __sshpass_cmd=mock-cmd-ok-print-args

  run print-status-remote test-host <<<test-pwd
  # printf '# l %s\n' "${lines[@]}" >&3

  [ ${status} -eq 0 ]
  [ "${output}" == '-P ass ssh test-host type rsync docker' ]
}

###########

@test "invoke rmt-docker-volumes-cmd-status - print-status-local - no args" {

  __rmt_docker_volumes_local_status_cmd_generator=mock-print-status-local-cmd-ok

  run print-status-local
  # printf '# l %s\n' "${lines[@]}" >&3

  [ ${status} -eq 0 ]
  [ "${output}" == 'type sshpass ssh rsync jq' ]
}

###########

@test "invoke rmt-docker-volumes-cmd-status - dispatch-status - no args - print help" {

  run dispatch-status
  # printf '# l1 %s\n' "${lines[@]}" >&3

  [ ${status} -eq 0 ]
  [ ! -z "${output}" ]
}

@test "invoke rmt-docker-volumes-cmd-status - dispatch-status - local" {

  __rmt_docker_volumes_local_status_cmd_generator=mock-print-status-local-cmd-ok

  run dispatch-status local
  # printf '# l1 %s\n' "${lines[@]}" >&3

  [ ${status} -eq 0 ]
  [ "${output}" == 'type sshpass ssh rsync jq' ]
  
  run dispatch-status l
  # printf '# l2 %s\n' "${lines[@]}" >&3

  [ ${status} -eq 0 ]
  [ "${output}" == 'type sshpass ssh rsync jq' ]
}

@test "invoke rmt-docker-volumes-cmd-status - dispatch-status - remote - no pwd" {

  __ssh_cmd=mock-cmd-ok-print-args

  run dispatch-status remote test-host
  # printf '# l %s\n' "${lines[@]}" >&3

  [ ${status} -eq 0 ]
  [ "${output}" == 'test-host type rsync docker' ]
  
  run dispatch-status r test-host
  # printf '# l %s\n' "${lines[@]}" >&3

  [ ${status} -eq 0 ]
  [ "${output}" == 'test-host type rsync docker' ]
}

@test "invoke rmt-docker-volumes-cmd-status - dispatch-status - remote - with pwd" {
  __sshpass_cmd=mock-cmd-ok-print-args

  run dispatch-status remote test-host <<<'test-pwd'
  # printf '# l %s\n' "${lines[@]}" >&3

  [ ${status} -eq 0 ]
  [ "${output}" == '-P ass ssh test-host type rsync docker' ]
}