#!/usr/bin/env bats

# Copyright (C) 2023 zeebdrraa
#
# This file is part of rmt-docker-volumes.
#
# rmt-docker-volumes is free software: you can redistribute it and/or
# modify it under the terms of the GNU General Public License as published
# by the Free Software Foundation, either version 3 of the License, or 
# (at your option) any later version.
#
# rmt-docker-volumes is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
# Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with rmt-docker-volumes. If not, see <https://www.gnu.org/licenses/>.

############################
# TODO
############################

# - how to handle  '/path/to' subpath in host folder when using --fullpath option
# - add basic tests for list-volumes, with and without sudo

############################
# INCLUDES
############################

declare -r -g __source_dir="${BATS_TEST_DIRNAME}"
declare -r -g __scripts_dir="${BATS_TEST_DIRNAME}/.."

load "${__source_dir}/../../../common/types.dotfile"
load "${__source_dir}/../../../common/json.dotfile"
load "${__source_dir}/../../../common/strings.dotfile"
load "${__source_dir}/../../rmt-docker-volumes-json.dotfile"
load "${__source_dir}/../../rmt-docker-volumes.dotfile"

load "${__source_dir}/test-rmt-docker-volumes-mocks-common.bash"
load "${__source_dir}/test-rmt-docker-volumes-mocks-copy.bash"
load "${__source_dir}/../../../common-test/test-rmt-mocks-common.bash"

############################
# GLOBAL DATA
############################

declare -r -g __test_data_dir=$(mktemp --directory --dry-run)

############################
# MOCKS
############################

mock-common-mktemp() {
  printf '%s\n' "${__test_data_dir}"
}

############################
# Setup
############################

setup() {
  [[ -d "${__test_data_dir}" ]] && rm -r "${__test_data_dir}"
  mkdir -p "${__test_data_dir}"

  # overwrite definition in rmt-docker-volumes-cmd-copy.dotfile
  __rmt_docker_volume_local_copy_path="${__test_data_dir}/volume-copies"
  
  # overwrite definition in common/tmp.dotfile
  # no need to create a real temp dir in this command as a temp-dir
  # is created already above by mkdir "${__test_data_dir}"
  __mktemp_cmd=mock-common-mktemp
}

teardown() {
  [[ -d "${__test_data_dir}" ]] && rm -r "${__test_data_dir}"
}

############################
# TESTS
############################

@test "invoke rmt-docker-volume.sh - help" {

  run run-rmt-docker-volume -h
  [ ${status} -eq 0 ]
  [ ! -z "${output}" ]

  run run-rmt-docker-volume --help
  [ ${status} -eq 0 ]
  [ ! -z "${output}" ]
  
  run run-rmt-docker-volume help
  [ ${status} -eq 0 ]
  [ ! -z "${output}" ]

  run run-rmt-docker-volume help s
  [ ${status} -eq 0 ]
  [ ! -z "${output}" ]
  
  run run-rmt-docker-volume help short
  [ ${status} -eq 0 ]
  [ ! -z "${output}" ]
  
  run run-rmt-docker-volume help d
  [ ${status} -eq 0 ]
  [ ! -z "${output}" ]
  
  run run-rmt-docker-volume help details
  [ ${status} -eq 0 ]
  [ ! -z "${output}" ]
  
  run run-rmt-docker-volume help e
  [ ${status} -eq 0 ]
  [ ! -z "${output}" ]
  
  run run-rmt-docker-volume help examples
  [ ${status} -eq 0 ]
  [ ! -z "${output}" ]
}

@test "invoke rmt-docker-volume.sh - version" {

  run run-rmt-docker-volume --version

  [ ${status} -eq 0 ]
  [ "${output}" == '1.0.0' ]
}

@test "invoke rmt-docker-volume.sh - status - no args" {

  run run-rmt-docker-volume status
  [ ${status} -eq 0 ]
  [ ! -z "${output}" ]
  
  run run-rmt-docker-volume s
  [ ${status} -eq 0 ]
  [ ! -z "${output}" ]
}

##############################
# status command
##############################

@test "invoke rmt-docker-volume.sh - status - local" {

  run run-rmt-docker-volume status local
  # printf '# v %s\n' "${lines[0]}" >&3
  [ ${status} -eq 0 ]
  [ ! -z "${output}" ]
  
  run run-rmt-docker-volume status l
  # printf '# v %s\n' "${lines[0]}" >&3
  [ ${status} -eq 0 ]
  [ ! -z "${output}" ]
}

@test "invoke rmt-docker-volume.sh - status - remote" {

  __sshpass_cmd=mock-cmd-ok-print-args

  run run-rmt-docker-volume status remote test-host <<< 'testpwd'
  # printf '# l %s\n' "${lines[0]}" >&3

  [ ${status} -eq 0 ]
  [ "${output}" == '-P ass ssh test-host type rsync docker' ]
  
  run run-rmt-docker-volume status r test-host<<< 'testpwd'
  # printf '# a %s\n' "${lines[0]}" >&3

  [ ${status} -eq 0 ]
  [ "${output}" == '-P ass ssh test-host type rsync docker' ]
}

@test "invoke rmt-docker-volume.sh - copy - not enough args" {

  run run-rmt-docker-volume copy
  # printf '# v %s\n' "${lines[@]}" >&3

  [ ${status} -eq 1 ]
  [ "${output}" == 'error: no volume uri(s) provided ... stopping' ]
}

##############################
# service volume - bind mounts
##############################

@test "invoke rmt-docker-volume.sh - copy - servol - with pwd - single - bind" {

  __sshpass_cmd=mock-ssh-docker-service-inspect-single-bind-json
  __rsync_cmd=mock-cmd-ok-print-args

  run run-rmt-docker-volume copy --verbose 'servol://test-server/test-service/volume' <<< 'testpwd'
  # printf '# v %s\n' "${lines[@]}" >&3

  [ ${status} -eq 0 ]
  [ "${lines[0]}" == 'copy volume [test-service:/some/path/in/volume] from remote host [test-server] to local directory ['"${__rmt_docker_volume_local_copy_path}"']' ]
  [ "${lines[2]}" == '-avz --update --progress -e sshpass -e -P ass ssh test-server:./volume-copies '"${__rmt_docker_volume_local_copy_path}" ]

  run run-rmt-docker-volume copy --verbose 'servol://test-server/test-service' <<< 'testpwd'
  # printf '# v %s\n' "${lines[@]}" >&3

  [ ${status} -eq 0 ]
  [ "${lines[0]}" == 'copy volume [test-service:/some/path/in/volume] from remote host [test-server] to local directory ['"${__rmt_docker_volume_local_copy_path}"']' ]
  [ "${lines[2]}" == '-avz --update --progress -e sshpass -e -P ass ssh test-server:./volume-copies '"${__rmt_docker_volume_local_copy_path}" ]
}

@test "invoke rmt-docker-volume.sh - copy - servol - with pwd - single - bind - fullpath" {

  __sshpass_cmd=mock-ssh-docker-service-inspect-single-bind-json
  __rsync_cmd=mock-cmd-ok-print-args

  run run-rmt-docker-volume copy --verbose --fullpath servol://test-server/test-service/volume <<< 'testpwd'
  # printf '# v %s\n' "${lines[@]}" >&3

  [ ${status} -eq 0 ]
  [ "${lines[0]}" == 'copy volume [test-service:/some/path/in/volume] from remote host [test-server] to local directory ['"${__rmt_docker_volume_local_copy_path}"']' ]
  [ "${lines[2]}" == '-avz --update --progress -e sshpass -e -P ass ssh test-server:./volume-copies/test-service/some/path/in/volume '"${__rmt_docker_volume_local_copy_path}"'/test-server/test-service/some/path/in/volume' ]
}

@test "invoke rmt-docker-volume.sh - copy - servol - with pwd - single - bind - path and file pattern" {

  __sshpass_cmd=mock-ssh-docker-service-inspect-single-bind-json
  __rsync_cmd=mock-cmd-ok-print-args

  run run-rmt-docker-volume copy --verbose 'servol://test-server/test-service/volume/*.txt' <<< 'testpwd'
  # printf '# l1 %s\n' "${lines[@]}" >&3

  [ ${status} -eq 0 ]
  [ "${lines[0]}" == 'copy volume [test-service:/some/path/in/volume] from remote host [test-server] to local directory ['"${__rmt_docker_volume_local_copy_path}"']' ]
  [ "${lines[2]}" == '-avz --update --progress -e sshpass -e -P ass ssh test-server:./volume-copies/*.txt '"${__rmt_docker_volume_local_copy_path}" ]

  run run-rmt-docker-volume copy --verbose 'servol://test-server/test-service/volume/path/to/*.pdf' <<< 'testpwd'
  # printf '# l2 %s\n' "${lines[@]}" >&3

  [ ${status} -eq 0 ]
  [ "${lines[0]}" == 'copy volume [test-service:/some/path/in/volume] from remote host [test-server] to local directory ['"${__rmt_docker_volume_local_copy_path}"']' ]
  [ "${lines[2]}" == '-avz --update --progress -e sshpass -e -P ass ssh test-server:./volume-copies/path/to/*.pdf '"${__rmt_docker_volume_local_copy_path}" ]

  run run-rmt-docker-volume copy --verbose 'servol://test-server/test-service/volume/path/to/file' <<< 'testpwd'
  # printf '# l3 %s\n' "${lines[@]}" >&3

  [ ${status} -eq 0 ]
  [ "${lines[0]}" == 'copy volume [test-service:/some/path/in/volume] from remote host [test-server] to local directory ['"${__rmt_docker_volume_local_copy_path}"']' ]
  [ "${lines[2]}" == '-avz --update --progress -e sshpass -e -P ass ssh test-server:./volume-copies/path/to/file '"${__rmt_docker_volume_local_copy_path}" ]  
}

@test "invoke rmt-docker-volume.sh - copy - servol - with pwd - single - bind - fullpath - path and file pattern " {

  __sshpass_cmd=mock-ssh-docker-service-inspect-single-bind-json
  __rsync_cmd=mock-cmd-ok-print-args

  run run-rmt-docker-volume copy --verbose --fullpath 'servol://test-server/test-service/volume/*.txt' <<< 'testpwd'
  # printf '# v %s\n' "${lines[@]}" >&3

  [ ${status} -eq 0 ]
  [ "${lines[0]}" == 'copy volume [test-service:/some/path/in/volume] from remote host [test-server] to local directory ['"${__rmt_docker_volume_local_copy_path}"']' ]
  [ "${lines[2]}" == '-avz --update --progress -e sshpass -e -P ass ssh test-server:./volume-copies/test-service/some/path/in/volume/*.txt '"${__rmt_docker_volume_local_copy_path}"'/test-server/test-service/some/path/in/volume' ]

  skip 'how to handle path and file patterns within bind volumes when using --fullpath option'
  
  run run-rmt-docker-volume copy --verbose --fullpath 'servol://test-server/test-service/volume/path/to/*.pdf' <<< 'testpwd'
  # printf '# v %s\n' "${lines[@]}" >&3

  [ ${status} -eq 0 ]
  [ "${lines[0]}" == 'copy volume [test-service:/some/path/in/volume] from remote host [test-server] to local directory ['"${__rmt_docker_volume_local_copy_path}"']' ]
  # TODO does it make sense to create the '/path/to' subpath in host folder as well?
  [ "${lines[2]}" == '-avz --update --progress -e sshpass -e -P ass ssh test-server:./volume-copies/test-service/some/path/in/volume/path/to/*.pdf '"${__rmt_docker_volume_local_copy_path}"'/test-server/test-service/some/path/in/volume' ]

  run run-rmt-docker-volume copy --verbose --fullpath 'servol://test-server/test-service/volume/path/to/file' <<< 'testpwd'
  # printf '# v %s\n' "${lines[@]}" >&3

  [ ${status} -eq 0 ]
  [ "${lines[0]}" == 'copy volume [test-service:/some/path/in/volume] from remote host [test-server] to local directory ['"${__rmt_docker_volume_local_copy_path}"']' ]
  # TODO does it make sense to create the '/path/to' subpath in host folder as well?
  [ "${lines[2]}" == '-avz --update --progress -e sshpass -e -P ass ssh test-server:./volume-copies/test-service/some/path/in/volume/path/to/file '"${__rmt_docker_volume_local_copy_path}"'/test-server/test-service/some/path/in/volume' ]  
}

@test "invoke rmt-docker-volume.sh - copy - servol - with pwd - multi - bind - all bind mounts" {

  __sshpass_cmd=mock-ssh-docker-service-inspect-multi-bind-json
  __rsync_cmd=mock-cmd-ok-print-args

  run run-rmt-docker-volume copy --verbose servol://test-server/test-service/volume <<< 'testpwd'
  # printf '# l1 %s\n' "${lines[@]}" >&3

  [ ${status} -eq 0 ]
  [ "${lines[0]}" == 'copy volume [test-service:/some/path/in/volume/abc] from remote host [test-server] to local directory ['"${__rmt_docker_volume_local_copy_path}"']' ]
  [ "${lines[2]}" == '-avz --update --progress -e sshpass -e -P ass ssh test-server:./volume-copies '"${__rmt_docker_volume_local_copy_path}" ]
  [ "${lines[4]}" == 'copy volume [test-service:/some/path/in/volume/xyz] from remote host [test-server] to local directory ['"${__rmt_docker_volume_local_copy_path}"']' ]
  [ "${lines[2]}" == '-avz --update --progress -e sshpass -e -P ass ssh test-server:./volume-copies '"${__rmt_docker_volume_local_copy_path}" ]

  run run-rmt-docker-volume copy --verbose servol://test-server/test-service <<< 'testpwd'
  # printf '# l2 %s\n' "${lines[@]}" >&3

  [ ${status} -eq 0 ]
  [ "${lines[0]}" == 'copy volume [test-service:/some/path/in/volume/abc] from remote host [test-server] to local directory ['"${__rmt_docker_volume_local_copy_path}"']' ]
  [ "${lines[2]}" == '-avz --update --progress -e sshpass -e -P ass ssh test-server:./volume-copies '"${__rmt_docker_volume_local_copy_path}" ]
  [ "${lines[4]}" == 'copy volume [test-service:/some/path/in/volume/xyz] from remote host [test-server] to local directory ['"${__rmt_docker_volume_local_copy_path}"']' ]
  [ "${lines[6]}" == '-avz --update --progress -e sshpass -e -P ass ssh test-server:./volume-copies '"${__rmt_docker_volume_local_copy_path}" ]
}

@test "invoke rmt-docker-volume.sh - copy - servol - with pwd - multi - bind - fullpath - all bind mounts" {

  __sshpass_cmd=mock-ssh-docker-service-inspect-multi-bind-json
  __rsync_cmd=mock-cmd-ok-print-args

  run run-rmt-docker-volume copy --verbose --fullpath servol://test-server/test-service/volume <<< 'testpwd'
  # printf '# l1 %s\n' "${lines[@]}" >&3

  [ ${status} -eq 0 ]
  [ "${lines[0]}" == 'copy volume [test-service:/some/path/in/volume/abc] from remote host [test-server] to local directory ['"${__rmt_docker_volume_local_copy_path}"']' ]
  [ "${lines[2]}" == '-avz --update --progress -e sshpass -e -P ass ssh test-server:./volume-copies/test-service/some/path/in/volume/abc '"${__rmt_docker_volume_local_copy_path}"'/test-server/test-service/some/path/in/volume/abc' ]
  [ "${lines[4]}" == 'copy volume [test-service:/some/path/in/volume/xyz] from remote host [test-server] to local directory ['"${__rmt_docker_volume_local_copy_path}"']' ]
  [ "${lines[6]}" == '-avz --update --progress -e sshpass -e -P ass ssh test-server:./volume-copies/test-service/some/path/in/volume/xyz '"${__rmt_docker_volume_local_copy_path}"'/test-server/test-service/some/path/in/volume/xyz' ]

  run run-rmt-docker-volume copy --verbose --fullpath servol://test-server/test-service <<< 'testpwd'
  # printf '# l2 %s\n' "${lines[@]}" >&3

  [ ${status} -eq 0 ]
  [ "${lines[0]}" == 'copy volume [test-service:/some/path/in/volume/abc] from remote host [test-server] to local directory ['"${__rmt_docker_volume_local_copy_path}"']' ]
  [ "${lines[2]}" == '-avz --update --progress -e sshpass -e -P ass ssh test-server:./volume-copies/test-service/some/path/in/volume/abc '"${__rmt_docker_volume_local_copy_path}"'/test-server/test-service/some/path/in/volume/abc' ]
  [ "${lines[4]}" == 'copy volume [test-service:/some/path/in/volume/xyz] from remote host [test-server] to local directory ['"${__rmt_docker_volume_local_copy_path}"']' ]
  [ "${lines[6]}" == '-avz --update --progress -e sshpass -e -P ass ssh test-server:./volume-copies/test-service/some/path/in/volume/xyz '"${__rmt_docker_volume_local_copy_path}"'/test-server/test-service/some/path/in/volume/xyz' ]
}

@test "invoke rmt-docker-volume.sh - copy - servol - with pwd - multi - bind - path and file pattern" {

  __sshpass_cmd=mock-ssh-docker-service-inspect-multi-bind-json
  __rsync_cmd=mock-cmd-ok-print-args

  run run-rmt-docker-volume copy --verbose servol://test-server/test-service/volume/*.txt <<< 'testpwd'
  # printf '# v %s\n' "${lines[@]}" >&3

  [ ${status} -eq 0 ]
  [ "${lines[0]}" == 'copy volume [test-service:/some/path/in/volume/abc] from remote host [test-server] to local directory ['"${__rmt_docker_volume_local_copy_path}"']' ]
  [ "${lines[2]}" == '-avz --update --progress -e sshpass -e -P ass ssh test-server:./volume-copies/*.txt '"${__rmt_docker_volume_local_copy_path}" ]
  [ "${lines[4]}" == 'copy volume [test-service:/some/path/in/volume/xyz] from remote host [test-server] to local directory ['"${__rmt_docker_volume_local_copy_path}"']' ]
  [ "${lines[6]}" == '-avz --update --progress -e sshpass -e -P ass ssh test-server:./volume-copies/*.txt '"${__rmt_docker_volume_local_copy_path}" ]

  run run-rmt-docker-volume copy --verbose servol://test-server/test-service/volume/path/to/*.txt <<< 'testpwd'
  # printf '# v %s\n' "${lines[@]}" >&3

  [ ${status} -eq 0 ]
  [ "${lines[0]}" == 'copy volume [test-service:/some/path/in/volume/abc] from remote host [test-server] to local directory ['"${__rmt_docker_volume_local_copy_path}"']' ]
  [ "${lines[2]}" == '-avz --update --progress -e sshpass -e -P ass ssh test-server:./volume-copies/path/to/*.txt '"${__rmt_docker_volume_local_copy_path}" ]
  [ "${lines[4]}" == 'copy volume [test-service:/some/path/in/volume/xyz] from remote host [test-server] to local directory ['"${__rmt_docker_volume_local_copy_path}"']' ]
  [ "${lines[6]}" == '-avz --update --progress -e sshpass -e -P ass ssh test-server:./volume-copies/path/to/*.txt '"${__rmt_docker_volume_local_copy_path}" ]

  run run-rmt-docker-volume copy --verbose servol://test-server/test-service/volume/path/to/file <<< 'testpwd'
  # printf '# v %s\n' "${lines[@]}" >&3

  [ ${status} -eq 0 ]
  [ "${lines[0]}" == 'copy volume [test-service:/some/path/in/volume/abc] from remote host [test-server] to local directory ['"${__rmt_docker_volume_local_copy_path}"']' ]
  [ "${lines[2]}" == '-avz --update --progress -e sshpass -e -P ass ssh test-server:./volume-copies/path/to/file '"${__rmt_docker_volume_local_copy_path}" ]
  [ "${lines[4]}" == 'copy volume [test-service:/some/path/in/volume/xyz] from remote host [test-server] to local directory ['"${__rmt_docker_volume_local_copy_path}"']' ]
  [ "${lines[6]}" == '-avz --update --progress -e sshpass -e -P ass ssh test-server:./volume-copies/path/to/file '"${__rmt_docker_volume_local_copy_path}" ]  
}

@test "invoke rmt-docker-volume.sh - copy - servol - with pwd - multi - bind - fullpath - path and file pattern" {

  __sshpass_cmd=mock-ssh-docker-service-inspect-multi-bind-json
  __rsync_cmd=mock-cmd-ok-print-args

  run run-rmt-docker-volume copy --verbose --fullpath servol://test-server/test-service/volume/*.txt <<< 'testpwd'
  # printf '# v %s\n' "${lines[@]}" >&3

  [ ${status} -eq 0 ]
  [ "${lines[0]}" == 'copy volume [test-service:/some/path/in/volume/abc] from remote host [test-server] to local directory ['"${__rmt_docker_volume_local_copy_path}"']' ]
  [ "${lines[2]}" == '-avz --update --progress -e sshpass -e -P ass ssh test-server:./volume-copies/test-service/some/path/in/volume/abc/*.txt '"${__rmt_docker_volume_local_copy_path}"'/test-server/test-service/some/path/in/volume/abc' ]
  [ "${lines[4]}" == 'copy volume [test-service:/some/path/in/volume/xyz] from remote host [test-server] to local directory ['"${__rmt_docker_volume_local_copy_path}"']' ]
  [ "${lines[6]}" == '-avz --update --progress -e sshpass -e -P ass ssh test-server:./volume-copies/test-service/some/path/in/volume/xyz/*.txt '"${__rmt_docker_volume_local_copy_path}"'/test-server/test-service/some/path/in/volume/xyz' ]

  skip 'how to handle path and file patterns within bind volumes when using --fullpath option'
  
  run run-rmt-docker-volume copy --verbose --fullpath servol://test-server/test-service/volume/path/to/*.txt <<< 'testpwd'
  # printf '# v %s\n' "${lines[@]}" >&3

  [ ${status} -eq 0 ]
  [ "${lines[0]}" == 'copy volume [test-service:/some/path/in/volume/abc] from remote host [test-server] to local directory ['"${__rmt_docker_volume_local_copy_path}"']' ]
  [ "${lines[2]}" == '-avz --update --progress -e sshpass -e -P ass ssh test-server:./volume-copies/test-service/some/path/in/volume/abc/path/to/*.txt '"${__rmt_docker_volume_local_copy_path}"'/test-server/test-service/some/path/in/volume/abc' ]
  [ "${lines[4]}" == 'copy volume [test-service:/some/path/in/volume/xyz] from remote host [test-server] to local directory ['"${__rmt_docker_volume_local_copy_path}"']' ]
  [ "${lines[6]}" == '-avz --update --progress -e sshpass -e -P ass ssh test-server:./volume-copies/test-service/some/path/in/volume/xyz/path/to/*.txt '"${__rmt_docker_volume_local_copy_path}"'/test-server/test-service/some/path/in/volume/xyz' ]

  run run-rmt-docker-volume copy --verbose --fullpath servol://test-server/test-service/volume/path/to/file <<< 'testpwd'
  # printf '# v %s\n' "${lines[@]}" >&3

  [ ${status} -eq 0 ]
  [ "${lines[0]}" == 'copy volume [test-service:/some/path/in/volume/abc] from remote host [test-server] to local directory ['"${__rmt_docker_volume_local_copy_path}"']' ]
  [ "${lines[2]}" == '-avz --update --progress -e sshpass -e -P ass ssh test-server:./volume-copies/test-service/some/path/in/volume/abc/path/to/file '"${__rmt_docker_volume_local_copy_path}"'/test-server/test-service/some/path/in/volume/abc' ]
  [ "${lines[4]}" == 'copy volume [test-service:/some/path/in/volume/xyz] from remote host [test-server] to local directory ['"${__rmt_docker_volume_local_copy_path}"']' ]
  [ "${lines[6]}" == '-avz --update --progress -e sshpass -e -P ass ssh test-server:./volume-copies/test-service/some/path/in/volume/xyz/path/to/file '"${__rmt_docker_volume_local_copy_path}"'/test-server/test-service/some/path/in/volume/xyz' ]  
}

@test "invoke rmt-docker-volume.sh - copy - servol - with pwd - failure - bind - no match" {

  __sshpass_cmd=mock-ssh-docker-service-inspect-single-bind-json
  __rsync_cmd=mock-cmd-ok-print-args

  run run-rmt-docker-volume copy --verbose 'servol://test-server/test-service/test-volume-123' <<< 'testpwd'
  # printf '# v %s\n' "${lines[@]}" >&3

  [ ${status} -eq 0 ]
  [ -z "${output}" ]

  run run-rmt-docker-volume copy --verbose 'servol://test-server/test-service/another-test-*' <<< 'testpwd'
  # printf '# v %s\n' "${lines[@]}" >&3

  [ ${status} -eq 0 ]
  [ -z "${output}" ]
}

##############################
# service volume - volumes
##############################

@test "invoke rmt-docker-volume.sh - copy - servol - with pwd - single - volume" {

  __sshpass_cmd=mock-ssh-docker-service-inspect-single-volume-json
  __rsync_cmd=mock-cmd-ok-print-args

  run run-rmt-docker-volume copy --verbose 'servol://test-server/test-service/test-volume' <<< 'testpwd'
  # printf '# v %s\n' "${lines[@]}" >&3

  [ ${status} -eq 0 ]
  [ "${lines[0]}" == 'copy volume [test-service:test-volume] from remote host [test-server] to local directory ['"${__rmt_docker_volume_local_copy_path}"']' ]
  [ "${lines[2]}" == '-avz --update --progress -e sshpass -e -P ass ssh test-server:./volume-copies '"${__rmt_docker_volume_local_copy_path}" ]

  run run-rmt-docker-volume copy --verbose 'servol://test-server/test-service' <<< 'testpwd'
  # printf '# v %s\n' "${lines[@]}" >&3

  [ ${status} -eq 0 ]
  [ "${lines[0]}" == 'copy volume [test-service:test-volume] from remote host [test-server] to local directory ['"${__rmt_docker_volume_local_copy_path}"']' ]
  [ "${lines[2]}" == '-avz --update --progress -e sshpass -e -P ass ssh test-server:./volume-copies '"${__rmt_docker_volume_local_copy_path}" ]
}

@test "invoke rmt-docker-volume.sh - copy - servol - with pwd - single - volume - fullpath" {

  __sshpass_cmd=mock-ssh-docker-service-inspect-single-volume-json
  __rsync_cmd=mock-cmd-ok-print-args

  run run-rmt-docker-volume copy --verbose --fullpath servol://test-server/test-service/test-volume <<< 'testpwd'
  # printf '# v %s\n' "${lines[@]}" >&3

  [ ${status} -eq 0 ]
  [ "${lines[0]}" == 'copy volume [test-service:test-volume] from remote host [test-server] to local directory ['"${__rmt_docker_volume_local_copy_path}"']' ]
  [ "${lines[2]}" == '-avz --update --progress -e sshpass -e -P ass ssh test-server:./volume-copies/test-service/test-volume '"${__rmt_docker_volume_local_copy_path}"'/test-server/test-service/test-volume' ]
}

@test "invoke rmt-docker-volume.sh - copy - servol - with pwd - single - volume - path and file pattern " {

  __sshpass_cmd=mock-ssh-docker-service-inspect-single-volume-json
  __rsync_cmd=mock-cmd-ok-print-args

  run run-rmt-docker-volume copy --verbose 'servol://test-server/test-service/test-volume/*.txt' <<< 'testpwd'
  # printf '# v %s\n' "${lines[@]}" >&3

  [ ${status} -eq 0 ]
  [ "${lines[0]}" == 'copy volume [test-service:test-volume] from remote host [test-server] to local directory ['"${__rmt_docker_volume_local_copy_path}"']' ]
  [ "${lines[2]}" == '-avz --update --progress -e sshpass -e -P ass ssh test-server:./volume-copies/*.txt '"${__rmt_docker_volume_local_copy_path}" ]

  run run-rmt-docker-volume copy --verbose 'servol://test-server/test-service/test-volume/path/to/*.pdf' <<< 'testpwd'
  # printf '# v %s\n' "${lines[@]}" >&3

  [ ${status} -eq 0 ]
  [ "${lines[0]}" == 'copy volume [test-service:test-volume] from remote host [test-server] to local directory ['"${__rmt_docker_volume_local_copy_path}"']' ]
  [ "${lines[2]}" == '-avz --update --progress -e sshpass -e -P ass ssh test-server:./volume-copies/path/to/*.pdf '"${__rmt_docker_volume_local_copy_path}" ]

  run run-rmt-docker-volume copy --verbose 'servol://test-server/test-service/test-volume/path/to/file' <<< 'testpwd'
  # printf '# v %s\n' "${lines[@]}" >&3

  [ ${status} -eq 0 ]
  [ "${lines[0]}" == 'copy volume [test-service:test-volume] from remote host [test-server] to local directory ['"${__rmt_docker_volume_local_copy_path}"']' ]
  [ "${lines[2]}" == '-avz --update --progress -e sshpass -e -P ass ssh test-server:./volume-copies/path/to/file '"${__rmt_docker_volume_local_copy_path}" ]  
}

@test "invoke rmt-docker-volume.sh - copy - servol - with pwd - single - volume - fullpath - path and file pattern" {

  __sshpass_cmd=mock-ssh-docker-service-inspect-single-volume-json
  __rsync_cmd=mock-cmd-ok-print-args

  run run-rmt-docker-volume copy --verbose --fullpath 'servol://test-server/test-service/test-volume/*.txt' <<< 'testpwd'
  # printf '# v %s\n' "${lines[@]}" >&3

  [ ${status} -eq 0 ]
  [ "${lines[0]}" == 'copy volume [test-service:test-volume] from remote host [test-server] to local directory ['"${__rmt_docker_volume_local_copy_path}"']' ]
  [ "${lines[2]}" == '-avz --update --progress -e sshpass -e -P ass ssh test-server:./volume-copies/test-service/test-volume/*.txt '"${__rmt_docker_volume_local_copy_path}"'/test-server/test-service/test-volume' ]

  run run-rmt-docker-volume copy --verbose --fullpath 'servol://test-server/test-service/test-volume/path/to/*.pdf' <<< 'testpwd'
  # printf '# v %s\n' "${lines[@]}" >&3

  [ ${status} -eq 0 ]
  [ "${lines[0]}" == 'copy volume [test-service:test-volume] from remote host [test-server] to local directory ['"${__rmt_docker_volume_local_copy_path}"']' ]
  [ "${lines[2]}" == '-avz --update --progress -e sshpass -e -P ass ssh test-server:./volume-copies/test-service/test-volume/path/to/*.pdf '"${__rmt_docker_volume_local_copy_path}"'/test-server/test-service/test-volume' ]

  run run-rmt-docker-volume copy --verbose --fullpath 'servol://test-server/test-service/test-volume/path/to/file' <<< 'testpwd'
  # printf '# v %s\n' "${lines[@]}" >&3

  [ ${status} -eq 0 ]
  [ "${lines[0]}" == 'copy volume [test-service:test-volume] from remote host [test-server] to local directory ['"${__rmt_docker_volume_local_copy_path}"']' ]
  [ "${lines[2]}" == '-avz --update --progress -e sshpass -e -P ass ssh test-server:./volume-copies/test-service/test-volume/path/to/file '"${__rmt_docker_volume_local_copy_path}"'/test-server/test-service/test-volume' ]  
}

@test "invoke rmt-docker-volume.sh - copy - servol - with pwd - multi - volume - all volumes" {

  __sshpass_cmd=mock-ssh-docker-service-inspect-multi-volume-json
  __rsync_cmd=mock-cmd-ok-print-args

  run run-rmt-docker-volume copy --verbose 'servol://test-server/test-service/test-volume' <<< 'testpwd'
  # printf '# v %s\n' "${lines[@]}" >&3

  [ ${status} -eq 0 ]
  [ "${lines[0]}" == 'copy volume [test-service:test-volume-abc] from remote host [test-server] to local directory ['"${__rmt_docker_volume_local_copy_path}"']' ]
  [ "${lines[2]}" == '-avz --update --progress -e sshpass -e -P ass ssh test-server:./volume-copies '"${__rmt_docker_volume_local_copy_path}" ]
  [ "${lines[4]}" == 'copy volume [test-service:test-volume-xyz] from remote host [test-server] to local directory ['"${__rmt_docker_volume_local_copy_path}"']' ]
  [ "${lines[6]}" == '-avz --update --progress -e sshpass -e -P ass ssh test-server:./volume-copies '"${__rmt_docker_volume_local_copy_path}" ]

  run run-rmt-docker-volume copy --verbose 'servol://test-server/test-service' <<< 'testpwd'
  # printf '# v %s\n' "${lines[@]}" >&3

  [ ${status} -eq 0 ]
  [ "${lines[0]}" == 'copy volume [test-service:test-volume-abc] from remote host [test-server] to local directory ['"${__rmt_docker_volume_local_copy_path}"']' ]
  [ "${lines[2]}" == '-avz --update --progress -e sshpass -e -P ass ssh test-server:./volume-copies '"${__rmt_docker_volume_local_copy_path}" ]
  [ "${lines[4]}" == 'copy volume [test-service:test-volume-xyz] from remote host [test-server] to local directory ['"${__rmt_docker_volume_local_copy_path}"']' ]
  [ "${lines[6]}" == '-avz --update --progress -e sshpass -e -P ass ssh test-server:./volume-copies '"${__rmt_docker_volume_local_copy_path}" ]

  run run-rmt-docker-volume copy --verbose 'servol://test-server/test-service/test-volume-abc' 'servol://test-server/test-service/test-volume-xyz' <<< 'testpwd'
  # printf '# v %s\n' "${lines[@]}" >&3

  [ ${status} -eq 0 ]
  [ "${lines[0]}" == 'copy volume [test-service:test-volume-abc] from remote host [test-server] to local directory ['"${__rmt_docker_volume_local_copy_path}"']' ]
  [ "${lines[2]}" == '-avz --update --progress -e sshpass -e -P ass ssh test-server:./volume-copies '"${__rmt_docker_volume_local_copy_path}" ]
  [ "${lines[4]}" == 'copy volume [test-service:test-volume-xyz] from remote host [test-server] to local directory ['"${__rmt_docker_volume_local_copy_path}"']' ]
  [ "${lines[6]}" == '-avz --update --progress -e sshpass -e -P ass ssh test-server:./volume-copies '"${__rmt_docker_volume_local_copy_path}" ]

}

@test "invoke rmt-docker-volume.sh - copy - servol - with pwd - multi - volume - fullpath - all volumes" {

  __sshpass_cmd=mock-ssh-docker-service-inspect-multi-volume-json
  __rsync_cmd=mock-cmd-ok-print-args

  run run-rmt-docker-volume copy --verbose --fullpath servol://test-server/test-service/test-volume <<< 'testpwd'
  # printf '# v %s\n' "${lines[@]}" >&3

  [ ${status} -eq 0 ]
  [ "${lines[0]}" == 'copy volume [test-service:test-volume-abc] from remote host [test-server] to local directory ['"${__rmt_docker_volume_local_copy_path}"']' ]
  [ "${lines[2]}" == '-avz --update --progress -e sshpass -e -P ass ssh test-server:./volume-copies/test-service/test-volume-abc '"${__rmt_docker_volume_local_copy_path}"'/test-server/test-service/test-volume-abc' ]
  [ "${lines[4]}" == 'copy volume [test-service:test-volume-xyz] from remote host [test-server] to local directory ['"${__rmt_docker_volume_local_copy_path}"']' ]
  [ "${lines[6]}" == '-avz --update --progress -e sshpass -e -P ass ssh test-server:./volume-copies/test-service/test-volume-xyz '"${__rmt_docker_volume_local_copy_path}"'/test-server/test-service/test-volume-xyz' ]

  run run-rmt-docker-volume copy --verbose --fullpath servol://test-server/test-service/test-volume-abc servol://test-server/test-service/test-volume-xyz <<< 'testpwd'
  # printf '# v %s\n' "${lines[@]}" >&3

  [ ${status} -eq 0 ]
  [ "${lines[0]}" == 'copy volume [test-service:test-volume-abc] from remote host [test-server] to local directory ['"${__rmt_docker_volume_local_copy_path}"']' ]
  [ "${lines[2]}" == '-avz --update --progress -e sshpass -e -P ass ssh test-server:./volume-copies/test-service/test-volume-abc '"${__rmt_docker_volume_local_copy_path}"'/test-server/test-service/test-volume-abc' ]
  [ "${lines[4]}" == 'copy volume [test-service:test-volume-xyz] from remote host [test-server] to local directory ['"${__rmt_docker_volume_local_copy_path}"']' ]
  [ "${lines[6]}" == '-avz --update --progress -e sshpass -e -P ass ssh test-server:./volume-copies/test-service/test-volume-xyz '"${__rmt_docker_volume_local_copy_path}"'/test-server/test-service/test-volume-xyz' ]

}

@test "invoke rmt-docker-volume.sh - copy - servol - with pwd - multi - volume - path and file pattern" {

  __sshpass_cmd=mock-ssh-docker-service-inspect-multi-volume-json
  __rsync_cmd=mock-cmd-ok-print-args

  run run-rmt-docker-volume copy --verbose servol://test-server/test-service/test-volume/*.txt <<< 'testpwd'
  # printf '# v %s\n' "${lines[@]}" >&3

  [ ${status} -eq 0 ]
  [ "${lines[0]}" == 'copy volume [test-service:test-volume-abc] from remote host [test-server] to local directory ['"${__rmt_docker_volume_local_copy_path}"']' ]
  [ "${lines[2]}" == '-avz --update --progress -e sshpass -e -P ass ssh test-server:./volume-copies/*.txt '"${__rmt_docker_volume_local_copy_path}" ]
  [ "${lines[4]}" == 'copy volume [test-service:test-volume-xyz] from remote host [test-server] to local directory ['"${__rmt_docker_volume_local_copy_path}"']' ]
  [ "${lines[6]}" == '-avz --update --progress -e sshpass -e -P ass ssh test-server:./volume-copies/*.txt '"${__rmt_docker_volume_local_copy_path}" ]

  run run-rmt-docker-volume copy --verbose servol://test-server/test-service/test-volume/path/to/*.txt <<< 'testpwd'
  # printf '# v %s\n' "${lines[@]}" >&3

  [ ${status} -eq 0 ]
  [ "${lines[0]}" == 'copy volume [test-service:test-volume-abc] from remote host [test-server] to local directory ['"${__rmt_docker_volume_local_copy_path}"']' ]
  [ "${lines[2]}" == '-avz --update --progress -e sshpass -e -P ass ssh test-server:./volume-copies/path/to/*.txt '"${__rmt_docker_volume_local_copy_path}" ]
  [ "${lines[4]}" == 'copy volume [test-service:test-volume-xyz] from remote host [test-server] to local directory ['"${__rmt_docker_volume_local_copy_path}"']' ]
  [ "${lines[6]}" == '-avz --update --progress -e sshpass -e -P ass ssh test-server:./volume-copies/path/to/*.txt '"${__rmt_docker_volume_local_copy_path}" ]

  run run-rmt-docker-volume copy --verbose servol://test-server/test-service/test-volume/path/to/file <<< 'testpwd'
  # printf '# v %s\n' "${lines[@]}" >&3

  [ ${status} -eq 0 ]
  [ "${lines[0]}" == 'copy volume [test-service:test-volume-abc] from remote host [test-server] to local directory ['"${__rmt_docker_volume_local_copy_path}"']' ]
  [ "${lines[2]}" == '-avz --update --progress -e sshpass -e -P ass ssh test-server:./volume-copies/path/to/file '"${__rmt_docker_volume_local_copy_path}" ]
  [ "${lines[4]}" == 'copy volume [test-service:test-volume-xyz] from remote host [test-server] to local directory ['"${__rmt_docker_volume_local_copy_path}"']' ]
  [ "${lines[6]}" == '-avz --update --progress -e sshpass -e -P ass ssh test-server:./volume-copies/path/to/file '"${__rmt_docker_volume_local_copy_path}" ]  
}

@test "invoke rmt-docker-volume.sh - copy - servol - with pwd - multi - volume - fullpath - path and file pattern" {

  __sshpass_cmd=mock-ssh-docker-service-inspect-multi-volume-json
  __rsync_cmd=mock-cmd-ok-print-args

  run run-rmt-docker-volume copy --verbose --fullpath servol://test-server/test-service/test-volume/*.txt <<< 'testpwd'
  # printf '# v %s\n' "${lines[@]}" >&3

  [ ${status} -eq 0 ]
  [ "${lines[0]}" == 'copy volume [test-service:test-volume-abc] from remote host [test-server] to local directory ['"${__rmt_docker_volume_local_copy_path}"']' ]
  [ "${lines[2]}" == '-avz --update --progress -e sshpass -e -P ass ssh test-server:./volume-copies/test-service/test-volume-abc/*.txt '"${__rmt_docker_volume_local_copy_path}"'/test-server/test-service/test-volume-abc' ]
  [ "${lines[4]}" == 'copy volume [test-service:test-volume-xyz] from remote host [test-server] to local directory ['"${__rmt_docker_volume_local_copy_path}"']' ]
  [ "${lines[6]}" == '-avz --update --progress -e sshpass -e -P ass ssh test-server:./volume-copies/test-service/test-volume-xyz/*.txt '"${__rmt_docker_volume_local_copy_path}"'/test-server/test-service/test-volume-xyz' ]

  run run-rmt-docker-volume copy --verbose --fullpath servol://test-server/test-service/test-volume/path/to/*.txt <<< 'testpwd'
  # printf '# v %s\n' "${lines[@]}" >&3

  [ ${status} -eq 0 ]
  [ "${lines[0]}" == 'copy volume [test-service:test-volume-abc] from remote host [test-server] to local directory ['"${__rmt_docker_volume_local_copy_path}"']' ]
  [ "${lines[2]}" == '-avz --update --progress -e sshpass -e -P ass ssh test-server:./volume-copies/test-service/test-volume-abc/path/to/*.txt '"${__rmt_docker_volume_local_copy_path}"'/test-server/test-service/test-volume-abc' ]
  [ "${lines[4]}" == 'copy volume [test-service:test-volume-xyz] from remote host [test-server] to local directory ['"${__rmt_docker_volume_local_copy_path}"']' ]
  [ "${lines[6]}" == '-avz --update --progress -e sshpass -e -P ass ssh test-server:./volume-copies/test-service/test-volume-xyz/path/to/*.txt '"${__rmt_docker_volume_local_copy_path}"'/test-server/test-service/test-volume-xyz' ]

  run run-rmt-docker-volume copy --verbose --fullpath servol://test-server/test-service/test-volume/path/to/file <<< 'testpwd'
  # printf '# v %s\n' "${lines[@]}" >&3

  [ ${status} -eq 0 ]
  [ "${lines[0]}" == 'copy volume [test-service:test-volume-abc] from remote host [test-server] to local directory ['"${__rmt_docker_volume_local_copy_path}"']' ]
  [ "${lines[2]}" == '-avz --update --progress -e sshpass -e -P ass ssh test-server:./volume-copies/test-service/test-volume-abc/path/to/file '"${__rmt_docker_volume_local_copy_path}"'/test-server/test-service/test-volume-abc' ]
  [ "${lines[4]}" == 'copy volume [test-service:test-volume-xyz] from remote host [test-server] to local directory ['"${__rmt_docker_volume_local_copy_path}"']' ]
  [ "${lines[6]}" == '-avz --update --progress -e sshpass -e -P ass ssh test-server:./volume-copies/test-service/test-volume-xyz/path/to/file '"${__rmt_docker_volume_local_copy_path}"'/test-server/test-service/test-volume-xyz' ]  
}

@test "invoke rmt-docker-volume.sh - copy - servol - with pwd - failure - volume - no match" {

  __sshpass_cmd=mock-ssh-docker-service-inspect-single-volume-json
  __rsync_cmd=mock-cmd-ok-print-args

  run run-rmt-docker-volume copy --verbose 'servol://test-server/test-service/test-volume-123' <<< 'testpwd'
  # printf '# v %s\n' "${lines[@]}" >&3

  [ ${status} -eq 0 ]
  [ -z "${output}" ]
  
  # 
  run run-rmt-docker-volume copy --verbose 'servol://test-server/test-service/another-test-*' <<< 'testpwd'
  # printf '# v %s\n' "${lines[@]}" >&3

  [ ${status} -eq 0 ]
  [ -z "${output}" ]
}

##############################
# service volumes - errors
##############################

@test "invoke rmt-docker-volume.sh - copy - servol - with pwd - error - incomplete uri - no given service" {

  __sshpass_cmd=mock-ssh-docker-service-inspect-single-volume-json
  __rsync_cmd=mock-cmd-ok-print-args

  run run-rmt-docker-volume copy --verbose 'servol://test-server' <<< 'testpwd'
  # printf '# v %s\n' "${lines[@]}" >&3

  [ ${status} -eq 1 ]
  [ "${output}" == 'error: uri [servol://test-server] contains an invalid number of parts [2 < 3]' ]
}


@test "invoke rmt-docker-volume.sh - copy - servol - with pwd - error - ssh - docker inspect failure" {

  __sshpass_cmd=mock-ssh-docker-service-inspect-error
  __rsync_cmd=mock-cmd-ok-print-args

  run run-rmt-docker-volume copy --verbose 'servol://test-server/test-service/test-volume' <<< 'testpwd'
  # printf '# v %s\n' "${lines[@]}" >&3

  [ ${status} -eq 1 ]
  [ "${lines[0]}" == 'error: retrieval of docker volumes failed' ]
}

@test "invoke rmt-docker-volume.sh - copy - servol - with pwd - error - ssh - docker copy volume failure" {

  __sshpass_cmd=mock-ssh-docker-service-inspect-single-volume-json-docker-copy-volume-error
  __rsync_cmd=mock-cmd-ok-print-args

  run run-rmt-docker-volume copy --verbose 'servol://test-server/test-service/test-volume' <<< 'testpwd'
  # printf '# v %s\n' "${lines[@]}" >&3

  [ ${status} -eq 1 ]
  [ "${lines[0]}" == 'copy volume [test-service:test-volume] from remote host [test-server] to local directory ['"${__rmt_docker_volume_local_copy_path}"']' ]
  [ "${lines[2]}" == 'error: copying data from docker service volume [1234:/some/path/in/volume] to host filesystem [test-server:./volume-copies] failed' ]
}

@test "invoke rmt-docker-volume.sh - copy - servol - with pwd - error - ssh - rsync failure" {

  __sshpass_cmd=mock-ssh-docker-service-inspect-single-volume-json
  __rsync_cmd=mock-cmd-error

  run run-rmt-docker-volume copy --verbose 'servol://test-server/test-service/test-volume' <<< 'testpwd'
  # printf '# v %s\n' "${lines[@]}" >&3

  [ ${status} -eq 1 ]
  [ "${lines[0]}" == 'copy volume [test-service:test-volume] from remote host [test-server] to local directory ['"${__rmt_docker_volume_local_copy_path}"']' ]
  [ "${lines[2]}" == 'error: rsync data from host [test-server:./volume-copies] to local directory ['"${__rmt_docker_volume_local_copy_path}"'] failed' ]
}

@test "invoke rmt-docker-volume.sh - copy - servol - with pwd - warning - cleanup failure" {

  __sshpass_cmd=mock-ssh-docker-service-inspect-single-volume-json-cleanup-error
  __rsync_cmd=mock-cmd-ok-no-output

  run run-rmt-docker-volume copy --verbose 'servol://test-server/test-service/test-volume' <<< 'testpwd'
  # printf '# v %s\n' "${lines[@]}" >&3

  [ ${status} -eq 0 ]
  [ "${lines[0]}" == 'copy volume [test-service:test-volume] from remote host [test-server] to local directory ['"${__rmt_docker_volume_local_copy_path}"']' ]
  [ "${lines[2]}" == 'warning: could not delete data folder [./volume-copies] on host [test-server] during cleanup' ]
}

##############################
# standalone volumes
##############################

@test "invoke rmt-docker-volume.sh - copy - vol - with pwd - single - volume" {

  __sshpass_cmd=mock-ssh-docker-volume-inspect-single-volume-json
  __rsync_cmd=mock-cmd-ok-print-args

  run run-rmt-docker-volume copy --verbose 'vol://test-server/test-volume' <<< 'testpwd'
  # printf '# v %s\n' "${lines[@]}" >&3

  [ ${status} -eq 0 ]
  [ "${lines[0]}" == 'copy volume [test-volume] from remote host [test-server] to local directory ['"${__rmt_docker_volume_local_copy_path}"']' ]
  [ "${lines[2]}" == '-avz --update --progress -e sshpass -e -P ass ssh test-server:./volume-copies '"${__rmt_docker_volume_local_copy_path}" ]

  # all volumes are selected if only the host is provided in path
  run run-rmt-docker-volume copy --verbose 'vol://test-server' <<< 'testpwd'
  # printf '# v %s\n' "${lines[@]}" >&3

  [ ${status} -eq 0 ]
  [ "${lines[0]}" == 'copy volume [test-volume] from remote host [test-server] to local directory ['"${__rmt_docker_volume_local_copy_path}"']' ]
  [ "${lines[2]}" == '-avz --update --progress -e sshpass -e -P ass ssh test-server:./volume-copies '"${__rmt_docker_volume_local_copy_path}" ]
}

@test "invoke rmt-docker-volume.sh - copy - vol - with pwd - single - volume - fullpath" {

  # source ${__scripts_dir}/rmt-docker-volumes.dotfile

  __sshpass_cmd=mock-ssh-docker-volume-inspect-single-volume-json
  __rsync_cmd=mock-cmd-ok-print-args

  run run-rmt-docker-volume copy --verbose --fullpath vol://test-server/test-volume <<< 'testpwd'
  # printf '# v %s\n' "${lines[@]}" >&3

  [ ${status} -eq 0 ]
  [ "${lines[0]}" == 'copy volume [test-volume] from remote host [test-server] to local directory ['"${__rmt_docker_volume_local_copy_path}"']' ]
  [ "${lines[2]}" == '-avz --update --progress -e sshpass -e -P ass ssh test-server:./volume-copies/test-server/test-volume '"${__rmt_docker_volume_local_copy_path}"'/test-server/test-volume' ]
}

@test "invoke rmt-docker-volume.sh - copy - vol - with pwd - single - volume - fullpath - match path and file pattern" {

  # source ${__scripts_dir}/rmt-docker-volumes.dotfile

  __sshpass_cmd=mock-ssh-docker-volume-inspect-single-volume-json
  __rsync_cmd=mock-cmd-ok-print-args

  run run-rmt-docker-volume copy --verbose --fullpath 'vol://test-server/test-volume/*.txt' <<< 'testpwd'
  # printf '# v %s\n' "${lines[@]}" >&3

  [ ${status} -eq 0 ]
  [ "${lines[0]}" == 'copy volume [test-volume] from remote host [test-server] to local directory ['"${__rmt_docker_volume_local_copy_path}"']' ]
  [ "${lines[2]}" == '-avz --update --progress -e sshpass -e -P ass ssh test-server:./volume-copies/test-server/test-volume/*.txt '"${__rmt_docker_volume_local_copy_path}"'/test-server/test-volume' ]

  run run-rmt-docker-volume copy --verbose --fullpath 'vol://test-server/test-volume/path/to/*.pdf' <<< 'testpwd'
  # printf '# v %s\n' "${lines[@]}" >&3

  [ ${status} -eq 0 ]
  [ "${lines[0]}" == 'copy volume [test-volume] from remote host [test-server] to local directory ['"${__rmt_docker_volume_local_copy_path}"']' ]
  [ "${lines[2]}" == '-avz --update --progress -e sshpass -e -P ass ssh test-server:./volume-copies/test-server/test-volume/path/to/*.pdf '"${__rmt_docker_volume_local_copy_path}"'/test-server/test-volume' ]

  run run-rmt-docker-volume copy --verbose --fullpath 'vol://test-server/test-volume/path/to/file' <<< 'testpwd'
  # printf '# v %s\n' "${lines[@]}" >&3

  [ ${status} -eq 0 ]
  [ "${lines[0]}" == 'copy volume [test-volume] from remote host [test-server] to local directory ['"${__rmt_docker_volume_local_copy_path}"']' ]
  [ "${lines[2]}" == '-avz --update --progress -e sshpass -e -P ass ssh test-server:./volume-copies/test-server/test-volume/path/to/file '"${__rmt_docker_volume_local_copy_path}"'/test-server/test-volume' ]

}

@test "invoke rmt-docker-volume.sh - copy - vol - with pwd - single - volume - path and file pattern" {

  # source ${__scripts_dir}/rmt-docker-volumes.dotfile

  __sshpass_cmd=mock-ssh-docker-volume-inspect-single-volume-json
  __rsync_cmd=mock-cmd-ok-print-args

  run run-rmt-docker-volume copy --verbose 'vol://test-server/test-volume/*.txt' <<< 'testpwd'
  # printf '# v %s\n' "${lines[@]}" >&3

  [ ${status} -eq 0 ]
  [ "${lines[0]}" == 'copy volume [test-volume] from remote host [test-server] to local directory ['"${__rmt_docker_volume_local_copy_path}"']' ]
  [ "${lines[2]}" == '-avz --update --progress -e sshpass -e -P ass ssh test-server:./volume-copies/*.txt '"${__rmt_docker_volume_local_copy_path}" ]

  run run-rmt-docker-volume copy --verbose 'vol://test-server/test-volume/path/to/*.pdf' <<< 'testpwd'
  # printf '# v %s\n' "${lines[@]}" >&3

  [ ${status} -eq 0 ]
  [ "${lines[0]}" == 'copy volume [test-volume] from remote host [test-server] to local directory ['"${__rmt_docker_volume_local_copy_path}"']' ]
  [ "${lines[2]}" == '-avz --update --progress -e sshpass -e -P ass ssh test-server:./volume-copies/path/to/*.pdf '"${__rmt_docker_volume_local_copy_path}" ]

  run run-rmt-docker-volume copy --verbose 'vol://test-server/test-volume/path/to/file' <<< 'testpwd'
  # printf '# v %s\n' "${lines[@]}" >&3

  [ ${status} -eq 0 ]
  [ "${lines[0]}" == 'copy volume [test-volume] from remote host [test-server] to local directory ['"${__rmt_docker_volume_local_copy_path}"']' ]
  [ "${lines[2]}" == '-avz --update --progress -e sshpass -e -P ass ssh test-server:./volume-copies/path/to/file '"${__rmt_docker_volume_local_copy_path}" ]
}

@test "invoke rmt-docker-volume.sh - copy - vol - with pwd - multi - all volumes" {

  # source ${__scripts_dir}/rmt-docker-volumes.dotfile

  __sshpass_cmd=sshpass-mock-docker-inspect-multi-volume-json
  __rsync_cmd=mock-cmd-ok-print-args

  run run-rmt-docker-volume copy --verbose 'vol://test-server/test-volume' <<< 'testpwd'
  # printf '# v %s\n' "${lines[@]}" >&3

  [ ${status} -eq 0 ]
  [ "${lines[0]}" == 'copy volume [test-volume-abc] from remote host [test-server] to local directory ['"${__rmt_docker_volume_local_copy_path}"']' ]
  [ "${lines[2]}" == '-avz --update --progress -e sshpass -e -P ass ssh test-server:./volume-copies '"${__rmt_docker_volume_local_copy_path}" ]
  [ "${lines[4]}" == 'copy volume [test-volume-xyz] from remote host [test-server] to local directory ['"${__rmt_docker_volume_local_copy_path}"']' ]
  [ "${lines[6]}" == '-avz --update --progress -e sshpass -e -P ass ssh test-server:./volume-copies '"${__rmt_docker_volume_local_copy_path}" ]
  
  run run-rmt-docker-volume copy --verbose 'vol://test-server' <<< 'testpwd'
  # printf '# v %s\n' "${lines[@]}" >&3

  [ ${status} -eq 0 ]
  [ "${lines[0]}" == 'copy volume [test-volume-abc] from remote host [test-server] to local directory ['"${__rmt_docker_volume_local_copy_path}"']' ]
  [ "${lines[2]}" == '-avz --update --progress -e sshpass -e -P ass ssh test-server:./volume-copies '"${__rmt_docker_volume_local_copy_path}" ]
  [ "${lines[4]}" == 'copy volume [test-volume-xyz] from remote host [test-server] to local directory ['"${__rmt_docker_volume_local_copy_path}"']' ]
  [ "${lines[6]}" == '-avz --update --progress -e sshpass -e -P ass ssh test-server:./volume-copies '"${__rmt_docker_volume_local_copy_path}" ]
}

@test "invoke rmt-docker-volume.sh - copy - vol - with pwd - multi - volume - fullpath - all volumes" {

  # source ${__scripts_dir}/rmt-docker-volumes.dotfile

  __sshpass_cmd=sshpass-mock-docker-inspect-multi-volume-json
  __rsync_cmd=mock-cmd-ok-print-args

  run run-rmt-docker-volume copy --verbose --fullpath 'vol://test-server/test-volume' <<< 'testpwd'
  # printf '# v %s\n' "${lines[@]}" >&3

  [ ${status} -eq 0 ]
  [ "${lines[0]}" == 'copy volume [test-volume-abc] from remote host [test-server] to local directory ['"${__rmt_docker_volume_local_copy_path}"']' ]
  [ "${lines[2]}" == '-avz --update --progress -e sshpass -e -P ass ssh test-server:./volume-copies/test-server/test-volume-abc '"${__rmt_docker_volume_local_copy_path}"'/test-server/test-volume-abc' ]
  [ "${lines[4]}" == 'copy volume [test-volume-xyz] from remote host [test-server] to local directory ['"${__rmt_docker_volume_local_copy_path}"']' ]
  [ "${lines[6]}" == '-avz --update --progress -e sshpass -e -P ass ssh test-server:./volume-copies/test-server/test-volume-xyz '"${__rmt_docker_volume_local_copy_path}"'/test-server/test-volume-xyz' ]
}

@test "invoke rmt-docker-volume.sh - copy - vol - with pwd - multi - volume - path and file pattern" {

  # source ${__scripts_dir}/rmt-docker-volumes.dotfile

  __sshpass_cmd=sshpass-mock-docker-inspect-multi-volume-json
  __rsync_cmd=mock-cmd-ok-print-args

  run run-rmt-docker-volume copy --verbose vol://test-server/test-volume/*.txt <<< 'testpwd'
  # printf '# v %s\n' "${lines[@]}" >&3

  [ ${status} -eq 0 ]
  [ "${lines[0]}" == 'copy volume [test-volume-abc] from remote host [test-server] to local directory ['"${__rmt_docker_volume_local_copy_path}"']' ]
  [ "${lines[2]}" == '-avz --update --progress -e sshpass -e -P ass ssh test-server:./volume-copies/*.txt '"${__rmt_docker_volume_local_copy_path}" ]
  [ "${lines[4]}" == 'copy volume [test-volume-xyz] from remote host [test-server] to local directory ['"${__rmt_docker_volume_local_copy_path}"']' ]
  [ "${lines[6]}" == '-avz --update --progress -e sshpass -e -P ass ssh test-server:./volume-copies/*.txt '"${__rmt_docker_volume_local_copy_path}" ]

  run run-rmt-docker-volume copy --verbose vol://test-server/test-volume/path/to/*.txt <<< 'testpwd'
  # printf '# v %s\n' "${lines[@]}" >&3

  [ ${status} -eq 0 ]
  [ "${lines[0]}" == 'copy volume [test-volume-abc] from remote host [test-server] to local directory ['"${__rmt_docker_volume_local_copy_path}"']' ]
  [ "${lines[2]}" == '-avz --update --progress -e sshpass -e -P ass ssh test-server:./volume-copies/path/to/*.txt '"${__rmt_docker_volume_local_copy_path}" ]
  [ "${lines[4]}" == 'copy volume [test-volume-xyz] from remote host [test-server] to local directory ['"${__rmt_docker_volume_local_copy_path}"']' ]
  [ "${lines[6]}" == '-avz --update --progress -e sshpass -e -P ass ssh test-server:./volume-copies/path/to/*.txt '"${__rmt_docker_volume_local_copy_path}" ]

  run run-rmt-docker-volume copy --verbose vol://test-server/test-volume/path/to/*.txt <<< 'testpwd'
  # printf '# v %s\n' "${lines[@]}" >&3

  [ ${status} -eq 0 ]
  [ "${lines[0]}" == 'copy volume [test-volume-abc] from remote host [test-server] to local directory ['"${__rmt_docker_volume_local_copy_path}"']' ]
  [ "${lines[2]}" == '-avz --update --progress -e sshpass -e -P ass ssh test-server:./volume-copies/path/to/*.txt '"${__rmt_docker_volume_local_copy_path}" ]
  [ "${lines[4]}" == 'copy volume [test-volume-xyz] from remote host [test-server] to local directory ['"${__rmt_docker_volume_local_copy_path}"']' ]
  [ "${lines[6]}" == '-avz --update --progress -e sshpass -e -P ass ssh test-server:./volume-copies/path/to/*.txt '"${__rmt_docker_volume_local_copy_path}" ]
}

@test "invoke rmt-docker-volume.sh - copy - vol - with pwd - multi - volume - fullpath - path and file pattern" {

  # source ${__scripts_dir}/rmt-docker-volumes.dotfile

  __sshpass_cmd=sshpass-mock-docker-inspect-multi-volume-json
  __rsync_cmd=mock-cmd-ok-print-args

  run run-rmt-docker-volume copy --verbose --fullpath vol://test-server/test-volume/*.txt <<< 'testpwd'
  # printf '# v %s\n' "${lines[@]}" >&3

  [ ${status} -eq 0 ]
  [ "${lines[0]}" == 'copy volume [test-volume-abc] from remote host [test-server] to local directory ['"${__rmt_docker_volume_local_copy_path}"']' ]
  [ "${lines[2]}" == '-avz --update --progress -e sshpass -e -P ass ssh test-server:./volume-copies/test-server/test-volume-abc/*.txt '"${__rmt_docker_volume_local_copy_path}"'/test-server/test-volume-abc' ]
  [ "${lines[4]}" == 'copy volume [test-volume-xyz] from remote host [test-server] to local directory ['"${__rmt_docker_volume_local_copy_path}"']' ]
  [ "${lines[6]}" == '-avz --update --progress -e sshpass -e -P ass ssh test-server:./volume-copies/test-server/test-volume-xyz/*.txt '"${__rmt_docker_volume_local_copy_path}"'/test-server/test-volume-xyz' ]

  run run-rmt-docker-volume copy --verbose --fullpath vol://test-server/test-volume/path/to/*.txt <<< 'testpwd'
  # printf '# v %s\n' "${lines[@]}" >&3

  [ ${status} -eq 0 ]
  [ "${lines[0]}" == 'copy volume [test-volume-abc] from remote host [test-server] to local directory ['"${__rmt_docker_volume_local_copy_path}"']' ]
  [ "${lines[2]}" == '-avz --update --progress -e sshpass -e -P ass ssh test-server:./volume-copies/test-server/test-volume-abc/path/to/*.txt '"${__rmt_docker_volume_local_copy_path}"'/test-server/test-volume-abc' ]
  [ "${lines[4]}" == 'copy volume [test-volume-xyz] from remote host [test-server] to local directory ['"${__rmt_docker_volume_local_copy_path}"']' ]
  [ "${lines[6]}" == '-avz --update --progress -e sshpass -e -P ass ssh test-server:./volume-copies/test-server/test-volume-xyz/path/to/*.txt '"${__rmt_docker_volume_local_copy_path}"'/test-server/test-volume-xyz' ]

  run run-rmt-docker-volume copy --fullpath --verbose vol://test-server/test-volume/path/to/*.txt <<< 'testpwd'
  # printf '# v %s\n' "${lines[@]}" >&3

  [ ${status} -eq 0 ]
  [ "${lines[0]}" == 'copy volume [test-volume-abc] from remote host [test-server] to local directory ['"${__rmt_docker_volume_local_copy_path}"']' ]
  [ "${lines[2]}" == '-avz --update --progress -e sshpass -e -P ass ssh test-server:./volume-copies/test-server/test-volume-abc/path/to/*.txt '"${__rmt_docker_volume_local_copy_path}"'/test-server/test-volume-abc' ]
  [ "${lines[4]}" == 'copy volume [test-volume-xyz] from remote host [test-server] to local directory ['"${__rmt_docker_volume_local_copy_path}"']' ]
  [ "${lines[6]}" == '-avz --update --progress -e sshpass -e -P ass ssh test-server:./volume-copies/test-server/test-volume-xyz/path/to/*.txt '"${__rmt_docker_volume_local_copy_path}"'/test-server/test-volume-xyz' ]
}

@test "invoke rmt-docker-volume.sh - copy - vol - with pwd - without sudo - failure - no match" {

  # source ${__scripts_dir}/rmt-docker-volumes.dotfile

  __sshpass_cmd=mock-ssh-docker-volume-inspect-single-volume-json
  __rsync_cmd=mock-cmd-ok-print-args

  run run-rmt-docker-volume copy --verbose 'vol://test-server/test-volume-123' <<< 'testpwd'
  # printf '# v %s\n' "${lines[@]}" >&3

  [ ${status} -eq 0 ]
  [ -z "${output}" ]
  
  run run-rmt-docker-volume copy --verbose 'vol://test-server/another-test-*' <<< 'testpwd'
  # printf '# v %s\n' "${lines[@]}" >&3

  [ ${status} -eq 0 ]
  [ -z "${output}" ]
}


@test "invoke rmt-docker-volume.sh - copy - vol - with pwd - with sudo - failure - no match" {

  # source ${__scripts_dir}/rmt-docker-volumes.dotfile

  __sshpass_cmd=mock-ssh-docker-volume-inspect-single-volume-json
  __rsync_cmd=mock-cmd-ok-print-args

  run run-rmt-docker-volume sudo copy --verbose 'vol://test-server/test-volume-123' <<< 'testpwd'
  # printf '# v %s\n' "${lines[@]}" >&3

  [ ${status} -eq 0 ]
  [ -z "${output}" ]
  
  run run-rmt-docker-volume sudo copy --verbose 'vol://test-server/another-test-*' <<< 'testpwd'
  # printf '# v %s\n' "${lines[@]}" >&3

  [ ${status} -eq 0 ]
  [ -z "${output}" ]
}

##############################
# standalone volumes - errors
##############################

@test "invoke rmt-docker-volume.sh - copy - vol - with pwd - error - incomplete uri - no given host" {

  # source ${__scripts_dir}/rmt-docker-volumes.dotfile

  __sshpass_cmd=mock-ssh-docker-volume-inspect-single-volume-json
  __rsync_cmd=mock-cmd-ok-print-args

  run run-rmt-docker-volume copy --verbose 'vol://' <<< 'testpwd'
  # printf '# v %s\n' "${lines[@]}" >&3

  [ ${status} -eq 1 ]
  [ "${output}" == 'error: uri [vol://] has no path' ]
}

@test "invoke rmt-docker-volume.sh - copy - vol - with pwd - without sudo - error - ssh - docker inspect failure" {

  # source ${__scripts_dir}/rmt-docker-volumes.dotfile

  __sshpass_cmd=mock-ssh-docker-volume-inspect-error
  __rsync_cmd=mock-cmd-ok-print-args

  run run-rmt-docker-volume copy --verbose 'vol://test-server/test-volume' <<< 'testpwd'
  # printf '# v %s\n' "${lines[@]}" >&3

  [ ${status} -eq 1 ]
  [ "${lines[0]}" == 'error: retrieval of docker volumes failed' ]
}

@test "invoke rmt-docker-volume.sh - copy - vol - with pwd - with sudo - error - ssh - docker inspect failure" {

  # source ${__scripts_dir}/rmt-docker-volumes.dotfile

  __sshpass_cmd=mock-ssh-docker-volume-inspect-error
  __rsync_cmd=mock-cmd-ok-print-args

  run run-rmt-docker-volume copy --verbose 'vol://test-server/test-volume' <<< 'testpwd'
  # printf '# v %s\n' "${lines[@]}" >&3

  [ ${status} -eq 1 ]
  [ "${lines[0]}" == 'error: retrieval of docker volumes failed' ]
}

@test "invoke rmt-docker-volume.sh - copy - vol - with pwd - without sudo - error - ssh - copy volume failure" {

  # source ${__scripts_dir}/rmt-docker-volumes.dotfile

  __sshpass_cmd=mock-ssh-docker-volume-inspect-single-volume-json-copy-volume-error
  __rsync_cmd=mock-cmd-ok-print-args

  run run-rmt-docker-volume copy --verbose 'vol://test-server/test-volume' <<< 'testpwd'
  # printf '# v %s\n' "${lines[@]}" >&3

  [ ${status} -eq 1 ]
  [ "${lines[0]}" == 'copy volume [test-volume] from remote host [test-server] to local directory ['"${__rmt_docker_volume_local_copy_path}"']' ]
  [ "${lines[2]}" == 'error: copying data from docker volume mount [/mount/path/on/remote/host] to host filesystem [test-server:./volume-copies] failed' ]
}

@test "invoke rmt-docker-volume.sh - copy - vol - with pwd - with sudo - error - ssh - copy volume failure" {

  # source ${__scripts_dir}/rmt-docker-volumes.dotfile
  
  __sshpass_cmd=mock-ssh-docker-volume-inspect-single-volume-json-copy-volume-error
  __rsync_cmd=mock-cmd-ok-print-args

  run run-rmt-docker-volume sudo copy --verbose 'vol://test-server/test-volume' <<< 'testpwd'
  # printf '# v %s\n' "${lines[@]}" >&3

  [ ${status} -eq 1 ]
  [ "${lines[0]}" == 'copy volume [test-volume] from remote host [test-server] to local directory ['"${__rmt_docker_volume_local_copy_path}"']' ]
  [ "${lines[2]}" == 'error: copying data from docker volume mount [/mount/path/on/remote/host] to host filesystem [test-server:./volume-copies] failed' ]
}

@test "invoke rmt-docker-volume.sh - copy - vol - with pwd - without sudo - warning - cleanup failure" {

  # source ${__scripts_dir}/rmt-docker-volumes.dotfile

  __sshpass_cmd=mock-ssh-docker-volume-inspect-single-volume-json-cleanup-error
  __rsync_cmd=mock-cmd-ok-no-output

  run run-rmt-docker-volume copy --verbose 'vol://test-server/test-volume' <<< 'testpwd'
  # printf '# v %s\n' "${lines[@]}" >&3

  [ ${status} -eq 0 ]
  [ "${lines[0]}" == 'copy volume [test-volume] from remote host [test-server] to local directory ['"${__rmt_docker_volume_local_copy_path}"']' ]
  [ "${lines[2]}" == 'warning: could not delete data folder [./volume-copies] on host [test-server] during cleanup' ]
}

@test "invoke rmt-docker-volume.sh - copy - vol - with pwd - with sudo - warning - cleanup failure" {

  # source ${__scripts_dir}/rmt-docker-volumes.dotfile

  __sshpass_cmd=mock-ssh-docker-volume-inspect-single-volume-json-cleanup-error
  __rsync_cmd=mock-cmd-ok-no-output

  run run-rmt-docker-volume sudo copy --verbose 'vol://test-server/test-volume' <<< 'testpwd'
  # printf '# v %s\n' "${lines[@]}" >&3

  [ ${status} -eq 0 ]
  [ "${lines[0]}" == 'copy volume [test-volume] from remote host [test-server] to local directory ['"${__rmt_docker_volume_local_copy_path}"']' ]
  [ "${lines[2]}" == 'warning: could not delete data folder [./volume-copies] on host [test-server] during cleanup' ]
}

@test "invoke rmt-docker-volume.sh - copy - vol - with pwd - without sudo - error - ssh - rsync failure" {

  # source ${__scripts_dir}/rmt-docker-volumes.dotfile

  __sshpass_cmd=mock-ssh-docker-volume-inspect-single-volume-json
  __rsync_cmd=mock-cmd-error

  run run-rmt-docker-volume copy --verbose 'vol://test-server/test-volume' <<< 'testpwd'
  # printf '# v %s\n' "${lines[@]}" >&3

  [ ${status} -eq 1 ]
  [ "${lines[0]}" == 'copy volume [test-volume] from remote host [test-server] to local directory ['"${__rmt_docker_volume_local_copy_path}"']' ]
  [ "${lines[2]}" == 'error: rsync data from host [test-server:./volume-copies] to local directory ['"${__rmt_docker_volume_local_copy_path}"'] failed' ]
}

@test "invoke rmt-docker-volume.sh - copy - vol - with pwd - with sudo - error - ssh - rsync failure" {

  # source ${__scripts_dir}/rmt-docker-volumes.dotfile

  __sshpass_cmd=mock-ssh-docker-volume-inspect-single-volume-json
  __rsync_cmd=mock-cmd-error

  run run-rmt-docker-volume sudo copy --verbose 'vol://test-server/test-volume' <<< 'testpwd'
  # printf '# v %s\n' "${lines[@]}" >&3

  [ ${status} -eq 1 ]
  [ "${lines[0]}" == 'copy volume [test-volume] from remote host [test-server] to local directory ['"${__rmt_docker_volume_local_copy_path}"']' ]
  [ "${lines[2]}" == 'error: rsync data from host [test-server:./volume-copies] to local directory ['"${__rmt_docker_volume_local_copy_path}"'] failed' ]
}


########################

@test "invoke rmt-docker-volume.sh - copy - vol - with pwd - without sudo - callstack" {

  __sshpass_cmd=mock-ssh-docker-inspect-single-volume-json-callstack
  __rsync_cmd=mock-cmd-ok-print-args

  run run-rmt-docker-volume copy --verbose 'vol://test-server/test-volume' <<< 'testpwd'
  # printf '# v %s\n' "${lines[@]}" >&3

  [ ${status} -eq 0 ]
  [ "${lines[2]}" == '-P ass ssh test-server [[ ! -d ./volume-copies ]] && mkdir -p ./volume-copies; cp -r -v -a /mount/path/on/remote/host ./volume-copies; chmod -R +r ./volume-copies' ]
  [ "${lines[3]}" == '-avz --update --progress -e sshpass -e -P ass ssh test-server:./volume-copies '"${__rmt_docker_volume_local_copy_path}" ]
  [ "${lines[4]}" == '-P ass ssh test-server rm -r -f ./volume-copies' ]
}

@test "invoke rmt-docker-volume.sh - copy - vol - with pwd - with sudo - callstack" {

  __sshpass_cmd=mock-ssh-docker-inspect-single-volume-json-callstack
  __rsync_cmd=mock-cmd-ok-print-args

  run run-rmt-docker-volume sudo copy --verbose 'vol://test-server/test-volume' <<< 'testpwd'
  # printf '# v %s\n' "${lines[@]}" >&3

  [ ${status} -eq 0 ]
  [ "${lines[2]}" == '-P ass ssh test-server [[ ! -d ./volume-copies ]] && mkdir -p ./volume-copies; sudo cp -r -v -a /mount/path/on/remote/host ./volume-copies; sudo chmod -R +r ./volume-copies' ]
  [ "${lines[3]}" == '-avz --update --progress -e sshpass -e -P ass ssh test-server:./volume-copies '"${__rmt_docker_volume_local_copy_path}" ]
  [ "${lines[4]}" == '-P ass ssh test-server sudo rm -r -f ./volume-copies' ]
}

@test "invoke rmt-docker-volume.sh - copy - servol - with pwd - without sudo - callstack" {

  __sshpass_cmd=mock-ssh-docker-service-inspect-single-volume-json-callstack
  __rsync_cmd=mock-cmd-ok-print-args

  run run-rmt-docker-volume copy --verbose 'servol://test-server/test-service/test-volume' <<< 'testpwd'
  # printf '# v %s\n' "${lines[@]}" >&3

  [ ${status} -eq 0 ]
  [ "${lines[2]}" == '-P ass ssh test-server [[ ! -d ./volume-copies ]] && mkdir -p ./volume-copies; docker cp -a 1234:/some/path/in/volume ./volume-copies' ]
  [ "${lines[3]}" == '-avz --update --progress -e sshpass -e -P ass ssh test-server:./volume-copies '"${__rmt_docker_volume_local_copy_path}" ]
  [ "${lines[4]}" == '-P ass ssh test-server rm -r -f ./volume-copies' ]
}

@test "invoke rmt-docker-volume.sh - copy - servol - with pwd - with sudo - callstack" {

  skip "not yet clear where sudo is necessary"

  __sshpass_cmd=mock-ssh-docker-service-inspect-single-volume-json-callstack
  __rsync_cmd=mock-cmd-ok-print-args

  run run-rmt-docker-volume sudo copy --verbose 'servol://test-server/test-service/test-volume' <<< 'testpwd'
  # printf '# v %s\n' "${lines[@]}" >&3

  [ ${status} -eq 0 ]
  [ "${lines[2]}" == '-P ass ssh test-server [[ ! -d ./volume-copies ]] && mkdir -p ./volume-copies; docker cp -a 1234:/some/path/in/volume ./volume-copies' ]
  [ "${lines[3]}" == '-avz --update --progress -e sshpass -e -P ass ssh test-server:./volume-copies '"${__rmt_docker_volume_local_copy_path}" ]
  [ "${lines[4]}" == '-P ass ssh test-server rm -r -f ./volume-copies' ]
}

@test "invoke rmt-docker-volume.sh - copy - vol - with pwd - fullpath - without sudo - callstack" {

  __sshpass_cmd=mock-ssh-docker-inspect-single-volume-json-callstack
  __rsync_cmd=mock-cmd-ok-print-args

  run run-rmt-docker-volume copy --verbose --fp 'vol://test-server/test-volume' <<< 'testpwd'
  # printf '# v %s\n' "${lines[@]}" >&3

  [ ${status} -eq 0 ]
  [ "${lines[2]}" == '-P ass ssh test-server [[ ! -d ./volume-copies/test-server/test-volume ]] && mkdir -p ./volume-copies/test-server/test-volume; cp -r -v -a /mount/path/on/remote/host ./volume-copies/test-server/test-volume; chmod -R +r ./volume-copies/test-server/test-volume' ]
  [ "${lines[3]}" == '-avz --update --progress -e sshpass -e -P ass ssh test-server:./volume-copies/test-server/test-volume '"${__rmt_docker_volume_local_copy_path}"'/test-server/test-volume' ]
  [ "${lines[4]}" == '-P ass ssh test-server rm -r -f ./volume-copies/test-server/test-volume' ]
}

@test "invoke rmt-docker-volume.sh - copy - vol - with pwd - fullpath - with sudo - callstack" {
  
  __sshpass_cmd=mock-ssh-docker-inspect-single-volume-json-callstack
  __rsync_cmd=mock-cmd-ok-print-args

  run run-rmt-docker-volume sudo copy --verbose --fp 'vol://test-server/test-volume' <<< 'testpwd'
  # printf '# v %s\n' "${lines[@]}" >&3

  [ ${status} -eq 0 ]
  [ "${lines[2]}" == '-P ass ssh test-server [[ ! -d ./volume-copies/test-server/test-volume ]] && mkdir -p ./volume-copies/test-server/test-volume; sudo cp -r -v -a /mount/path/on/remote/host ./volume-copies/test-server/test-volume; sudo chmod -R +r ./volume-copies/test-server/test-volume' ]
  [ "${lines[3]}" == '-avz --update --progress -e sshpass -e -P ass ssh test-server:./volume-copies/test-server/test-volume '"${__rmt_docker_volume_local_copy_path}"'/test-server/test-volume' ]
  [ "${lines[4]}" == '-P ass ssh test-server sudo rm -r -f ./volume-copies/test-server/test-volume' ]
}

@test "invoke rmt-docker-volume.sh - copy - servol - with pwd - without sudo  - fullpath - callstack" {

  __sshpass_cmd=mock-ssh-docker-service-inspect-single-volume-json-callstack
  __rsync_cmd=mock-cmd-ok-print-args

  run run-rmt-docker-volume copy --verbose --fp  'servol://test-server/test-service/test-volume' <<< 'testpwd'
  # printf '# v %s\n' "${lines[@]}" >&3

  [ ${status} -eq 0 ]
  [ "${lines[2]}" == '-P ass ssh test-server [[ ! -d ./volume-copies/test-service/test-volume ]] && mkdir -p ./volume-copies/test-service/test-volume; docker cp -a 1234:/some/path/in/volume ./volume-copies/test-service/test-volume' ]
  [ "${lines[3]}" == '-avz --update --progress -e sshpass -e -P ass ssh test-server:./volume-copies/test-service/test-volume '"${__rmt_docker_volume_local_copy_path}"'/test-server/test-service/test-volume' ]
  [ "${lines[4]}" == '-P ass ssh test-server rm -r -f ./volume-copies/test-service/test-volume' ]
}

@test "invoke rmt-docker-volume.sh - copy - servol - with pwd - with sudo  - fullpath - callstack" {

  skip "not yet clear where sudo is necessary"

  __sshpass_cmd=mock-ssh-docker-service-inspect-single-volume-json-callstack
  __rsync_cmd=mock-cmd-ok-print-args

  run run-rmt-docker-volume sudo copy --verbose --fp  'servol://test-server/test-service/test-volume' <<< 'testpwd'
  # printf '# v %s\n' "${lines[@]}" >&3

  [ ${status} -eq 0 ]
  [ "${lines[2]}" == '-P ass ssh test-server [[ ! -d ./volume-copies/test-service/test-volume ]] && mkdir -p ./volume-copies/test-service/test-volume; docker cp -a 1234:/some/path/in/volume ./volume-copies/test-service/test-volume' ]
  [ "${lines[3]}" == '-avz --update --progress -e sshpass -e -P ass ssh test-server:./volume-copies/test-service/test-volume '"${__rmt_docker_volume_local_copy_path}"'test-server/test-service/test-volume' ]
  [ "${lines[4]}" == '-P ass ssh test-server rm -r -f ./volume-copies/test-service/test-volume' ]
}

########################

@test "invoke rmt-docker-volume.sh - list - servol - data - with pwd - callstack" {

  __sshpass_cmd=mock-ssh-docker-service-inspect-single-volume-json-callstack

  run run-rmt-docker-volume list-data 'servol://test-server/test-service' <<< 'testpwd'
  # printf '# l1 %s\n' "${lines[@]}" >&3

  [ ${status} -eq 0 ]
  [ "${lines[0]}" == '-P ass ssh test-server ls -la /mount/path/on/remote/host' ]
  
  run run-rmt-docker-volume sudo list-data 'servol://test-server/test-service' <<< 'testpwd'
  # printf '# l2 %s\n' "${lines[@]}" >&3

  [ ${status} -eq 0 ]
  [ "${lines[0]}" == '-P ass ssh test-server sudo ls -la /mount/path/on/remote/host' ]
  
  run run-rmt-docker-volume list-data --verbose 'servol://test-server/test-service' <<< 'testpwd'
  # printf '# l3 %s\n' "${lines[@]}" >&3

  [ ${status} -eq 0 ]
  [ "${lines[0]}" == 'list volume data for [test-service:test-volume]' ]
  [ "${lines[2]}" == '-P ass ssh test-server ls -la /mount/path/on/remote/host' ]
  
  run run-rmt-docker-volume sudo list-data --verbose 'servol://test-server/test-service' <<< 'testpwd'
  # printf '# l4 %s\n' "${lines[@]}" >&3

  [ ${status} -eq 0 ]
  [ "${lines[0]}" == 'list volume data for [test-service:test-volume]' ]
  [ "${lines[2]}" == '-P ass ssh test-server sudo ls -la /mount/path/on/remote/host' ]
  
  __sshpass_cmd=mock-ssh-docker-service-inspect-multi-volume-json-callstack
  
  run run-rmt-docker-volume sudo list-data --verbose 'servol://test-server/test-service/test-volume-abc' 'servol://test-server/test-service/test-volume-xyz' <<< 'testpwd'
  # printf '# l5 %s\n' "${lines[@]}" >&3

  [ ${status} -eq 0 ]
  [ "${lines[0]}" == 'list volume data for [test-service:test-volume-abc]' ]
  [ "${lines[2]}" == '-P ass ssh test-server sudo ls -la /mount/path/on/remote/host/abc' ]
  [ "${lines[4]}" == 'list volume data for [test-service:test-volume-xyz]' ]
  [ "${lines[6]}" == '-P ass ssh test-server sudo ls -la /mount/path/on/remote/host/xyz' ]
}

@test "invoke rmt-docker-volume.sh - list - servol - data - no pwd - callstack" {

  __ssh_cmd=mock-ssh-docker-service-inspect-single-volume-json-callstack

  run run-rmt-docker-volume list-data 'servol://test-server/test-service'
  # printf '# l1 %s\n' "${lines[@]}" >&3

  [ ${status} -eq 0 ]
  [ "${lines[0]}" == 'test-server ls -la /mount/path/on/remote/host' ]

  run run-rmt-docker-volume sudo list-data 'servol://test-server/test-service'
  # printf '# l2 %s\n' "${lines[@]}" >&3

  [ ${status} -eq 0 ]
  [ "${lines[0]}" == 'test-server sudo ls -la /mount/path/on/remote/host' ]
  
  run run-rmt-docker-volume list-data --verbose 'servol://test-server/test-service'
  # printf '# l3 %s\n' "${lines[@]}" >&3

  [ ${status} -eq 0 ]
  [ "${lines[0]}" == 'list volume data for [test-service:test-volume]' ]
  [ "${lines[2]}" == 'test-server ls -la /mount/path/on/remote/host' ]
  
  run run-rmt-docker-volume sudo list-data --verbose 'servol://test-server/test-service'
  # printf '# l4 %s\n' "${lines[@]}" >&3

  [ ${status} -eq 0 ]
  [ "${lines[0]}" == 'list volume data for [test-service:test-volume]' ]
  [ "${lines[2]}" == 'test-server sudo ls -la /mount/path/on/remote/host' ]
  
  __sshpass_cmd=mock-ssh-docker-service-inspect-multi-volume-json-callstack
  
  run run-rmt-docker-volume sudo list-data --verbose 'servol://test-server/test-service/test-volume-abc' 'servol://test-server/test-service/test-volume-xyz' <<< 'testpwd'
  # printf '# l5 %s\n' "${lines[@]}" >&3

  [ ${status} -eq 0 ]
  [ "${lines[0]}" == 'list volume data for [test-service:test-volume-abc]' ]
  [ "${lines[2]}" == '-P ass ssh test-server sudo ls -la /mount/path/on/remote/host/abc' ]
  [ "${lines[4]}" == 'list volume data for [test-service:test-volume-xyz]' ]
  [ "${lines[6]}" == '-P ass ssh test-server sudo ls -la /mount/path/on/remote/host/xyz' ]
}

@test "invoke rmt-docker-volume.sh - list - vol - data - with pwd - callstack" {

  __sshpass_cmd=mock-ssh-docker-inspect-single-volume-json-callstack

  run run-rmt-docker-volume list-data 'vol://test-server/test-volume' <<< 'testpwd'
  # printf '# l1 %s\n' "${lines[@]}" >&3

  [ ${status} -eq 0 ]
  [ "${lines[0]}" == '-P ass ssh test-server ls -la /mount/path/on/remote/host' ]
  
  run run-rmt-docker-volume sudo list-data 'vol://test-server/test-volume' <<< 'testpwd'
  # printf '# l2 %s\n' "${lines[@]}" >&3

  [ ${status} -eq 0 ]
  [ "${lines[0]}" == '-P ass ssh test-server sudo ls -la /mount/path/on/remote/host' ]
  
  run run-rmt-docker-volume list-data --verbose 'vol://test-server/test-volume' <<< 'testpwd'
  # printf '# l3 %s\n' "${lines[@]}" >&3

  [ ${status} -eq 0 ]
  [ "${lines[0]}" == 'list volume data for [test-volume]' ]
  [ "${lines[2]}" == '-P ass ssh test-server ls -la /mount/path/on/remote/host' ]
  
  run run-rmt-docker-volume sudo list-data --verbose 'vol://test-server/test-volume' <<< 'testpwd'
  # printf '# l4 %s\n' "${lines[@]}" >&3

  [ ${status} -eq 0 ]
  [ "${lines[0]}" == 'list volume data for [test-volume]' ]
  [ "${lines[2]}" == '-P ass ssh test-server sudo ls -la /mount/path/on/remote/host' ]
  
  __sshpass_cmd=mock-ssh-docker-inspect-multi-volume-json-callstack
  
  run run-rmt-docker-volume sudo list-data --verbose 'vol://test-server/test-volume-abc' 'vol://test-server/test-volume-xyz' <<< 'testpwd'
  # printf '# l5 %s\n' "${lines[@]}" >&3

  [ ${status} -eq 0 ]
  [ "${lines[0]}" == 'list volume data for [test-volume-abc]' ]
  [ "${lines[2]}" == '-P ass ssh test-server sudo ls -la /mount/path/on/remote/host/abc' ]
  [ "${lines[4]}" == 'list volume data for [test-volume-xyz]' ]
  [ "${lines[6]}" == '-P ass ssh test-server sudo ls -la /mount/path/on/remote/host/xyz' ]
}

@test "invoke rmt-docker-volume.sh - list - vol - data - no pwd - callstack" {

  __ssh_cmd=mock-ssh-docker-inspect-single-volume-json-callstack

  run run-rmt-docker-volume list-data 'vol://test-server/test-volume'
  # printf '# l1 %s\n' "${lines[@]}" >&3

  [ ${status} -eq 0 ]
  [ "${lines[0]}" == 'test-server ls -la /mount/path/on/remote/host' ]

  run run-rmt-docker-volume sudo list-data 'vol://test-server/test-volume'
  # printf '# l2 %s\n' "${lines[@]}" >&3

  [ ${status} -eq 0 ]
  [ "${lines[0]}" == 'test-server sudo ls -la /mount/path/on/remote/host' ]
  
  run run-rmt-docker-volume list-data --verbose 'vol://test-server/test-volume'
  # printf '# l3 %s\n' "${lines[@]}" >&3

  [ ${status} -eq 0 ]
  [ "${lines[0]}" == 'list volume data for [test-volume]' ]
  [ "${lines[2]}" == 'test-server ls -la /mount/path/on/remote/host' ]
  
  run run-rmt-docker-volume sudo list-data --verbose 'vol://test-server/test-volume'
  # printf '# l4 %s\n' "${lines[@]}" >&3

  [ ${status} -eq 0 ]
  [ "${lines[0]}" == 'list volume data for [test-volume]' ]
  [ "${lines[2]}" == 'test-server sudo ls -la /mount/path/on/remote/host' ]
}

@test "invoke rmt-docker-volume.sh - list - vol - data - no pwd - multi - callstack" {

  __ssh_cmd=mock-ssh-docker-inspect-multi-volume-json-callstack

  run run-rmt-docker-volume list-data --verbose 'vol://test-server/test-volume-abc' 'vol://test-server/test-volume-xyz'
  # printf '# l1 %s\n' "${lines[@]}" >&3

  [ ${status} -eq 0 ]
  [ "${lines[0]}" == 'list volume data for [test-volume-abc]' ]
  [ "${lines[2]}" == 'test-server ls -la /mount/path/on/remote/host/abc' ]
  [ "${lines[4]}" == 'list volume data for [test-volume-xyz]' ]
  [ "${lines[6]}" == 'test-server ls -la /mount/path/on/remote/host/xyz' ]
  
  run run-rmt-docker-volume sudo list-data --verbose 'vol://test-server/test-volume-abc' 'vol://test-server/test-volume-xyz'
  # printf '# l2 %s\n' "${lines[@]}" >&3

  [ ${status} -eq 0 ]
  [ "${lines[0]}" == 'list volume data for [test-volume-abc]' ]
  [ "${lines[2]}" == 'test-server sudo ls -la /mount/path/on/remote/host/abc' ]
  [ "${lines[4]}" == 'list volume data for [test-volume-xyz]' ]
  [ "${lines[6]}" == 'test-server sudo ls -la /mount/path/on/remote/host/xyz' ]
}

@test "invoke rmt-docker-volume.sh - list - servol - data - no pwd - multi - callstack" {

  __ssh_cmd=mock-ssh-docker-service-inspect-multi-volume-json-callstack

  run run-rmt-docker-volume list-data --verbose 'servol://test-server/test-service/test-volume-abc' 'servol://test-server/test-service/test-volume-xyz'
  # printf '# l %s\n' "${lines[@]}" >&3

  [ ${status} -eq 0 ]
  [ "${lines[0]}" == 'list volume data for [test-service:test-volume-abc]' ]
  [ "${lines[2]}" == 'test-server ls -la /mount/path/on/remote/host/abc' ]
  [ "${lines[4]}" == 'list volume data for [test-service:test-volume-xyz]' ]
  [ "${lines[6]}" == 'test-server ls -la /mount/path/on/remote/host/xyz' ]
}

@test "invoke rmt-docker-volume.sh - list - vol and servol - data - no pwd - multi - callstack" {

  __ssh_cmd=mock-ssh-docker-service-and-volume-inspect-multi-volume-json-callstack
  
  run run-rmt-docker-volume list-data --verbose 'servol://test-server/test-service/test-volume-abc' 'vol://test-server/test-volume-xyz'
  # printf '# l1 %s\n' "${lines[@]}" >&3

  [ ${status} -eq 0 ]
  [ "${lines[0]}" == 'list volume data for [test-service:test-volume-abc]' ]
  [ "${lines[2]}" == 'test-server ls -la /mount/path/on/remote/host/abc' ]
  [ "${lines[4]}" == 'list volume data for [test-volume-xyz]' ]
  [ "${lines[6]}" == 'test-server ls -la /mount/path/on/remote/host/xyz' ]
}

@test "invoke rmt-docker-volume.sh - list - vol and servol - data - with pwd - callstack - error" {

  __sshpass_cmd=mock-cmd-ok-no-output

  # no inspect json returned by mock at all
  run run-rmt-docker-volume sudo list-data --verbose 'servol://test-server/test-volume-abc' 'vol://test-server/test-volume-xyz' <<< 'testpwd'
  
  # printf '# l1 %s\n' "${lines[@]}" >&3
  [ ${status} -eq 1 ]
  [ "${lines[0]}" == 'error: empty inspect json' ]

  __sshpass_cmd=mock-ssh-docker-service-and-volume-inspect-multi-service-volume-only-json-callstack
  
  # no inspect json returned by mock for standalone volum test-volume-xyz
  run run-rmt-docker-volume sudo list-data --verbose 'servol://test-server/test-service/test-volume-abc' 'vol://test-server/test-volume-xyz' <<< 'testpwd'
  
  # printf '# l2 %s\n' "${lines[@]}" >&3
  [ ${status} -eq 2 ]
  [ "${lines[0]}" == 'list volume data for [test-service:test-volume-abc]' ]
  [ "${lines[2]}" == '-P ass ssh test-server sudo ls -la /mount/path/on/remote/host/abc' ]
  [ "${lines[4]}" == 'error: empty inspect json' ]

  __sshpass_cmd=mock-ssh-docker-service-and-volume-inspect-multi-volume-json-callstack

  # mocked service inspect json contains 'test-service' as service name but the macthed service 
  # name is supposed to be called 'test-service-abc'
  run run-rmt-docker-volume sudo list-data --verbose 'servol://test-server/test-service-abc' 'vol://test-server/test-volume-xyz' <<< 'testpwd'

  # printf '# l3 %s\n' "${lines[@]}" >&3
  [ ${status} -eq 2 ]
  [ "${lines[0]}" == 'error: json field [.[].Name] is not set to [test-service-abc] but [test-service]' ]

  __sshpass_cmd=mock-ssh-docker-service-and-volume-inspect-multi-standalone-volume-only-json-callstack
  
  # no inspect json is returned for servol uri
  run run-rmt-docker-volume sudo list-data --verbose 'vol://test-server/test-volume-abc' 'servol://test-server/test-service/test-volume-xyz' <<< 'testpwd'  

  # printf '# l4 %s\n' "${lines[@]}" >&3
  [ ${status} -eq 2 ]
  [ "${lines[0]}" == 'list volume data for [test-volume-abc]' ]
  [ "${lines[2]}" == '-P ass ssh test-server sudo ls -la /mount/path/on/remote/host/abc' ]
  [ "${lines[4]}" == 'error: empty inspect json' ]
  
  __sshpass_cmd=mock-ssh-docker-service-and-volume-inspect-multi-service-volume-only-json-callstack

  # no inspect json is returned for vol uti
  run run-rmt-docker-volume sudo list-data --verbose 'vol://test-server/test-service/test-volume-abc' 'servol://test-server/test-service/test-volume-xyz' <<< 'testpwd'
  
  # printf '# l5 %s\n' "${lines[@]}" >&3
  [ ${status} -eq 2 ]
  [ "${lines[0]}" == 'error: empty inspect json' ]
}

@test "invoke rmt-docker-volume.sh - rm - vol - no pwd - without sudo - callstack" {

  skip "remove not yet implemented"

  __ssh_cmd=mock-ssh-docker-inspect-single-volume-json-callstack

  run run-rmt-docker-volume rm 'vol://test-server/test-volume'
  # printf '# l1 %s\n' "${lines[@]}" >&3

  [ ${status} -eq 0 ]
  [ "${lines[0]}" == 'no yet clear' ]
  
  run run-rmt-docker-volume rm 'servol://test-server/test-service'
  # printf '# l1 %s\n' "${lines[@]}" >&3

  [ ${status} -eq 0 ]
  [ "${lines[0]}" == 'no yet clear' ]
}

@test "invoke rmt-docker-volume.sh - rm - vol - no pwd - with sudo - callstack" {

  skip "remove not yet implemented"

  __ssh_cmd=mock-ssh-docker-inspect-single-volume-json-callstack

  run run-rmt-docker-volume sudo rm 'vol://test-server/test-volume'
  # printf '# l1 %s\n' "${lines[@]}" >&3

  [ ${status} -eq 0 ]
  [ "${lines[0]}" == 'no yet clear' ]
  
  run run-rmt-docker-volume sudo rm 'servol://test-server/test-service'
  # printf '# l1 %s\n' "${lines[@]}" >&3

  [ ${status} -eq 0 ]
  [ "${lines[0]}" == 'no yet clear' ]
}
