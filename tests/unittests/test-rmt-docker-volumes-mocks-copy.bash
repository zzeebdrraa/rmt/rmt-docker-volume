#!/usr/bin/env bash

# Copyright (C) 2023 zeebdrraa
#
# This file is part of rmt-docker-volumes.
#
# rmt-docker-volumes is free software: you can redistribute it and/or
# modify it under the terms of the GNU General Public License as published
# by the Free Software Foundation, either version 3 of the License, or 
# (at your option) any later version.
#
# rmt-docker-volumes is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
# Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with rmt-docker-volumes. If not, see <https://www.gnu.org/licenses/>. 

############################
# DEPENDENCIES
############################

# the following file must have been sourced before using functions of this file:
#
# - test-rmt-docker-volumes-mocks-common
#

############################
# MOCKS 
############################
# for volumes copy command

mock-ssh-docker-service-inspect-single-volume-json-docker-copy-volume-error() {
  [[ "$*" =~ (.*docker cp -a*) ]] && return 1
  mock-ssh-docker-service-inspect-single-volume-json "$@"
  return 0
}

mock-ssh-docker-service-inspect-single-volume-json-cleanup-error() {
  [[ "$*" =~ (.*sudo rm -r -f*) ]] && return 1
  [[ "$*" =~ (.*rm -r -f*) ]] && return 1
  mock-ssh-docker-service-inspect-single-volume-json "$@"
  return 0
}

mock-ssh-docker-volume-inspect-single-volume-json-copy-volume-error() {
  [[ "$*" =~ (.*sudo cp -r -v -a*) ]] && return 1
  [[ "$*" =~ (.*cp -r -v -a*) ]] && return 1
  mock-ssh-docker-volume-inspect-single-volume-json "$@"
  return 0
}

mock-ssh-docker-volume-inspect-single-volume-json-cleanup-error() {
  [[ "$*" =~ (.*sudo rm -r -f*) ]] && return 1
  [[ "$*" =~ (.*rm -r -f*) ]] && return 1
  mock-ssh-docker-volume-inspect-single-volume-json "$@"
  return 0
}

sshpass-mock-docker-inspect-multi-volume-json() {
  [[ "$*" =~ (.*docker volume inspect test-volume-abc) ]] && printf '%s' "${__docker_inspect_volume_single_abc_json}" && return 0
  [[ "$*" =~ (.*docker volume inspect test-volume-xyz) ]] && printf '%s' "${__docker_inspect_volume_single_xyz_json}" && return 0

  [[ "$*" =~ (.*docker volume inspect*) ]] && printf '%s' "${__docker_inspect_volume_multi_json}"
  return 0
}

mock-ssh-docker-service-inspect-single-volume-json-callstack() {
  mock-ssh-docker-service-inspect-single-volume-json "$@"
  [[ ! "$*" =~ (.*docker inspect*) ]] && mock-cmd-ok-print-args "$@"
  return 0
}

mock-ssh-docker-service-inspect-multi-volume-json-callstack() {
  mock-ssh-docker-service-inspect-multi-volume-json "$@"
  [[ ! "$*" =~ (.*docker inspect*) ]] && mock-cmd-ok-print-args "$@"
  return 0
}

mock-ssh-docker-inspect-single-volume-json-callstack() {
  mock-ssh-docker-volume-inspect-single-volume-json "$@"
  [[ ! "$*" =~ (.*docker volume inspect*) ]] && mock-cmd-ok-print-args "$@"
  return 0
}

mock-ssh-docker-inspect-multi-volume-json-callstack() {
  mock-ssh-docker-volume-inspect-multi-volume-json "$@"
  [[ ! "$*" =~ (.*docker volume inspect*) ]] && mock-cmd-ok-print-args "$@"
  return 0
}

mock-ssh-docker-service-and-volume-inspect-multi-volume-json-callstack() {

  mock-ssh-docker-volume-inspect-multi-volume-json "$@"
  mock-ssh-docker-service-inspect-multi-volume-json "$@"
  # catch both >docker service inspect and >docker inspect 
  [[ ! "$*" =~ (.*docker.*inspect*) ]] && mock-cmd-ok-print-args "$@" && return 0
  return 0
}

mock-ssh-docker-service-and-volume-inspect-multi-service-volume-only-json-callstack() {

  mock-ssh-docker-service-inspect-multi-volume-json "$@"
  # catch both >docker service inspect and >docker inspect 
  [[ ! "$*" =~ (.*docker.*inspect*) ]] && mock-cmd-ok-print-args "$@" && return 0
  return 0
}

mock-ssh-docker-service-and-volume-inspect-multi-standalone-volume-only-json-callstack() {

  mock-ssh-docker-volume-inspect-multi-volume-json "$@"
  # catch both >docker service inspect and >docker inspect 
  [[ ! "$*" =~ (.*docker.*inspect*) ]] && mock-cmd-ok-print-args "$@" && return 0
  return 0
}