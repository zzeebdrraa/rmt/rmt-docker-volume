#!/usr/bin/env bats

# Copyright (C) 2023 zeebdrraa
#
# This file is part of rmt-docker-volumes.
#
# rmt-docker-volumes is free software: you can redistribute it and/or
# modify it under the terms of the GNU General Public License as published
# by the Free Software Foundation, either version 3 of the License, or 
# (at your option) any later version.
#
# rmt-docker-volumes is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
# Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with rmt-docker-volumes. If not, see <https://www.gnu.org/licenses/>. 

############################
# INCLUDES
############################

# currently, dotfiles to test reside in the same directory as bats files
declare -g -r __source_dir="${BATS_TEST_DIRNAME}"

load "${__source_dir}/../../rmt-docker-volumes-common.dotfile"
load "${__source_dir}/../../../common/types.dotfile"

############################
# TESTS
############################

@test "invoke rmt-docker-volume-common - verify-volume-uri - no args" {

  # missing uri
  run verify-volume-uri
  # printf '# l %s\n' "${lines[@]}" >&3

  [ ${status} -eq 1 ]
  [ "${output}" == 'error: no uri provided' ]
}

@test "invoke rmt-docker-volume-common - verify-volume-uri - no volume uri" {

  run verify-volume-uri http://example.com/volume
  # printf '# l1 %s\n' "${lines[@]}" >&3

  [ ${status} -eq 1 ]
  [ "${output}" == 'error: invalid uri scheme [http]. must be [vol] or [servol].' ]

  run verify-volume-uri www.example.com/volume
  # printf '# l2 %s\n' "${lines[@]}" >&3

  [ ${status} -eq 1 ]
  [ "${output}" == 'error: invalid scheme [www.example.com/volume] in uri [www.example.com/volume]' ]

  run verify-volume-uri vvol://example.com/volume
  # printf '# l3 %s\n' "${lines[@]}" >&3

  [ ${status} -eq 1 ]
  [ "${output}" == 'error: invalid uri scheme [vvol]. must be [vol] or [servol].' ]
}

@test "invoke rmt-docker-volume-common - verify-volume-uri - with volume-uri - valid volume" {

  # a particular volume or volume pattern
  run verify-volume-uri vol://example.com/volume
  # printf '# l1 %s\n' "${lines[@]}" >&3

  [ ${status} -eq 0 ]
  [ -z "${output}" ]

  # all volumes that have not been created as part of a particular service
  run verify-volume-uri vol://example.com
  # printf '# l2 %s\n' "${lines[@]}" >&3

  [ ${status} -eq 0 ]
  [ -z "${output}" ]
}

@test "invoke rmt-docker-volume-common - verify-volume-uri - with volume-uri - invalid volume - too long" {

  skip 'maybe no error'

  # a particular volume or volume pattern
  run verify-volume-uri 'vol://example.com/volume/unexpected'
  # printf '# l %s\n' "${lines[@]}" >&3

  [ ${status} -eq 1 ]
  [ "${output}" == 'error: uri [vol://example.com/volume/unexpected] contains an invalid number of parts [4 > 3]' ]
}

@test "invoke rmt-docker-volume-common - verify-volume-uri - with volume-uri - invalid volume - too short" {

  run verify-volume-uri 'vol://'
  # printf '# l %s\n' "${lines[@]}" >&3

  [ ${status} -eq 1 ]
  [ "${output}" == 'error: uri [vol://] has no path' ]
}

@test "invoke rmt-docker-volume-common - verify-volume-uri - with volume-uri - valid service volume" {

  # a particular volume or volume pattern
  run verify-volume-uri 'servol://example.com/service'
  # printf '# l1 %s\n' "${lines[@]}" >&3

  [ ${status} -eq 0 ]
  [ -z "${output}" ]

  run verify-volume-uri 'servol://example.com/service/volume'
  # printf '# l2 %s\n' "${lines[@]}" >&3

  [ ${status} -eq 0 ]
  [ -z "${output}" ]
}

@test "invoke rmt-docker-volume-common - verify-volume-uri - with volume-uri - invalid service volume - too long" {

  skip 'maybe no error'

  # a particular volume or volume pattern
  run verify-volume-uri 'servol://example.com/service/volume/unexpected'
  # printf '# l %s\n' "${lines[@]}" >&3

  [ ${status} -eq 1 ]
  [ "${output}" == 'error: uri [servol://example.com/service/volume/unexpected] contains an invalid number of parts [5 > 4]' ]
}

@test "invoke rmt-docker-volume-common - verify-volume-uri - with volume-uri - invalid service volume - too short" {

  run verify-volume-uri 'servol://example.com'
  # printf '# l %s\n' "${lines[@]}" >&3

  [ ${status} -eq 1 ]
  [ "${output}" == 'error: uri [servol://example.com] contains an invalid number of parts [2 < 3]' ]
}

###########

@test "invoke rmt-docker-volume-common - split-uri - no args" {

  # missing uri
  run split-uri
  # printf '# l %s\n' "${lines[@]}" >&3

  [ ${status} -eq 1 ]
  [ "${output}" == 'error: no uri provided' ]
}

@test "invoke rmt-docker-volume-common - split-uri - valid uri" {

  run split-uri http://example.com/service
  # printf '# l %s\n' "${lines[@]}" >&3

  [ ${status} -eq 0 ]
  [ "${lines[0]}" == 'http' ]
  [ "${lines[1]}" == 'example.com' ]
  [ "${lines[2]}" == 'service' ]

  run split-uri vvol://example.com/service
  # printf '# l %s\n' "${lines[@]}" >&3

  [ ${status} -eq 0 ]
  [ "${lines[0]}" == 'vvol' ]
  [ "${lines[1]}" == 'example.com' ]
  [ "${lines[2]}" == 'service' ]

}

@test "invoke rmt-docker-volume-common - split-uri - incomplete uri scheme" {

  run split-uri '://example.com/service'
  # printf '# l %s\n' "${lines[@]}" >&3

  [ ${status} -eq 1 ]
  [ "${output}" == 'error: invalid scheme [] in uri [://example.com/service]' ]
}

@test "invoke rmt-docker-volume-common - split-uri - no uri path" {

  run split-uri 'file://'
  # printf '# l %s\n' "${lines[@]}" >&3

  [ ${status} -eq 1 ]
  [ "${output}" == 'error: uri [file://] has no path' ]
}

@test "invoke rmt-docker-volume-common - split-uri - invalid uri format" {

  skip 'no clear yet if a whitespace in uri path has to be considerd as error'

  run split-uri 'file://path with/whitespace'
  printf '# l %s\n' "${lines[@]}" >&3

  [ ${status} -eq 1 ]
  [ "${output}" == 'error: uri [file://] has no path' ]
}

@test "invoke rmt-docker-volume-common - split-uri - invalid uri scheme" {

  local -r invalidUriSchemeList=(
    ':/example.com/service'
    '//example.com/service'
    '/example.com/service'
    ':example.com/service'
    'example.com/service'
    'www.example.com/service'
  )
  
  for invalidUri in "${invalidUriSchemeList[@]}"; do
    run split-uri "${invalidUri}"
    # printf '# l %s\n' "${lines[@]}" >&3

    [ ${status} -eq 1 ]
    [ "${output}" == 'error: invalid scheme ['"${invalidUri}"'] in uri ['"${invalidUri}"']' ]
  done
}