#!/usr/bin/env bats

# Copyright (C) 2023 zeebdrraa
#
# This file is part of rmt-docker-volumes.
#
# rmt-docker-volumes is free software: you can redistribute it and/or
# modify it under the terms of the GNU General Public License as published
# by the Free Software Foundation, either version 3 of the License, or 
# (at your option) any later version.
#
# rmt-docker-volumes is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
# Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with rmt-docker-volumes. If not, see <https://www.gnu.org/licenses/>. 

############################
# INCLUDES
############################

declare -g -r __source_dir="${BATS_TEST_DIRNAME}"

load "${__source_dir}/test-rmt-docker-volumes-mocks-common.bash"

load "${__source_dir}/../../rmt-docker-volumes-json.dotfile"
load "${__source_dir}/../../rmt-docker-volumes-cmd-get.dotfile"
load "${__source_dir}/../../rmt-docker-volumes-data-common.dotfile"

load "${__source_dir}/../../../common/json.dotfile"
load "${__source_dir}/../../../common/strings.dotfile"
load "${__source_dir}/../../../common/types.dotfile"

load "${__source_dir}/../../../common-test/test-rmt-mocks-common.bash"

############################
# WRAPPERS
############################

# this helper writes the content of listRef (which is the first argument for host-get-servol
#   and gets filled in that function) to stdout, in order to access its content via result
#   list that is provided by bats
host-get-servol-wrapper() {
# set -x
  declare -r -n listRef="$1"
  host-get-servol "$@"
  local res=$?
  printf '%s\n' "${listRef[@]}"
  return "${res}"
# set +x
}

# this helper writes the content of listRef (which is the first argument for host-get-servol
#   and gets filled in that function) to stdout, in order to access its content via result
#   list that is provided by bats
host-get-vol-wrapper() {
# set -x
  declare -r -n listRef="$1"
  host-get-vol "$@"
  local res=$?
  printf '%s\n' "${listRef[@]}"
# set +x
  return "${res}"
}

############################
# TESTS
############################

@test "invoke rmt-docker-volume-cmd-get - host-get-vol - no args" {

  # missing list and host arg
  run host-get-vol
  # printf '# l %s\n' "${output}" >&3

  [ ${status} -eq 1 ]
  [[ "${output}" =~ .*declare* ]]
  
  # missing host arg
  run host-get-vol outList

  [ ${status} -eq 1 ]
  [ -z "${output}" ]
}

@test "invoke rmt-docker-volume-cmd-get - host-get-vol - invalid args" {

  run host-get-vol 'out list' host
  # printf '# l %s\n' "${lines[@]}" >&3

  [ ${status} -eq 1 ]
  [[ "${output}" =~ .*declare* ]]
  
  run host-get-vol outlist 'host name'

  [ ${status} -eq 1 ]
  [ -z "${output}" ]
}

@test "invoke rmt-docker-volume-cmd-get - host-get-servol - no args" {

  # missing list, host and service args
  run host-get-servol

  [ ${status} -eq 1 ]
  [[ "${output}" =~ .*declare* ]]
  
  # missing host and service args
  run host-get-servol outList

  [ ${status} -eq 1 ]
  [ -z "${output}" ]
  
  # missing service arg
  run host-get-servol outList hostname

  [ ${status} -eq 1 ]
  [ -z "${output}" ]
}

@test "invoke rmt-docker-volume-cmd-get - host-get-servol - invalid args" {

  run host-get-servol 'out list' hostname servicename

  [ ${status} -eq 1 ]
  [[ "${output}" =~ .*declare* ]]
  
  run host-get-servol outlist 'host name' servicename

  [ ${status} -eq 1 ]
  [ -z "${output}" ]
  
  run host-get-servol outlist hostname 'service name'

  [ ${status} -eq 1 ]
  [ -z "${output}" ]
}

###########

@test "invoke rmt-docker-volume-cmd-get - host-get-servol - error - ssh" {

   __sshpass_cmd=mock-cmd-error

  local outList=()
  run host-get-servol-wrapper outList hostname 'test-service' 'test-volume' <<< 'somepwd'
  # printf '# l %s\n' "${lines[@]}" >&3
  
  [ ${status} -eq 1 ]
  [ "${lines[0]}" == 'error: retrieval of docker volumes failed' ]
  
  run host-get-servol-wrapper outList hostname 'test-service'  <<< 'somepwd'
  # printf '# l %s\n' "${lines[@]}" >&3
  
  [ ${status} -eq 1 ]
  [ "${lines[0]}" == 'error: retrieval of docker volumes failed' ]
}

@test "invoke rmt-docker-volume-cmd-get - host-get-servol - error - invalid docker inspect json" {

   __sshpass_cmd=mock-ssh-invalid-object-json

  local outList=()
  run host-get-servol-wrapper outList hostname 'test-service' 'test-volume' <<< 'somepwd'
  # printf '# l %s\n' "${lines[@]}" >&3
  
  [ ${status} -eq 1 ]
  [[ "${lines[0]}" =~ 'parse error' ]]
}

@test "invoke rmt-docker-volume-cmd-get - host-get-servol - error - unexpected json - number" {

   __sshpass_cmd=mock-ssh-filled-list-json

  local outList=()
  run host-get-servol-wrapper outList hostname 'test-service' 'test-volume' <<< 'somepwd'
  # printf '# l %s\n' "${lines[@]}" >&3
  
  [ ${status} -eq 1 ]
  [ "${lines[0]}" == 'error: json object is not of type [object] but [number]' ]
  [ "${lines[1]}" == 'error: json object is not of type [object] but [number]' ]
  [ "${lines[2]}" == 'error: json object is not of type [object] but [number]' ]
  [ "${lines[3]}" == 'error: json object is not of type [object] but [number]' ]
}

@test "invoke rmt-docker-volume-cmd-get - host-get-servol - error - unexpected json - object" {

   __sshpass_cmd=mock-ssh-basic-object-json

  local outList=()
  run host-get-servol-wrapper outList hostname 'test-service' 'test-volume' <<< 'somepwd'
  # printf '# l %s\n' "${lines[@]}" >&3
  
  [ ${status} -eq 1 ]
  [ "${lines[0]}" == 'error: json object is not of type [array] but [object]' ]
}

@test "invoke rmt-docker-volume-cmd-get - host-get-servol - error - incomplete single json" {

   __sshpass_cmd=mock-ssh-docker-volume-inspect-single-volume-incomplete-json

  local outList=()
  run host-get-servol-wrapper outList hostname 'test-service' 'test-volume' <<< 'somepwd'
  # printf '# l %s\n' "${lines[@]}" >&3
  
  [ ${status} -eq 1 ]
  [ "${lines[0]}" == 'error: json object has no keys ["Mounts"]' ]
}

@test "invoke rmt-docker-volume-cmd-get - host-get-servol - error - incomplete multi json" {

   __sshpass_cmd=mock-ssh-docker-volume-inspect-multi-volume-incomplete-json

  local outList=()
  run host-get-servol-wrapper outList hostname 'test-service' 'test-volume' <<< 'somepwd'
  # printf '# l %s\n' "${lines[@]}" >&3
  
  [ ${status} -eq 1 ]
  [ "${lines[0]}" == 'error: json object has no keys ["Source"]' ]
}

###########

@test "invoke rmt-docker-volume-cmd-get - host-get-vol - error - ssh" {

   __sshpass_cmd=mock-cmd-error

  local outList=()
  run host-get-vol-wrapper outList hostname 'test-volume' <<< 'somepwd'
  # printf '# l %s\n' "${lines[@]}" >&3
  
  [ ${status} -eq 1 ]
  [ "${lines[0]}" == 'error: retrieval of docker volumes failed' ]
  
  run host-get-vol-wrapper outList hostname  <<< 'somepwd'
  # printf '# l %s\n' "${lines[@]}" >&3
  
  [ ${status} -eq 1 ]
  [ "${lines[0]}" == 'error: retrieval of docker volumes failed' ]
}

@test "invoke rmt-docker-volume-cmd-get - host-get-vol - error - invalid docker inspect json" {

   __sshpass_cmd=mock-ssh-invalid-object-json

  local outList=()
  run host-get-vol-wrapper outList hostname 'test-volume' <<< 'somepwd'
  # printf '# l %s\n' "${lines[@]}" >&3
  
  [ ${status} -eq 1 ]
  [[ "${lines[0]}" =~ 'parse error' ]]
}

@test "invoke rmt-docker-volume-cmd-get - host-get-vol - error - unexpected json - number" {

   __sshpass_cmd=mock-ssh-filled-list-json

  local outList=()
  run host-get-vol-wrapper outList hostname 'test-volume' <<< 'somepwd'
  # printf '# l %s\n' "${lines[@]}" >&3
  
  [ ${status} -eq 1 ]
  [ "${lines[0]}" == 'error: json object is not of type [object] but [number]' ]
  [ "${lines[1]}" == 'error: json object is not of type [object] but [number]' ]
  [ "${lines[2]}" == 'error: json object is not of type [object] but [number]' ]
  [ "${lines[3]}" == 'error: json object is not of type [object] but [number]' ]
}

@test "invoke rmt-docker-volume-cmd-get - host-get-vol - error - unexpected json - object" {

   __sshpass_cmd=mock-ssh-basic-object-json

  local outList=()
  run host-get-vol-wrapper outList hostname 'test-volume' <<< 'somepwd'
  # printf '# l %s\n' "${lines[@]}" >&3
  
  [ ${status} -eq 1 ]
  [ "${lines[0]}" == 'error: json object is not of type [array] but [object]' ]
}

@test "invoke rmt-docker-volume-cmd-get - host-get-vol - error - incomplete single json" {

   __sshpass_cmd=mock-ssh-docker-inspect-single-volume-incomplete-json

  local outList=()
  run host-get-vol-wrapper outList hostname 'test-service' 'test-volume' <<< 'somepwd'
  # printf '# l %s\n' "${lines[@]}" >&3
  
  [ ${status} -eq 1 ]
  [ "${lines[0]}" == 'error: json object has no keys ["Name"]' ]
}

@test "invoke rmt-docker-volume-cmd-get - host-get-vol - error - incomplete multi json" {

   __sshpass_cmd=mock-ssh-docker-inspect-multi-volume-incomplete-json

  local outList=()
  run host-get-vol-wrapper outList hostname 'test-service' 'test-volume' <<< 'somepwd'
  # printf '# l %s\n' "${lines[@]}" >&3
  
  [ ${status} -eq 1 ]
  [ "${lines[0]}" == 'error: json object has no keys ["Mountpoint"]' ]
}

###########

# NOTE outList wont contain data after successfull invocation as
#   run forks a subshell thus data is not propagated to the caller
#   instead the wrapper contains an internal list that gets filled
#   and prints the list content to stdout
@test "invoke rmt-docker-volume-cmd-get - host-get-servol - with pwd - single volume - match - default" {

  __sshpass_cmd=mock-ssh-docker-service-inspect-single-volume-json

  local outList=()
  run host-get-servol-wrapper outList hostname 'test-service' <<< 'somepwd'
  # printf '# l %s\n' "${lines[@]}" >&3
  
  [ ${status} -eq 0 ]
  [ "${lines[0]}" == 'test-service|1234|test-volume|/mount/path/on/remote/host|/some/path/in/volume|volume' ]
}

@test "invoke rmt-docker-volume-cmd-get - host-get-servol - with pwd - single volume - match - pattern" {

  __sshpass_cmd=mock-ssh-docker-service-inspect-single-volume-json

  local outList=()
  run host-get-servol-wrapper outList hostname 'test-service' 'volume' <<< 'somepwd'
  # printf '# l %s\n' "${lines[@]}" >&3
  
  [ ${status} -eq 0 ]
  [ "${lines[0]}" == 'test-service|1234|test-volume|/mount/path/on/remote/host|/some/path/in/volume|volume' ]
}

@test "invoke rmt-docker-volume-cmd-get - host-get-servol - with pwd - single volume - match - regex" {

  __sshpass_cmd=mock-ssh-docker-service-inspect-single-volume-json

  local outList=()
  run host-get-servol-wrapper outList hostname 'test-service' '.*-volume' <<< 'somepwd'
  # printf '# l %s\n' "${lines[@]}" >&3
  
  [ ${status} -eq 0 ]
  [ "${lines[0]}" == 'test-service|1234|test-volume|/mount/path/on/remote/host|/some/path/in/volume|volume' ]
}

@test "invoke rmt-docker-volume-cmd-get - host-get-servol - with pwd - single volume - no match" {

  __sshpass_cmd=mock-ssh-docker-service-inspect-single-volume-json

  local outList=()
  run host-get-servol-wrapper outList hostname 'test-service' 'test-volume-1' <<< 'somepwd'
  # printf '# l %s\n' "${lines[@]}" >&3
  
  [ ${status} -eq 0 ]
  [ -z "${output}" ]
}

@test "invoke rmt-docker-volume-cmd-get - host-get-servol - with pwd - multi volumes - match - default" {

  __sshpass_cmd=mock-ssh-docker-service-inspect-multi-volume-json

  local outList=()
  run host-get-servol-wrapper outList hostname 'test-service' <<< 'somepwd'
  # printf '# l %s\n' "${lines[@]}" >&3
  
  [ ${status} -eq 0 ]
  [ "${lines[0]}" == 'test-service|1234|test-volume-abc|/mount/path/on/remote/host/abc|/some/path/in/volume/abc|volume' ]
  [ "${lines[1]}" == 'test-service|1234|test-volume-xyz|/mount/path/on/remote/host/xyz|/some/path/in/volume/xyz|volume' ]
}

@test "invoke rmt-docker-volume-cmd-get - host-get-servol - with pwd - multi volumes - match - pattern" {

  __sshpass_cmd=mock-ssh-docker-service-inspect-multi-volume-json

  local outList=()
  run host-get-servol-wrapper outList hostname 'test-service' 'test-volume' <<< 'somepwd'
  # printf '# l %s\n' "${lines[@]}" >&3
  
  [ ${status} -eq 0 ]
  [ "${lines[0]}" == 'test-service|1234|test-volume-abc|/mount/path/on/remote/host/abc|/some/path/in/volume/abc|volume' ]
  [ "${lines[1]}" == 'test-service|1234|test-volume-xyz|/mount/path/on/remote/host/xyz|/some/path/in/volume/xyz|volume' ]
}

@test "invoke rmt-docker-volume-cmd-get - host-get-servol - with pwd - multi volumes - match - regex" {

  __sshpass_cmd=mock-ssh-docker-service-inspect-multi-volume-json

  local outList=()
  run host-get-servol-wrapper outList hostname 'test-service' '(test-volume-abc|test-volume-xyz)' <<< 'somepwd'
  # printf '# l %s\n' "${lines[@]}" >&3
  
  [ ${status} -eq 0 ]
  [ "${lines[0]}" == 'test-service|1234|test-volume-abc|/mount/path/on/remote/host/abc|/some/path/in/volume/abc|volume' ]
  [ "${lines[1]}" == 'test-service|1234|test-volume-xyz|/mount/path/on/remote/host/xyz|/some/path/in/volume/xyz|volume' ]
}

@test "invoke rmt-docker-volume-cmd-get - host-get-servol - with pwd - multi volumes - no match" {

  __sshpass_cmd=mock-ssh-docker-service-inspect-multi-volume-json

  local outList=()
  run host-get-servol-wrapper outList hostname 'test-service' 'unknown-service' <<< 'somepwd'
  # printf '# l %s\n' "${lines[@]}" >&3
  
  [ ${status} -eq 0 ]
  [ -z "${output}" ]
}

###########

@test "invoke rmt-docker-volume-cmd-get - host-get-vol - with pwd - single volume - match - default" {

  __sshpass_cmd=mock-ssh-docker-volume-inspect-single-volume-json

  local outList=()
  run host-get-vol-wrapper outList hostname <<< 'somepwd'
  # printf '# l %s\n' "${lines[@]}" >&3
  
  [ ${status} -eq 0 ]
  [ "${lines[0]}" == 'test-volume|/mount/path/on/remote/host' ]
}

@test "invoke rmt-docker-volume-cmd-get - host-get-vol - with pwd - single volume - match - pattern" {

  __sshpass_cmd=mock-ssh-docker-volume-inspect-single-volume-json

  local outList=()
  run host-get-vol-wrapper outList hostname 'volume' <<< 'somepwd'
  # printf '# l %s\n' "${lines[@]}" >&3
  
  [ ${status} -eq 0 ]
  [ "${lines[0]}" == 'test-volume|/mount/path/on/remote/host' ]
}

@test "invoke rmt-docker-volume-cmd-get - host-get-vol - with pwd - single volume - match - regex" {

  __sshpass_cmd=mock-ssh-docker-volume-inspect-single-volume-json

  local outList=()
  run host-get-vol-wrapper outList hostname '.*-volume' <<< 'somepwd'
  # printf '# l %s\n' "${lines[@]}" >&3
  
  [ ${status} -eq 0 ]
  [ "${lines[0]}" == 'test-volume|/mount/path/on/remote/host' ]
}

@test "invoke rmt-docker-volume-cmd-get - host-get-vol - with pwd - single volume - no match" {

  __sshpass_cmd=mock-ssh-docker-volume-inspect-single-volume-json

  local outList=()
  run host-get-vol-wrapper outList hostname 'test-volume-1' <<< 'somepwd'
  # printf '# l %s\n' "${lines[@]}" >&3
  
  [ ${status} -eq 0 ]
  [ -z "${output}" ]
}

@test "invoke rmt-docker-volume-cmd-get - host-get-vol - with pwd - multi volumes - match - default" {

  __sshpass_cmd=mock-ssh-docker-volume-inspect-multi-volume-json

  local outList=()
  run host-get-vol-wrapper outList hostname <<< 'somepwd'
  # printf '# l %s\n' "${lines[@]}" >&3
  
  [ ${status} -eq 0 ]
  [ "${lines[0]}" == 'test-volume-abc|/mount/path/on/remote/host/abc' ]
  [ "${lines[1]}" == 'test-volume-xyz|/mount/path/on/remote/host/xyz' ]
}

@test "invoke rmt-docker-volume-cmd-get - host-get-vol - with pwd - multi volumes - match - pattern" {

  __sshpass_cmd=mock-ssh-docker-volume-inspect-multi-volume-json

  local outList=()
  run host-get-vol-wrapper outList hostname 'test-volume' <<< 'somepwd'
  # printf '# l %s\n' "${lines[@]}" >&3
  
  [ ${status} -eq 0 ]
  [ "${lines[0]}" == 'test-volume-abc|/mount/path/on/remote/host/abc' ]
  [ "${lines[1]}" == 'test-volume-xyz|/mount/path/on/remote/host/xyz' ]
}

@test "invoke rmt-docker-volume-cmd-get - host-get-vol - with pwd - multi volumes - match - regex" {

  __sshpass_cmd=mock-ssh-docker-volume-inspect-multi-volume-json

  local outList=()
  run host-get-vol-wrapper outList hostname '(test-volume-abc|test-volume-xyz)' <<< 'somepwd'
  # printf '# l %s\n' "${lines[@]}" >&3
  
  [ ${status} -eq 0 ]
  [ "${lines[0]}" == 'test-volume-abc|/mount/path/on/remote/host/abc' ]
  [ "${lines[1]}" == 'test-volume-xyz|/mount/path/on/remote/host/xyz' ]
}

@test "invoke rmt-docker-volume-cmd-get - make-docker-inspect-vol-cmd" {

  run make-docker-inspect-vol-cmd
  # printf '# l %s\n' "${lines[@]}" >&3
  
  [ ${status} -eq 0 ]
  [ "${output}" == 'while read -r id; do docker volume inspect ${id}; done < <(docker volume list -q)' ]
}

@test "invoke rmt-docker-volume-cmd-get - make-docker-inspect-servol-cmd" {

  run make-docker-inspect-servol-cmd 'test-service'
  # printf '# l %s\n' "${lines[@]}" >&3
  
  [ ${status} -eq 0 ]
  [ "${output}" == 'while read -r id; do docker inspect ${id}; done < <(docker ps -f name=test-service -q)' ]
  }