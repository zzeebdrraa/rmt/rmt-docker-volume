#!/usr/bin/env bash

# Copyright (C) 2023 zeebdrraa
#
# This file is part of rmt-docker-volumes.
#
# rmt-docker-volumes is free software: you can redistribute it and/or
# modify it under the terms of the GNU General Public License as published
# by the Free Software Foundation, either version 3 of the License, or 
# (at your option) any later version.
#
# rmt-docker-volumes is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
# Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with rmt-docker-volumes. If not, see <https://www.gnu.org/licenses/>. 

################################
# MOCKS
################################

#*******************************
# json for cmd-list-data-servol
#*******************************

# misisng Mounts field
declare -r -g __docker_inspect_service_single_volume_incomplete_missing_mounts_json='[
  {
    "Name":"test-service",
    "Id":"1234"
  }
]'

# missing Source field in one volume object
declare -r -g __docker_inspect_service_multi_volume_incomplete_missing_source_json='[
  {
    "Mounts":[{
      "Type":"volume",
      "Name":"test-volume-abc",
      "Destination":"/some/path/in/volume/abc",
      "Source":"/mount/path/on/remote/host/abc"
    },{
      "Type":"volume",
      "Name":"test-volume-xyz",
      "Destination":"/some/path/in/volume/xyz"
    }],
    "Name":"test-service",
    "Id":"1234"
  }
]'

# Type can only be 'bind' or "volume"
declare -r -g __docker_inspect_service_single_volume_unknown_type_json='[
  {
    "Mounts":[{
      "Type":"unknownType",
      "Name":"test-volume",
      "Destination":"/some/path/in/volume",
      "Source":"/mount/path/on/remote/host"
    }],
    "Name":"test-service",
    "Id":"1234"
  }
]'

declare -r -g __docker_inspect_service_single_volume_unknown_name_json='[
  {
    "Mounts":[{
      "Type":"volume",
      "Name":"unknown",
      "Destination":"/some/path/in/volume",
      "Source":"/mount/path/on/remote/host"
    }],
    "Name":"test-service",
    "Id":"1234"
  }
]'

declare -r -g __docker_inspect_service_single_bind_unknown_name_json='[
  {
    "Mounts":[{
      "Type":"bind",
      "Destination":"/some/path/unknown",
      "Source":"/mount/path/on/remote/host/unknown",
      "Mode": "",
      "RW": true,
      "Propagation": "rprivate"
    }],
    "Name":"test-service",
    "Id":"1234"
  }
]'

declare -r -g __docker_inspect_service_single_mixed_unknown_name_json='[
  {
    "Mounts":[{
      "Type":"volume",
      "Name":"unknown",
      "Destination":"/some/path/in/volume",
      "Source":"/mount/path/on/remote/host"
    },{
      "Type":"bind",
      "Destination":"/some/path/unknown",
      "Source":"/mount/path/on/remote/host/unknown",
      "Mode": "",
      "RW": true,
      "Propagation": "rprivate"
    }],
    "Name":"test-service",
    "Id":"1234"
  }
]'

declare -r -g __docker_inspect_service_single_volume_json='[
  {
    "Mounts":[{
      "Type":"volume",
      "Name":"test-volume",
      "Destination":"/some/path/in/volume",
      "Source":"/mount/path/on/remote/host"
    }],
    "Name":"test-service",
    "Id":"1234"
  }
]'

declare -r -g __docker_inspect_service_multi_volume_json='[
  {
    "Mounts":[{
      "Type":"volume",
      "Name":"test-volume-abc",
      "Destination":"/some/path/in/volume/abc",
      "Source":"/mount/path/on/remote/host/abc"
    },{
      "Type":"volume",
      "Name":"test-volume-xyz",
      "Destination":"/some/path/in/volume/xyz",
      "Source":"/mount/path/on/remote/host/xyz"
    }],
    "Name":"test-service",
    "Id":"1234"
  }
]'

declare -r -g __docker_inspect_service_single_bind_json='[
  {
    "Mounts":[{
      "Type":"bind",
      "Destination":"/some/path/in/volume",
      "Source":"/mount/path/on/remote/host",
      "Mode": "",
      "RW": true,
      "Propagation": "rprivate"
    }],
    "Name":"test-service",
    "Id":"1234"
  }
]'

declare -r -g __docker_inspect_service_multi_bind_json='[
  {
    "Mounts":[{
      "Type":"bind",
      "Destination":"/some/path/in/volume/abc",
      "Source":"/mount/path/on/remote/host/abc",
      "Mode": "",
      "RW": true,
      "Propagation": "rprivate"
    },{
      "Type":"bind",
      "Destination":"/some/path/in/volume/xyz",
      "Source":"/mount/path/on/remote/host/xyz",
      "Mode": "",
      "RW": true,
      "Propagation": "rprivate"
    }],
    "Name":"test-service",
    "Id":"1234"
  }
]'

declare -r -g  __docker_inspect_service_multi_mixed_json='[
  {
    "Mounts":[{
      "Type":"bind",
      "Destination":"/some/path/in/volume/abc",
      "Source":"/mount/path/on/remote/host/abc",
      "Mode": "",
      "RW": true,
      "Propagation": "rprivate"
    },{
      "Type":"volume",
      "Name":"test-volume-xyz",
      "Destination":"/some/path/in/volume/xyz",
      "Source":"/mount/path/on/remote/host/xyz"
    }],
    "Name":"test-service",
    "Id":"1234"
  }
]'

#*******************************
# json for cmd-list-data-vol
#*******************************

declare -r -g __docker_inspect_volume_single_incomplete_missing_name_json='[
  {
    "Driver": "local",
    "Labels": {},
    "Mountpoint":"/mount/path/on/remote/host"
  }
]'

declare -r -g __docker_inspect_volume_single_abc_incomplete_missing_mountpoint_json='[
  {
    "Driver": "local",
    "Labels": {},
    "Name":"test-volume-abc",
  }
]'

declare -r -g __docker_inspect_volume_single_xyz_incomplete_missing_mountpoint_json='[
  {
    "Driver": "local",
    "Labels": {},
    "Name":"test-volume-xyz"
  }
]'

declare -r -g __docker_inspect_volume_multi_incomplete_missing_mountpoint_json='[
  {
    "Driver": "local",
    "Labels": {},
    "Name":"test-volume-abc"
  },{
    "Driver": "local",
    "Labels": {},
    "Name":"test-volume-xyz"
  }
]'

declare -r -g __docker_inspect_volume_single_unknown_name_json='[
  {
    "Driver": "local",
    "Labels": {},
    "Name":"test-unknown",
    "Mountpoint":"/mount/path/on/remote/host/abc"
  }
]'

declare -r -g __docker_inspect_volume_single_json='[
  {
    "Driver": "local",
    "Labels": {},
    "Name":"test-volume",
    "Mountpoint":"/mount/path/on/remote/host"
  }
]'

declare -r -g __docker_inspect_volume_single_abc_json='[
  {
    "Driver": "local",
    "Labels": {},
    "Name":"test-volume-abc",
    "Mountpoint":"/mount/path/on/remote/host/abc"
  }
]'

declare -r -g __docker_inspect_volume_single_xyz_json='[
  {
    "Driver": "local",
    "Labels": {},
    "Name":"test-volume-xyz",
    "Mountpoint":"/mount/path/on/remote/host/xyz"
  }
]'

declare -r -g __docker_inspect_volume_multi_json='[
  {
    "Driver": "local",
    "Labels": {},
    "Name":"test-volume-abc",
    "Mountpoint":"/mount/path/on/remote/host/abc"
  },{
    "Driver": "local",
    "Labels": {},
    "Name":"test-volume-xyz",
    "Mountpoint":"/mount/path/on/remote/host/xyz"
  }
]'

###############################
# Mocks
###############################

#*******************************
# mocks for valid plain json 
#*******************************

mock-ssh-empty-object-json() {
  printf '{}'
  return 0
}

mock-ssh-empty-objects-in-list-json() {
  printf '[{},{},{}]'
  return 0
}

mock-ssh-empty-list-json() {
  printf '[]'
  return 0
}

mock-ssh-basic-object-json() {
  printf '{"name":"test"}'
  return 0
}

mock-ssh-filled-list-json() {
  printf '[1,2,3,4]'
  return 0
}

#*******************************
# mocks for invalid plain json 
#*******************************

mock-ssh-invalid-object-json() {
  printf '%s\n' "[}"
  return 0
}

#*******************************
# ls mocks
#*******************************

mock-ssh-ls() {

  [[ "$*" =~ (ls -la /mount/path/on/remote/host/abc) ]] && printf '%s\n' 'book-abc.pdf'
  [[ "$*" =~ (ls -la /mount/path/on/remote/host/xyz) ]] && printf '%s\n' 'book-xyz.pdf'
  
  return 0
}

mock-ssh-ls-sudo() {

  [[ "$*" =~ (sudo ls -la /mount/path/on/remote/host/abc) ]] && printf '%s\n' 'book-abc.pdf'
  [[ "$*" =~ (sudo ls -la /mount/path/on/remote/host/xyz) ]] && printf '%s\n' 'book-xyz.pdf'
  
  return 0
}

#*******************************
# common mocks for incomplete inspect json
#*******************************

mock-ssh-docker-volume-inspect-single-volume-incomplete-json() {
  [[ "$*" =~ (.*docker inspect*) ]] && printf '%s' "${__docker_inspect_service_single_volume_incomplete_missing_mounts_json}"
  return 0
}

mock-ssh-docker-volume-inspect-multi-volume-incomplete-json() {
  [[ "$*" =~ (.*docker inspect*) ]] && printf '%s' "${__docker_inspect_service_multi_volume_incomplete_missing_source_json}"
  return 0
}

mock-ssh-docker-inspect-single-volume-incomplete-json() {
  [[ "$*" =~ (.*docker volume inspect*) ]] && printf '%s' "${__docker_inspect_volume_single_incomplete_missing_name_json}"
  return 0
}

mock-ssh-docker-inspect-multi-volume-incomplete-json() {
  [[ "$*" =~ (.*docker volume inspect*) ]] && printf '%s' "${__docker_inspect_volume_multi_incomplete_missing_mountpoint_json}"
  return 0
}

#*******************************
# common mocks docker volume inspect
#*******************************

mock-ssh-docker-volume-inspect-error() {
  [[ "$*" =~ (.*docker volume inspect*) ]] && return 1
  return 0
}

mock-ssh-docker-service-inspect-error() {
  [[ "$*" =~ (.*docker inspect*) ]] && return 1
  return 0
}

mock-ssh-docker-volume-inspect-unknown-name() {
  [[ "$*" =~ (.*docker volume inspect*) ]] && printf '%s' "${__docker_inspect_volume_single_unknown_name_json}"
  return 1
}

mock-ssh-docker-volume-inspect-single-volume-json() {
  [[ "$*" =~ (.*docker volume inspect*) ]] && printf '%s' "${__docker_inspect_volume_single_json}" 
  return 0
}

mock-ssh-docker-volume-inspect-multi-volume-json() {
  [[ "$*" =~ (.*docker volume inspect*) ]] && printf '%s' "${__docker_inspect_volume_multi_json}"
  return 0
}

mock-ssh-docker-service-inspect-single-volume-json() {
  [[ "$*" =~ (.*docker inspect*) ]] && printf '%s' "${__docker_inspect_service_single_volume_json}"
  return 0
}

mock-ssh-docker-service-inspect-multi-volume-json() {
  [[ "$*" =~ (.*docker inspect*) ]] && printf '%s' "${__docker_inspect_service_multi_volume_json}"
  return 0
}

mock-ssh-docker-volume-inspect-single-volume-abc-json() {
  [[ "$*" =~ (.*docker volume inspect test-volume-abc) ]] && printf '%s' "${__docker_inspect_volume_single_abc_json}"
  return 0
}

mock-ssh-docker-volume-inspect-single-volume-xyz-json() {
  [[ "$*" =~ (.*docker volume inspect test-volume-xyz) ]] && printf '%s' "${__docker_inspect_volume_single_xyz_json}"
  return 0
}

mock-ssh-docker-service-inspect-single-bind-json() {
  [[ "$*" =~ (.*docker inspect*) ]] && printf '%s' "${__docker_inspect_service_single_bind_json}"
  return 0
}

mock-ssh-docker-service-inspect-multi-bind-json() {
  [[ "$*" =~ (.*docker inspect*) ]] && printf '%s' "${__docker_inspect_service_multi_bind_json}"
  return 0
}
