#!/usr/bin/env bash
# set -x

# Copyright (C) 2023 zeebdrraa
#
# This file is part of rmt-docker-volumes.
#
# rmt-docker-volumes is free software: you can redistribute it and/or
# modify it under the terms of the GNU General Public License as published
# by the Free Software Foundation, either version 3 of the License, or 
# (at your option) any later version.
#
# rmt-docker-volumes is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
# Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with rmt-docker-volumes. If not, see <https://www.gnu.org/licenses/>. 

# shellcheck disable=SC2155
declare -r __rmt_docker_volumes_sh_script_dir=$(cd -P -- "$(dirname -- "$0")" && pwd -P)
cd "${__rmt_docker_volumes_sh_script_dir}" || exit

# shellcheck disable=SC1091
source ./rmt-docker-volumes.dotfile

# see rmt-docker-volumes.sh -h for detailed help
run-rmt-docker-volume "$@"